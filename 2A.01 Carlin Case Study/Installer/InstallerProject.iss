#define COMPANY_NAME 'Cortina Productions, Inc'
#define PRODUCT_VERSION '0.9.037b'
#define PRODUCT_NAME 'Carlin Case Study'
#define DEST_DIR_NAME 'C:\Exhibit\CCS'
#define EXE_NAME 'ApplicationManager.exe'

[Setup]
; Set the installer name and core properties
AppName={#PRODUCT_NAME}
AppVersion={#PRODUCT_VERSION}
DefaultDirName={#DEST_DIR_NAME}
DefaultGroupName={#COMPANY_NAME}
AlwaysRestart=yes
DisableProgramGroupPage=yes
PrivilegesRequired=admin
Compression=lzma2/fast 
OutputDir=D:\Cortina\Installers
SolidCompression=yes
InternalCompressLevel=fast
OutputBaseFilename=Setup - {#PRODUCT_NAME}_{#PRODUCT_VERSION}
DisableDirPage=yes
DisableReadyPage=yes
;WizardImageFile=CortinaLogo.bmp
WizardImageBackColor=clWhite
DiskSpanning=no
SlicesPerDisk=1
DiskSliceSize=1500000000
AppPublisher={#COMPANY_NAME}
AppPublisherURL=http://www.CortinaProductions.com
UninstallDisplayIcon={uninstallexe}
DisableWelcomePage=no

[Messages]
WelcomeLabel2=This will install [name/ver] on your computer. %n%nIt is recommended that you close all other applications and disable any anti virus before continuing.

[Files]
; Tell Inno Setup which files to install and where to put them
Source: "K-Lite_Codec_Pack_1040_Basic.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall ignoreversion;
Source: "HapDirectShowCodecSetup.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall ignoreversion;
Source: "CompactView_1.4.12.0_Installer.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall ignoreversion;
Source: "SSCERuntime_x64-ENU.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall ignoreversion;
Source: "klcp_basic_unattended.ini"; DestDir: "{tmp}"; Flags: deleteafterinstall ignoreversion;
Source: "*"; DestDir: "{#DEST_DIR_NAME}"; Excludes: "*.iss,*.vhost.exe,*.eit, output_log.txt"; Flags: recursesubdirs ignoreversion;

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"
Name: startupicon; Description: "Create a &startup icon"
Name: prepkiosk; Description: "Prep Kiosk"

[Run]
Filename: "{tmp}{\}K-Lite_Codec_Pack_1040_Basic.exe"; Parameters: "/verysilent /norestart /LoadInf={tmp}{\}klcp_basic_unattended.ini"
Filename: "{tmp}{\}HapDirectShowCodecSetup.exe"; Parameters: "/S"
Filename: "{tmp}{\}SSCERuntime_x64-ENU.exe"; Parameters: "/i /passive"
Filename: "{tmp}{\}CompactView_1.4.12.0_Installer.exe"; Parameters: "/S"
; Offer to launch our product at the end of the installation
Filename: "{#DEST_DIR_NAME}{\}{#EXE_NAME}"; Flags: postinstall; Description: "Launch {#PRODUCT_NAME} now!"

[UninstallDelete]
Type: files; Name: "{#DEST_DIR_NAME}{\}startInteractive.cmd"

[Registry]

; Disable the lock screen
Root: "HKLM"; Subkey: "SOFTWARE\Policies\Microsoft\Windows\Personalization"; ValueType: dword; ValueName: "NoLockScreen"; ValueData: "1"; Tasks: prepkiosk
; Disables access to Windows Updates
Root: "HKLM"; Subkey: "Software\Policies\Microsoft\Windows\WindowsUpdate"; ValueType: dword; ValueName: "DisableWindowsUpdateAccess"; ValueData: "1"; Tasks: prepkiosk  
; Disables the Windows shell from attempting to restart
Root: "HKLM"; Subkey: "Software\Microsoft\Windows NT\CurrentVersion\Winlogon"; ValueType: dword; ValueName: "AutoRestartShell"; ValueData: "0"; Tasks: prepkiosk  
; Disables the Windows error reporting popup
Root: "HKCU"; Subkey: "Software\Microsoft\Windows\Windows Error Reporting"; ValueType: dword; ValueName: "DontShowUI"; ValueData: "1"; Tasks: prepkiosk
; Disables error reporting from sending additional info
Root: "HKCU"; Subkey: "Software\Microsoft\Windows\Windows Error Reporting"; ValueType: dword; ValueName: "DontSendAdditionalData"; ValueData: "1"; Tasks: prepkiosk
; Boot directly to the desktop vs. metro menu
Root: "HKCU"; Subkey: "Software\Microsoft\Windows\CurrentVersion\Explorer\StartPage"; ValueType: dword; ValueName: "OpenAtLogon"; ValueData: "0"; Tasks: prepkiosk
; Disable notification balloons
Root: "HKCU"; Subkey: "Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced"; ValueType: dword; ValueName: "EnableBalloonTips"; ValueData: "0"; Tasks: prepkiosk
; Disable toast notifications
Root: "HKCU"; Subkey: "Software\Microsoft\Windows\CurrentVersion\PushNotifications"; ValueType: dword; ValueName: "ToastEnabled"; ValueData: "0"; Tasks: prepkiosk
; Disable help stickers
Root: "HKLM"; Subkey: "Software\Policies\Microsoft\Windows\EdgeUI"; ValueType: dword; ValueName: "DisableHelpSticker"; ValueData: "1"; Tasks: prepkiosk
; Disable press and hold touch options
Root: "HKCU"; Subkey: "Software\Microsoft\Wisp\Touch"; ValueType: dword; ValueName: "TouchMode_hold"; ValueData: "0"; Tasks: prepkiosk
Root: "HKCU"; Subkey: "Software\Microsoft\Wisp\Touch"; ValueType: dword; ValueName: "TouchModeN_HoldTime_Animation"; ValueData: "0"; Tasks: prepkiosk
Root: "HKCU"; Subkey: "Software\Microsoft\Wisp\Touch"; ValueType: dword; ValueName: "TouchModeN_HoldTime_BeforeAnimation"; ValueData: "0"; Tasks: prepkiosk
Root: "HKCU"; Subkey: "Software\Microsoft\Wisp\Touch"; ValueType: dword; ValueName: "TouchUI"; ValueData: "0"; Tasks: prepkiosk
; Disables flicks
Root: "HKLM"; Subkey: "Software\Microsoft\Windows NT\CurrentVersion\Winlogon"; ValueType: dword; ValueName: "FlickMode"; ValueData: "0"; Tasks: prepkiosk
;  Enables dpi scaling to 100 percent using two following keys
Root: "HKCU"; Subkey: "Control Panel\Desktop"; ValueType: dword; ValueName: "Win8DpiScaling"; ValueData: "1"; Tasks: prepkiosk
Root: "HKCU"; Subkey: "Control Panel\Desktop"; ValueType: dword; ValueName: "LogPixels"; ValueData: "96"; Tasks: prepkiosk
; Disables pen press and hold for right click
Root: "HKLM"; Subkey: "Software\Microsoft\Wisp\Pen\SysEventParameters"; ValueType: dword; ValueName: "HoldMode"; ValueData: "00000003"; Tasks: prepkiosk
; Disables pen feedback
Root: "HKLM"; Subkey: "Software\Microsoft\Wisp\Pen\SysEventParameters"; ValueType: dword; ValueName: "UIFeedbackMode"; ValueData: "0"; Tasks: prepkiosk
 ; Disable edge gestures using three following keys
Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableEdges"; ValueData: "255"; Tasks: prepkiosk
Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableCharmsHint"; ValueData: "1"; Tasks: prepkiosk
Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableTLcorner"; ValueData: "1"; Tasks: prepkiosk
; Disable user account control
Root: "HKLM"; Subkey: "Software\Microsoft\Windows\CurrentVersion\Policies\System"; ValueType: dword; ValueName: "EnableLUA"; ValueData: "0"; Tasks: prepkiosk

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; The above, but target 64-bit
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Disable the lock screen
Root: "HKLM64"; Subkey: "SOFTWARE\Policies\Microsoft\Windows\Personalization"; ValueType: dword; ValueName: "NoLockScreen"; ValueData: "1"; Tasks: prepkiosk
; Disables access to Windows Updates
Root: "HKLM64"; Subkey: "Software\Policies\Microsoft\Windows\WindowsUpdate"; ValueType: dword; ValueName: "DisableWindowsUpdateAccess"; ValueData: "1"; Tasks: prepkiosk  
; Disables the Windows shell from attempting to restart
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows NT\CurrentVersion\Winlogon"; ValueType: dword; ValueName: "AutoRestartShell"; ValueData: "0"; Tasks: prepkiosk  
; Disable help stickers
Root: "HKLM64"; Subkey: "Software\Policies\Microsoft\Windows\EdgeUI"; ValueType: dword; ValueName: "DisableHelpSticker"; ValueData: "1"; Tasks: prepkiosk
; Disables flicks
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows NT\CurrentVersion\Winlogon"; ValueType: dword; ValueName: "FlickMode"; ValueData: "0"; Tasks: prepkiosk
; Disables pen press and hold for right click
Root: "HKLM64"; Subkey: "Software\Microsoft\Wisp\Pen\SysEventParameters"; ValueType: dword; ValueName: "HoldMode"; ValueData: "00000003"; Tasks: prepkiosk
; Disables pen feedback
Root: "HKLM64"; Subkey: "Software\Microsoft\Wisp\Pen\SysEventParameters"; ValueType: dword; ValueName: "UIFeedbackMode"; ValueData: "0"; Tasks: prepkiosk
 ; Disable edge gestures using three following keys
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableEdges"; ValueData: "255"; Tasks: prepkiosk
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableCharmsHint"; ValueData: "1"; Tasks: prepkiosk
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows\CurrentVersion\ImmersiveShell\EdgeUI"; ValueType: dword; ValueName: "DisableTLcorner"; ValueData: "1"; Tasks: prepkiosk
; Disable user account control
Root: "HKLM64"; Subkey: "Software\Microsoft\Windows\CurrentVersion\Policies\System"; ValueType: dword; ValueName: "EnableLUA"; ValueData: "0"; Tasks: prepkiosk

[code]

function CreateBatch(): boolean;
var
  fileName : string;
  lines : TArrayOfString;
begin
  Result := true;
  fileName := ExpandConstant('{#DEST_DIR_NAME}{\}startInteractive.cmd');
  SetArrayLength(lines, 4);
  lines[0] := '@echo off';
  lines[1] := 'timeout 30';
  lines[2] := 'cd "{#DEST_DIR_NAME}"';
  lines[3] := 'start ApplicationManager';
  Result := SaveStringsToFile(filename, lines, false);
  exit;
end;
 
procedure CurStepChanged(CurStep: TSetupStep);
begin
  if  CurStep=ssPostInstall then
    begin
         CreateBatch();
    end
end;

[Icons]
;Add a start menu shortcut
Name: "{group}{\}{#PRODUCT_NAME}{\}{#PRODUCT_NAME}"; Filename: "{#DEST_DIR_NAME}{\}{#EXE_NAME}"
Name: "{commondesktop}{\}{#PRODUCT_NAME}"; Filename: "{#DEST_DIR_NAME}{\}{#EXE_NAME}"; WorkingDir: {app}; Tasks: desktopicon
Name: "{commonstartup}{\}{#PRODUCT_NAME}"; Filename: "{#DEST_DIR_NAME}{\}startInteractive.cmd"; Tasks: startupicon
Name: "{group}\{#PRODUCT_NAME}{\}Uninstall"; Filename: "{#DEST_DIR_NAME}{\}unins000.exe"

