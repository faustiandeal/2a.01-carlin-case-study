﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <appSettings>
    <!-- 
    kiosk application settings
    *************************************************************************************************************
    -->

    <!-- friendly name for the interactive -->
    <add key="friendly_name" value="Carlin Case Study"/>

    <!-- target an exe, or use one of the optional commands: {webbrowser}, {kiosk_ie} -->
    <add key="process" value="dummy.exe"/>
    <add key="arguments" value="-popupwindow"/>

    <!-- [future feature] supply a list of extra software to launch when run-->
    <add key="secondary_processes" value=""/>

    <!-- use an x and y coordinate comma separated (ie 0,0) to tell the open window to position to.
    Leave this blank if unsure since most apps will self manage this -->
    <add key="force_window_pos" value=""/>

    <!-- 
    CMS Caching
    *************************************************************************************************************    
    This type of caching is designed to work with the CMS in a Box system developed by Cortina Productions.    
    -->

    <add key="autocms_cache" value="true"/>
    
    <!-- if this value is 0 or left out, it is assumed to use version 1.0 of the api.  
    Version 2.0 uses the newer friendly json formats -->
    <add key="autocms_api_version" value="2" />

    <!-- example http://extranet.cortinaproductions.com:8080/ram -->
    <add key="autocms_base_path" value="http://35.168.11.80/ncc"/>

    <!-- comma separated list of section keys, if left blank, no cms caching takes place.  
    Files are stored in cache folder with name scheme api_secton_XX.json  -->
    <add key="autocms_data_keys" value="158,168"/>

    <!-- comma separated list of field keys that you would like to ignore, if left blank nothing will be filtered.  
    Beneficial if you have multiple programs sharing the same feed but want to seperate out which media each one downloads. -->
    <add key="autocms_ignored_keys" value=""/>

    <!-- extra urls that are cached and stored locally, these must be full urls followed by a | and the local file name.  Comma separated.
    example http://extranet.cortinaproductions.com:8080/ram/API/GetRecords/?section=29|my_custom_url.json -->
    <add key="custom_data_urls" value="" />

    <!-- parse all custom data url as if they were autocms formatted. This will break on non-autocms format -->
    <add key="parse_custom_url_autocms" value="true"/>

    <!-- stores cms files inside of a folder called "cache".  If left blank, will store cache folder in application's root -->
    <add key="autocms_cache_path" value="CMS_data"/>

    <!-- if the value is set, then any image (jpg, png) cached will be no larger then width x height -->
    <add key="limit_image_size" value="4096x4096"/>

    <!-- these are stored alongside the regular image but with a special naming convention added at the end.  For example: image.png is saved as image-256x256.png -->
    <add key="additional_image_sizes" value=""/>

    <!-- this may go away in future CMS's, but for now it forces image to be downloaded from the video path -->
    <add key="download_from_autocms_videopath" value="false"/>

    <!-- comma separted list of file extensions other then movie (mov, mp4, flv, f4v, wmv, mpeg, ogg), audio (mp3, aiff, wav), or image (png, jpg, jpeg, bmp, tif, tiff, gif) 
     types the CMS knows about - these will be downloaded from video path (soon to be media path)  -->
    <add key="autocms_extra_file_extensions" value=".xml, .swf, .webm"/>
    
     <!-- will create a .dds file for every image that is downloaded.  Programs such as Unity can load these compressed images faster into the graphics card-->
    <add key="create_dds" value="true"/>
    <add key="dds_type" value="dxt5"/>

    <!-- 
    Plugins
    *************************************************************************************************************
    -->
    
    <add key="plugin_dll_folder" value="plugins"/>

   

    <!-- 
    Analytics
    *************************************************************************************************************
    
    Analytic data requires SQLCE to be installed ahead of time.   Applications can send events via a web address
    hosted by applicationmanager.  Normally this is on port 6603 and from the path /analytics.  For example
    to view the portal page with more instructions go to http://127.0.0.1:6604/analytics      
    -->

    <!-- if set to false, no analytics tracking will be enabled, if set to true then SQLCE must be installed -->
    <add key="use_analytics" value="true"/>

    <!-- webserver to send JSON data to, will use date stamp generated at kiosk -->
    <add key="analytic_server_endpoint" value="http://extranet.cortinaproductions.com:8083/ncc/Analytics/Entries/Add?GenerateDateStamp=false" />
    <add key="analytic_server_username" value="username" />
    <add key="analytic_server_password" value="password" />
    <add key="analytic_push_frequency_minutes" value="0.25" />

    <!-- If override true, will try to use ids set here when entries are submitted from a client library.
         If a value is left at -1, that value will not be overriden. This allows selective overriding at a kiosk level. -->
    <add key="analytic_override_values" value="true"/>
    <add key="analytic_app_id" value="31" />
    <add key="analytic_station_id" value="0" />
    <add key="analytic_location_id" value="0" />
    
    <!-- 
    ADA features
    *************************************************************************************************************
    -->

    <!-- if ada_bar is false then no ada bar appears-->
    <add key="ada_bar" value="false"/>
    <add key="ada_bar_offset" value="0,0"/>

    <!-- 
    OS management and lockdown settings
    *************************************************************************************************************
    -->
    
    <!-- if true it will not lockout the screen -->
    <add key="lockdown_windows" value="false"/>

    <!-- time of day to reboot computer formatted HH:mm (ie 24:00), leave -1 or blank if not intending to use-->
    <add key="auto_reboot" value="-1"/>
    
    <!-- time of day to restart application formatted HH:mm (ie 24:00), leave -1 or blank if not intending to use-->
    <add key="auto_restart_app" value="-1"/>

    <!-- specifiy a time range (01:00-24:00) during each day the kiosk goes into a state where the interactive is disabled. 
    A blank value ignores this feature. The interactive can be started again by touching the screen -->
    <add key="idle_schedule" value="" />

    <!-- unique instance name, change this to allow more then one application manager at a time -->
    <add key="instance_name" value="app-manager-instance"/>

    <!-- if persistent is true, then AM will stay open when quitting software with the escape key -->
    <add key="persistent" value="true"/>

    <!-- is heartbeat is more then 0, then a signal must be sent within those seconds or the app will restart -->
    <add key="use_heartbeat" value="0"/>

    <!-- preload shows the cortina logo -->
    <add key="prelaunch_delay" value="3"/>

    <!-- time between launches and relaunches -->
    <add key="app_launch_seconds" value="5"/>

    <!-- if this is true, then explorer.exe will be force quitted, This is required for Windows 8.
    note that registry must be edited for this to be allowed, "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\AutoRestartShell" to 0
    -->
    <add key="force_quit_explorer" value="false"/>

    <!-- exit zero usually means the software was exited by a user normally -->
    <add key="ignore_exit_code_zero" value="false"/>

    <!-- forbidden process list ,notepad.exe-->
    <add key="kill_list" value=""/>


    <!-- 
    Misc Interface Settings
    *************************************************************************************************************
    -->

    <!-- false means application manager will not shutdown the kiosk app when escape is pressed-->
    <add key="ignore_keyboard_hooks" value="false"/>

    <!--  this may create double taps on touch enabled screens, but forces AM to recognize mouse events -->
    <add key="force_mouse" value="false"/>

    <!-- application manager will hide the mouse cursor over it's own visuals, kiosk app must still disable theirs also-->
    <add key="show_mouse" value="false"/>

    <!-- will close facebook and email after the set seconds-->
    <add key="popup_timeout_seconds" value="30"/>

    <add key="show_feedback_message" value="false"/>        
                                           
    <!-- 
    Diagnostics Logging - local and remote
    *************************************************************************************************************
    -->
    
     <!-- log frequency in minutes in which computer health will be recorded -->
    <add key="process_log_freq" value="5" />
    
    <!-- Applicatin manager server settings.
    If server_ipaddress is blank, then the client will use address in UDP broadcast messages
    sent by application manager server.  This will not work over firewalls.
    -->
    <add key="server_broadcast_port" value="6601" />
    <add key="server_ipaddress" value="" />
    
    <!-- remote server to send system health and diagostic updates 
    Updates will be sent based on the process_log_freq internal -->
    <add key="use_diagnostic_server" value="false" />
    <add key="diagnostic_server_endpoint" value="http://localhost:60000" />
    


    <!-- 
    Incoming communication and web services hosted by Application Manager
    *************************************************************************************************************
    -->

    <!-- if true, Application Manager will serve files over http port 6603 -->
    <add key="serve_files" value="false" />
    <add key="httpserver_basepath" value="webserver" />
    
    <!-- the port used for any http communication with modules, this includes web sockets-->
    <add key="webserver_port" value="6603" />
    
    <!-- total number of connections that can work with application manager at once -->
    <add key="max_comm_clients" value="10" />
    
    <!-- port used for persistent TCP communication with application manager's custom protocol-->
    <add key="api_port" value="6602" />
     
    <!-- Authentication to access application manager services over http.-->
    <add key="api_user" value="kiosk" />
    <add key="api_password" value="password" />
    
    <!-- 
    The version of this config file.  Application manager must be compatible
    *************************************************************************************************************
    -->
  
    <add key="config_version" value="4.4" />

  </appSettings>


  <system.data>
    <DbProviderFactories>
      <remove invariant="System.Data.SqlServerCe.4.0" />
      <add name="Microsoft SQL Server Compact Data Provider 4.0" invariant="System.Data.SqlServerCe.4.0" description=".NET Framework Data Provider for Microsoft SQL Server Compact" type="System.Data.SqlServerCe.SqlCeProviderFactory, System.Data.SqlServerCe, Version=4.0.0.1, Culture=neutral, PublicKeyToken=89845dcd8080cc91" />
    </DbProviderFactories>
  </system.data>

  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
  </startup>
  <runtime>
    <loadFromRemoteSources enabled="true"/>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <probing privatePath="plugins"/>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>
