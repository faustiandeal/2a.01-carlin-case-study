﻿using UnityEngine;
using System.Collections;
using TMPro;
using System.Collections.Generic;
using cortina.video;
using System;

public class CaptionComponent : AbstractVideoControl {

    [SerializeField]
    private TextMeshProUGUI captionText;

    private List<CaptionItem> captionData;

    public bool Paused { get; set; }
    private float currentCaptionTime;
    private int currentCaptionIndex;

    private void Update( ) {
        // Early exit if no captions
        if ( captionData == null || captionData.Count == 0 ) {
            return;
        }

        if ( !Paused ) {
            //currentCaptionTime += Time.deltaTime;
            //DisplayCurrentCaption( );
        }
    }

    public override void VideoPlayerStateChanged(VideoPlayerStateModel state)
    {
        if(state.CurrentAction == VideoPlayerActions.Progress)
        {
            SetCurrentTime(state.PositionSeconds);
        }
    }

    private void DisplayCurrentCaption( ) {
        if (captionData == null ||
            captionData.Count == 0)
            return;

        captionText.text = "";
        for ( int i = 0; i < captionData.Count; i++ )
        {
            if (currentCaptionTime >= captionData[ i ].StartTime && currentCaptionTime < captionData[i].EndTime)
            {
                captionText.text = captionData[i].Caption;
                currentCaptionIndex = i;
                break;
            }
        }
    }


    public void SetCaptionData( List<CaptionItem> data ) {
        captionText.text = "";
        captionData = data;
        Paused = true;
    }


    public void SetCurrentTime( float time ) {
        currentCaptionTime = time;
        DisplayCurrentCaption( );
    }


    public void StartCaptions( ) {
        currentCaptionTime = 0f;
        currentCaptionIndex = 0;
        Paused = false;
        DisplayCurrentCaption();
    }

    public override void Show()
    {
        
    }

    public override void Hide()
    {
       
    }
}