﻿//using UnityEngine;
//using System;
//using System.Collections;
//using System.Collections.Generic;

//public static class SRTParser {

//    public static List<CaptionItem> SRTToCaptions( string srtText ) {
//        List<CaptionItem> captionList = new List<CaptionItem>( );
        
//        // Split based on both possible newline formats
//        string[] lines = srtText.Split( new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries );

//        int lineIndex = 0;
//        while ( lineIndex < lines.Length ) {
//            CaptionItem newCaption = new CaptionItem( );

//            // Line 1 is caption number, ignore this b/c we don't need it
//            lineIndex++;

//            // Line 2 (first 12 chars) is caption display time, parse it into seconds
//            string[] timeStrings = lines[lineIndex].Split(new String[] { "-->" }, StringSplitOptions.None);

//            string startTime = timeStrings[0].Trim().Replace(',', '.');
//            string endTine = timeStrings[1].Trim().Replace(',', '.');

//            newCaption.StartTime = ( float )TimeSpan.Parse( startTime ).TotalSeconds;
//            newCaption.EndTime = (float)TimeSpan.Parse(endTine).TotalSeconds;
//            lineIndex++;

//            int parseResult;
//            // Line 3+ is the caption text, line can be parsed as int
//            //Debug.Log(" ******   Try Parse String as Int: " + int.TryParse(lines[lineIndex], out parseResult));
//            while ( lineIndex < lines.Length && !int.TryParse(lines[lineIndex], out parseResult)) { 
                
//                // Add newline if this is a multiline caption
//                if (newCaption.Caption.Length > 0 ) {
//                    newCaption.Caption += '\n';
//                }
//                newCaption.Caption += lines[ lineIndex++ ];
//            }

//            //lineIndex++;
//            captionList.Add( newCaption );
//        }

//        return captionList;
//    }
//}