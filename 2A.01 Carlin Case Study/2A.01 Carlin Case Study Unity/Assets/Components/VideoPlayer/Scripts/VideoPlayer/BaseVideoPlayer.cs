﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using cortina.video;
using RenderHeads.Media.AVProVideo;
using UnityEngine.EventSystems;

public class BaseVideoPlayer : MonoBehaviour, IVideoPlayer, IPointerDownHandler
{
    public event Action OnComplete;
    public event Action<VideoPlayerStateModel> StateChanged;
    public event Action VideoTouched;

    [Tooltip("None = No size change to rect, Both = auto size to the video files WxH, Height= auto height based on Apsect Ratio and the width of parent rect, Width is the same just based on the height")]
    public AutoSize AutoSizeOption;

    public CanvasGroup VideoCanvas;
    public VideoRenderer VideoRendererComponent;

    [SerializeField]
    private DisplayUGUI display;

    public bool Loop;

    public bool isPlaying { get; set; }

    public float DurationSeconds { get { return VideoRendererComponent.DurationSeconds; } }
    public float PositionSeconds { get { return VideoRendererComponent.PositionSeconds; } }

    public float NormalizedPosition
    {
        get
        {
            return VideoRendererComponent.NormalizedPosition;
        }
        set
        {
            VideoRendererComponent.NormalizedPosition = value;
        }
    }

    private VideoPlayerStateModel stateModel = new VideoPlayerStateModel();
    public VideoPlayerStateModel StateModel
    {
        get
        {
            return stateModel;
        }
    }

    protected VideoPlayerStateModel UpdateStateModel(VideoPlayerActions action)
    {
        stateModel.State = CurrentState;
        stateModel.Progress = progress;
        stateModel.Duration = DurationSeconds;
        stateModel.PositionSeconds = PositionSeconds;
        stateModel.CurrentAction = action;
        if (StateChanged != null) StateChanged(stateModel);
        return stateModel;
    }

    private float progress
    {
        get
        {
            return NormalizedPosition;
        }

        set
        {

        }
    }

    private bool isFullScreen = false;

    #region Mono Methods

    public virtual void Start()
    {
        VideoRendererComponent.Completed += VideoCompleteHandler;
        VideoRendererComponent.StateChanged += StateChangedHandler;
    }

    public virtual void Update()
    {
        if (VideoRendererComponent.IsVideoLoaded)
        {
            progress = VideoRendererComponent.NormalizedPosition;
            UpdateStateModel(VideoPlayerActions.Progress);
        }
    }
    #endregion

    #region Screen Methods

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public void FadeVideo(float alpha, float fadeTime = 0.5f)
    {
        LeanTween.cancel(VideoCanvas.gameObject);
        LeanTween.alphaCanvas(VideoCanvas, alpha, fadeTime);
    }
    #endregion

    #region VideoPlayerMethods
    public virtual void Load(string video, MediaPlayer.FileLocation location = MediaPlayer.FileLocation.RelativeToProjectFolder, bool autoPlay = false)
    {
        VideoRendererComponent.CanLoop = Loop;
        VideoRendererComponent.LoadVideo(video, location, autoPlay);
    }

    public virtual void UnloadVideo()
    {
        VideoRendererComponent.Reset();
    }

    public virtual void SetUnloadedTexture(Texture loadingTexture)
    {
        display._noDefaultDisplay = false;
        display._defaultTexture = loadingTexture;
    }

    public virtual void Play()
    {
        isPlaying = true;
        VideoRendererComponent.Play();
    }
    public virtual void Pause()
    {
        isPlaying = false;
        VideoRendererComponent.Pause();
    }

    public virtual void Stop()
    {
        progress = 0;
        isPlaying = false;
        VideoRendererComponent.Rewind();
    }

    public virtual void Replay()
    {
        Stop();
        Play();
    }

    public virtual void Reset()
    {
        progress = 0;
        isPlaying = false;
        VideoRendererComponent.Reset();
    }

    public VideoPlayerState CurrentState
    {
        get
        {
            return VideoRendererComponent.CurrentState;
        }
    }

    #endregion

    #region EventHandlers

    public virtual void StateChangedHandler()
    {
        Debug.Log("<color=lime>{VideoPlayer}Renderer state changed</color>" + CurrentState, gameObject);

        switch (CurrentState)
        {
            case VideoPlayerState.Playing:
                UpdateStateModel(VideoPlayerActions.Play);
                break;

            case VideoPlayerState.Paused:
                UpdateStateModel(VideoPlayerActions.Pause);
                break;

            case VideoPlayerState.Stopped:
                UpdateStateModel(VideoPlayerActions.Stop);
                break;

            case VideoPlayerState.Ready:
                UpdateStateModel(VideoPlayerActions.None);
                break;

            case VideoPlayerState.Loading:
                UpdateStateModel(VideoPlayerActions.None);
                break;
        }

        isPlaying = CurrentState == VideoPlayerState.Playing;

        if (CurrentState == VideoPlayerState.Ready ||
            CurrentState == VideoPlayerState.Playing)
        {
            SetRectSize(AutoSizeOption);
        }
    }

    public virtual void VideoCompleteHandler()
    {
        isPlaying = false;
        Debug.Log("<color=lime>{VideoPlayer} Video Completed </color>" + CurrentState);
        if (OnComplete != null)
        {
            OnComplete();
        }
    }

    #endregion

    public RectTransform videoScaleRect;
    #region Custom Methods
    private void SetRectSize(AutoSize autoSize)
    {
        Debug.Log("Setting Size...");

        float aspectRatio = VideoRendererComponent.AspectRatio;
        Vector2 n = new Vector2();

        RectTransform rectTrans = videoScaleRect;
        if (rectTrans == null) rectTrans =  GetComponent<RectTransform>();

        switch (autoSize)
        {
            case AutoSize.Both:
                {
                    n = VideoRendererComponent.VideoSize;
                    rectTrans.sizeDelta = n;
                    break;
                }
            case AutoSize.Width:
                {
                    n.y = rectTrans.sizeDelta.y;
                    n.x = rectTrans.sizeDelta.y * aspectRatio;
                    rectTrans.sizeDelta = n;
                    break;
                }
            case AutoSize.Height:
                {
                    n.y = rectTrans.sizeDelta.x / aspectRatio;
                    n.x = rectTrans.sizeDelta.x;
                    rectTrans.sizeDelta = n;
                    break;
                }
            case AutoSize.None:
                {

                    break;
                }

        }


    }

    public void FullScreen()
    {
        if (!isFullScreen)
        {
            RectTransform rectTrans = GetComponent<RectTransform>();
            rectTrans.sizeDelta = new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
            isFullScreen = true;
        }
        else
        {
            SetRectSize(AutoSizeOption);
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (VideoTouched != null) VideoTouched();
    }
    #endregion
}

public enum AutoSize
{
    Both, Height, Width, None
}

public struct VideoPlayerStateModel
{
    public VideoPlayerState State;
    public VideoPlayerActions CurrentAction;
    public float Duration { get; set; }
    public float Progress { get; set; }
    public float PositionSeconds { get; set; }
    //Volume
}

public enum VideoPlayerState
{
    Playing, Stopped, Loading, Ready, Paused
}

public enum VideoPlayerActions
{
    None, Play, Pause, Stop, Volume, Progress, Interact
}