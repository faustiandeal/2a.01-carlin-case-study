﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;

public interface IVideoPlayer
{
    void Play();
    void Pause();
    void Reset();
    void Replay();
    void Stop();
    void Load(string video, MediaPlayer.FileLocation location, bool autoPlay);
}
