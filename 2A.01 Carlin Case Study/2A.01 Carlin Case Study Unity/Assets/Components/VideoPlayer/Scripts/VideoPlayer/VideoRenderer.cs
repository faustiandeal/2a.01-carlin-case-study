﻿using UnityEngine;
using UnityEngine.UI; 
using System;
using System.Collections;
using System.Collections.Generic;
using RenderHeads.Media.AVProVideo;

public class VideoRenderer : MonoBehaviour
{
    private MediaPlayer localVideoPlayer
    {
        get
        {
            return GetComponent<MediaPlayer>();
        }
    }
    private DisplayUGUI avproUGUIcomponent; 
    public event Action StateChanged; 
    public event Action Completed;
    private VideoPlayerState currentState = VideoPlayerState.Stopped; 
    public VideoPlayerState CurrentState
    {
        get
        {
            return currentState; 
        } 
        private set
        {
            currentState = value;
            if (StateChanged != null)
            {
                StateChanged();
            }
        }
    }
    
    public bool CanLoop
    {
        get
        {
            return localVideoPlayer.m_Loop; 
        }
        set
        {
            localVideoPlayer.m_Loop = value; 
        }
    }
    
    public bool IsVideoPlaying
    {
        get
        {
            //Debug.Log("<color=orange>{VideoScrubber}Is Playing </color>"+ localVideoPlayer.MovieInstance.IsPlaying);
            return localVideoPlayer.Control.IsPlaying();
        }
    }
   
    public bool IsVideoLoaded { get; private set;  }

    public float PositionSeconds
    {
        get
        {
            
            return localVideoPlayer.Control != null? localVideoPlayer.Control.GetCurrentTimeMs() / 1000 :0; 
        }
        set
        {
            localVideoPlayer.Control.SeekFast( value * 1000);
        }
    }

    public float DurationSeconds
    {
        get
        {
            return localVideoPlayer.Info != null ? localVideoPlayer.Info.GetDurationMs() / 1000 : 0;
        }
    }

    public float NormalizedPosition
    {
        get
        {
            float position = (localVideoPlayer.Info != null && localVideoPlayer.Control != null) ? localVideoPlayer.Control.GetCurrentTimeMs() / localVideoPlayer.Info.GetDurationMs():0;
            return position;
        }
        set
        {
            if (localVideoPlayer.Info != null)
            {
                float newFramePosition = localVideoPlayer.Info.GetDurationMs() * value;
                localVideoPlayer.Control.SeekFast(newFramePosition);
            }
            
        }
    }

    /*public int PositionFrames
    {
        get
        {
            return localVideoPlayer.MovieInstance != null ? (int)localVideoPlayer.MovieInstance.PositionFrames : 0;
        }

        set
        {
            localVideoPlayer.MovieInstance.PositionFrames = (uint)value;
        }
    }*/

    public float AspectRatio
    {
        get
        {
            IMediaInfo info = localVideoPlayer.Info;
            if(info == null || (info.GetVideoWidth() == 0 || info.GetVideoHeight() == 0))
            {
                return 1; 
            }
            float width = info.GetVideoWidth();
            float height = info.GetVideoHeight();
            return width / height; 
        }
    }

    public Vector2 VideoSize
    {
        get
        {
            return new Vector2((float)localVideoPlayer.Info.GetVideoWidth(), (float)localVideoPlayer.Info.GetVideoHeight()); 
        }
    }

    private string currentFile; 

    private bool videoComplete = false;

    #region Monobehavior Methods
    void Awake()
    {
        
        localVideoPlayer.m_AutoStart = false;
        localVideoPlayer.m_AutoOpen = false;
        localVideoPlayer.Events.AddListener(OnVideoEvent);

        avproUGUIcomponent = GetComponent<DisplayUGUI>();
        avproUGUIcomponent.CurrentMediaPlayer = localVideoPlayer;
        currentState = VideoPlayerState.Stopped; 
       
    }

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        //Debug.Log(et);
        switch (et)
        {
            case MediaPlayerEvent.EventType.ReadyToPlay:
                CurrentState = VideoPlayerState.Ready;
                break;
            case MediaPlayerEvent.EventType.FinishedPlaying:
                if (!videoComplete)
                {
                    videoComplete = true;
                    CurrentState = VideoPlayerState.Stopped;

                    Debug.Log("<color=yellow>[VideoRenderer]Video Complete </color>");
                    if (Completed != null)
                    {
                        Completed();
                    }
                }
                break;

            case MediaPlayerEvent.EventType.Started:
                CurrentState = VideoPlayerState.Playing;
                break;
        }
        Debug.Log("Event: " + et.ToString());
    }

    /*void Start()
    {

    }

    private void Update()
    {
        if(IsVideoLoaded)
        {
            if(localVideoPlayer.Control. && !videoComplete)
            {
                videoComplete = true;
                CurrentState = VideoScrubberState.Stopped; 
                Debug.Log("<color=yellow>[VideoRenderer]Video Complete </color>");
                if(Completed != null)
                {
                    Completed(); 
                }
                
            }
        }
    }*/

    //private void OnRenderObject()
    //{
    //    avproUGUIcomponent.SetVerticesDirty();
    //}

    #endregion

    #region VideoPlayer Methods
    public void Play()
    {
        localVideoPlayer.Play();
    }

    public void Pause()
    {
        localVideoPlayer.Pause();
        CurrentState = VideoPlayerState.Paused;
    }
    
    public void Rewind()
    {
        if( localVideoPlayer.Control == null)
        { return; }
        CurrentState = VideoPlayerState.Stopped;
        localVideoPlayer.Control.Rewind();
        videoComplete = false; 
    }

    public void Restart()
    {
        Rewind();
        Play();
    }

    public void LoadVideo(string filename, MediaPlayer.FileLocation location, bool autoPlay = false)
    {
        Debug.Log("<color=yellow>[VideoRenderer]LoadVideo</color>");
        CurrentState = VideoPlayerState.Loading;
        if (currentFile == filename)
        {
            Rewind();

            if (autoPlay)
            {
                Play();
                CurrentState = VideoPlayerState.Playing;
            }
            return;
        }

        currentFile = filename;
        IsVideoLoaded = localVideoPlayer.OpenVideoFromFile(location, filename, autoPlay);

        //If we loaded something, reset videoComplete to allow the message to be resent.
        if (IsVideoLoaded)
            videoComplete = false;

        /*IsVideoLoaded = avproUGUIcomponent.m_movie.LoadMovie(autoPlay);
        if (!IsVideoLoaded)
        {
            avproUGUIcomponent.m_movie._colourFormat = avproUGUIcomponent.m_movie._colourFormat ==  AVProWindowsMediaMovie.ColourFormat.RGBA32? AVProWindowsMediaMovie.ColourFormat.YCbCr_HD: AVProWindowsMediaMovie.ColourFormat.RGBA32;
            IsVideoLoaded = avproUGUIcomponent.m_movie.LoadMovie(autoPlay);
        }*/
        
        Debug.Log("<color=yellow>[VideoRenderer]LoadVideo at the end! </color>" + IsVideoLoaded);
    }

    /*public void LoadVideo(string filename, FileLocation location, bool autoPlay = false)
    {
        localVideoPlayer._folder = folder;
        LoadVideo(filename,  autoPlay); 
    }*/

    public void Reset()
    {
        CurrentState = VideoPlayerState.Stopped;
        IsVideoLoaded = false;
        localVideoPlayer.CloseVideo();
        currentFile = ""; 
        
    }
    #endregion


}

