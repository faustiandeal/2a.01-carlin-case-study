﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cortina.video
{
    public abstract class AbstractVideoControl : MonoBehaviour
    {

        public List<VideoPlayerActions> UpdateAction = new List<VideoPlayerActions>();

        public event Action<VideoPlayerActionRequest> StateChangeRequested;
        //public event Action<VideoPlayerActions> VideoControlEvent;

        public virtual void VideoPlayerStateChanged(VideoPlayerStateModel state)
        {
            
        }

        protected void RequestPlayerAction(VideoPlayerActions action, object data = null)
        {
            if (StateChangeRequested != null) StateChangeRequested(new VideoPlayerActionRequest { Action = action, Data = data });
        }

        public abstract void Show();
        public abstract void Hide();
    }
}

public class VideoPlayerActionRequest
{
    public VideoPlayerActions Action;
    public object Data;
}


