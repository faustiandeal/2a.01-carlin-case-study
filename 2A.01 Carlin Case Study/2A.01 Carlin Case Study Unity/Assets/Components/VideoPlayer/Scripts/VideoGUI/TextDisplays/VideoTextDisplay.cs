﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;

namespace cortina.video
{
    public class VideoTextDisplay : AbstractVideoControl
    {
        public string FormattedText = "{0:D2}:{1:D2}:{2:D2}";
        public VideoTextType Type;
        private TextMeshProUGUI textElement;

        public string TextDisplay
        {
            get
            {
                if (textElement == null)
                    textElement = GetComponent<TextMeshProUGUI>();

                return textElement.text;
            }
            set
            {
                if (textElement == null)
                    textElement = GetComponent<TextMeshProUGUI>();

                textElement.text = value;
            }
        }

        public override void Hide()
        {
            //TextDisplay = SecondsToTime(0f);
        }

        public override void Show()
        {
            
        }

        public override void VideoPlayerStateChanged(VideoPlayerStateModel model)
        {
            switch(Type)
            {
                case VideoTextType.Duration:
                    TextDisplay = SecondsToTime((float)model.Duration);
                    break;

                case VideoTextType.Position:
                    TextDisplay = SecondsToTime((float)model.PositionSeconds);
                    break;
            }
        }

        string SecondsToTime(float seconds)
        {
            TimeSpan t = TimeSpan.FromSeconds(seconds);

            return string.Format(FormattedText,
                                t.Hours,
                                t.Minutes,
                                t.Seconds);
        }
    }
}

public enum VideoTextType
{
    Duration, Position
}