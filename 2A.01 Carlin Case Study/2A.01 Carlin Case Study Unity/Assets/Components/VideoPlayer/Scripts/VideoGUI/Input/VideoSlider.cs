﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace cortina.video
{
    [RequireComponent(typeof(Slider))]
    public class VideoSlider : AbstractVideoControl, IPointerDownHandler, IPointerUpHandler
    {
        private Slider slider;

        [SerializeField]
        private Image HandleImage;

        [SerializeField]
        private Image FillImage;

        public Sprite BaseHandleSprite;
        public Sprite HiddenHandleSprite;
        public Sprite TouchingHandleSprite;

        //private Vector2 minimumSize = new Vector2(18.5f, 23.6f);
        //private Vector2 maximumSize = new Vector2(35.8f, 64.3f);
        private bool isPlaying;
        private float progress;
        public bool CanPlayOnRelease;

        void Awake()
        {
            slider = GetComponent<Slider>();
            slider.onValueChanged.AddListener(delegate { ValueChanged(); });
            HandleImage.sprite = BaseHandleSprite;
        }

        public void ValueChanged()
        {
            if(progress != slider.value)
            {
                RequestPlayerAction(VideoPlayerActions.Progress, slider.value);
            }
        }

        public override void Show()
        {
            HandleImage.sprite = TouchingHandleSprite;
            //LeanTween.size(HandleImage.rectTransform, maximumSize, 1.0f);
        }

        public override void Hide()
        {
            HandleImage.sprite = BaseHandleSprite;
            //LeanTween.size(HandleImage.rectTransform, minimumSize, 1.0f);
        }

        public override void VideoPlayerStateChanged(VideoPlayerStateModel state)
        {
            isPlaying = state.State == VideoPlayerState.Playing;
            progress = state.Progress;
            slider.value = progress;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //HandleImage.sprite = TouchingHandleSprite;

            RequestPlayerAction(VideoPlayerActions.Interact);

            if (isPlaying)
            {
                Debug.Log("<color=yellow>{Video Slider} Slider Pressed!</color>");
                RequestPlayerAction(VideoPlayerActions.Pause );
                CanPlayOnRelease = true;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            //HandleImage.sprite = BaseHandleSprite;

            if (CanPlayOnRelease)
            {
                CanPlayOnRelease = false;
                RequestPlayerAction( VideoPlayerActions.Play );
                Debug.Log("<color=yellow>{Video Slider} Slider Released!</color>");
            }
        }

        public void SetFillColor(Color c)
        {
            FillImage.color = c;
        }
    }
}
