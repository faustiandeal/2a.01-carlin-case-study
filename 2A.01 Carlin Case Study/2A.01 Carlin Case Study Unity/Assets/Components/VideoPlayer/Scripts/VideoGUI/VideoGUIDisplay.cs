﻿using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
namespace cortina.video
{
    public class VideoGUIDisplay : MonoBehaviour
    {
        public BaseVideoPlayer VideoPlayer;

        public CanvasGroup hideableControls;
        Dictionary<VideoPlayerActions, List<AbstractVideoControl>> VideoActionQueue = new Dictionary<VideoPlayerActions, List<AbstractVideoControl>>();

        void Awake()
        {
            AbstractVideoControl[] controls = GetComponentsInChildren<AbstractVideoControl>();

            foreach (AbstractVideoControl control in controls)
            {
                control.StateChangeRequested += VideoControlEventHandler;
                AddControlToQueue(control);
            }

            if (VideoPlayer != null)
            {
                SetVideoPlayer(VideoPlayer);
            }
        }

        public void Show()
        {
            AbstractVideoControl[] controls = GetComponentsInChildren<AbstractVideoControl>();
            foreach (AbstractVideoControl control in controls)
            {
                control.Show();
            }
        }

        public void Hide()
        {
            AbstractVideoControl[] controls = GetComponentsInChildren<AbstractVideoControl>();
            foreach (AbstractVideoControl control in controls)
            {
                control.Hide();
            }
        }

        public void Reset()
        {
            Hide();
        }

        public void SetVideoPlayer(BaseVideoPlayer videoPlayer)
        {
            VideoPlayer = videoPlayer;
            VideoPlayer.StateChanged += VideoPlayerStateChanged;
            VideoPlayer.VideoTouched += VideoPlayer_VideoTouched;
        }

        private void VideoPlayerStateChanged(VideoPlayerStateModel model)
        {
            VideoControlStateChange(model);
        }

        private void VideoControlStateChange(VideoPlayerStateModel model)
        {
            if (VideoActionQueue.ContainsKey(model.CurrentAction))
            {
                foreach (AbstractVideoControl control in VideoActionQueue[model.CurrentAction])
                {
                    control.VideoPlayerStateChanged(model);
                }
            }
        }

        private void VideoControlEventHandler(VideoPlayerActionRequest request)
        {
            switch (request.Action)
            {
                case VideoPlayerActions.Play:
                    VideoPlayer.Play();
                    break;
                case VideoPlayerActions.Pause:
                    VideoPlayer.Pause();
                    break;
                case VideoPlayerActions.Stop:
                    VideoPlayer.Stop();
                    break;
                case VideoPlayerActions.Progress:
                    VideoPlayer.NormalizedPosition = (float)request.Data;
                    break;

                case VideoPlayerActions.Interact:
                    Show();
                    break;
            }
        }

        private void AddControlToQueue(AbstractVideoControl control)
        {
            foreach (VideoPlayerActions action in control.UpdateAction)
            {
                if (VideoActionQueue.ContainsKey(action))
                {
                    if (!VideoActionQueue[action].Contains(control))
                    {
                        VideoActionQueue[action].Add(control);
                    }
                }
                else
                {
                    List<AbstractVideoControl> temp = new List<AbstractVideoControl>();
                    temp.Add(control);
                    VideoActionQueue.Add(action, temp);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Show();
        }

        private void VideoPlayer_VideoTouched()
        {
            Show();
        }
    }
}