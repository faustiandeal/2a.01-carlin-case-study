﻿using UnityEngine;
using UnityEngine.UI; 
using System;
using System.Collections;
using System.Collections.Generic;

namespace cortina.video
{
    public class ProgressBar : AbstractVideoControl
    {
        public Color FillColor;

        public virtual float Progress
        {
            get
            {
                float p = FillRect.sizeDelta.x / GetComponent<RectTransform>().sizeDelta.x;
                return p;
            }
            set
            {
                float p = GetComponent<RectTransform>().rect.width * value;
                FillRect.sizeDelta = new Vector2(p, FillRect.sizeDelta.y);
            }
        }

        public RectTransform FillRect;

        public override void VideoPlayerStateChanged(VideoPlayerStateModel state)
        {
            base.VideoPlayerStateChanged(state);
            Progress = state.Progress;
        }

        public virtual void Awake()
        {
            FillRect.GetComponent<Image>().color = FillColor;
        }

        public override void Show()
        {
        }

        public override void Hide()
        {
        }
    }
}