﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoPlayerTest : MonoBehaviour {

	public BaseVideoPlayer player = new BaseVideoPlayer();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnGUI() {
		if(GUI.Button(new Rect(0f, 0f, 100f, 20f), "Load Video"))
		{
            //player.Load("AVProVideoSamples/BigBuckBunny_720p30.mp4", RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder);
            player.Load("CMS_data/cache/media/id_485_8016_165_Monologue_George_Carlin_-_SNL_TEMP.mov", RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL);
		}

		if(GUI.Button(new Rect(0f, 30f, 100f, 20f), "Play Video"))
		{
			player.Play();
		}
	}
}
