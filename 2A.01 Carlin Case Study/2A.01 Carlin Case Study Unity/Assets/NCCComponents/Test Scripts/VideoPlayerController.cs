﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using NCCExternalComponentScripts.Wrappers;

namespace TestingScripts
{
    public class VideoPlayerController : MonoBehaviour
    {
        public event Action ClosedElement;

        public VideoUIWrapper VideoUI;

        [SerializeField]
        public UnityEngine.Video.VideoPlayer LocalVideoPlayer;
        [SerializeField]
        private Button CloseButton;

        [SerializeField]
        private Image CloseButtonBackground;
        private bool isPlaying = false;
        private string currentFile;

        public float PositionSeconds
        {
            get
            {
                return (float)LocalVideoPlayer.time;
            }
            set
            {
                LocalVideoPlayer.time = value;
            }
        }

        public float NormalizedPosition
        {
            get
            {
                return (float)LocalVideoPlayer.frame / (float)LocalVideoPlayer.frameCount;
            }
            set
            {
                //Debug.Log("Frame Before: " + LocalVideoPlayer.frame);
                LocalVideoPlayer.frame = (long)(LocalVideoPlayer.frameCount * value);
                //Debug.Log("Frame After: " + LocalVideoPlayer.frame + " Value: " + value);
            }
        }

        private bool isDraggingScrubber = false;


        #region Unity Methods
        void Awake()
        {
            CloseButton.onClick.AddListener( () =>
            {
                if (ClosedElement != null)
                {
                    ClosedElement();
                }
            });


            //LocalVideoPlayer.Events.AddListener(VideoPlayerEventHandler); 
        }
        void Start()
        {

            VideoUI.ScrubberGrabbed += () =>
            {
                isDraggingScrubber = true;
                Pause();
            };

            VideoUI.ScrubberReleased += () =>
            {
                isDraggingScrubber = false;
                Play();
            };
        }

        private void Update()
        {
            if (!isDraggingScrubber)
            {
                VideoUI.ProgressValue = NormalizedPosition;
            }
            else
            {
                NormalizedPosition = VideoUI.ProgressValue;
            }
        }

        #endregion
        #region Interface Methods
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void LoadMedia(string media)
        {

        }
        #endregion

        #region VideoPlayer Methods
        public void Play()
        {
            isPlaying = true;
            LocalVideoPlayer.Play();
        }

        public void Pause()
        {
            isPlaying = false;
            LocalVideoPlayer.Pause();
        }

        public void Rewind()
        {
            // if( LocalVideoPlayer.Control == null)
            // { return; }
            // LocalVideoPlayer.Control.Rewind();
        }

        public void Restart()
        {
            Rewind();
            Play();
        }

        public RenderTexture targetVideoTexture;

        public void LoadVideo(string mediaPath, bool v, Color backgroundColor)
        {
            LoadVideo(mediaPath, v);
            CloseButtonBackground.color = backgroundColor;
            VideoUI.BackGroundColor = backgroundColor;
        }

        public void LoadVideo(string filename, bool autoPlay = false)
        {
            gameObject.SetActive(true);

            // set first frame to black
            RenderTexture rt = UnityEngine.RenderTexture.active;
            UnityEngine.RenderTexture.active = targetVideoTexture;
            GL.Clear(true, true, UnityEngine.Color.black);
            UnityEngine.RenderTexture.active = rt;


            if (currentFile == filename)
            {
                Restart();
                return;
            }
            else
            {
                isPlaying = true;
                LocalVideoPlayer.url = Application.streamingAssetsPath + @"/" + filename;
                LocalVideoPlayer.Play();
                // LocalVideoPlayer.OpenVideoFromFile(location, filename, autoPlay);
            }
            currentFile = filename;
        }

        public void Reset()
        {
            LocalVideoPlayer.Stop();
            //LocalVideoPlayer.CloseVideo();
            currentFile = "";
        }

        // private void VideoPlayerEventHandler(MediaPlayer player, MediaPlayerEvent.EventType type, ErrorCode code)
        // {
        //     if(type == MediaPlayerEvent.EventType.ReadyToPlay)
        //     {
        //         Play();
        //     }
        // }

        #endregion
    }
}