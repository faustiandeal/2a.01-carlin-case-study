﻿using NCCExternalComponentScripts.Proxies;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class TimeoutTester : MonoBehaviour
    {

        public TimeoutWrapper timeout;

        private void Awake()
        {
            timeout.TimedOut += () =>
            {
                Debug.Log("Timed out exited");
            };

            timeout.NewUserButtonPressed += () =>
            {
                Debug.Log("Time out New User Pressed");
            };

            timeout.enabled = true;
        }

        // Update is called once per frame
        void Update()
        {

            timeout.enabled = true;

            if (Input.GetKeyDown(KeyCode.S))
            {
                timeout.StartTimeout();
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                timeout.EndTimeout();
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                timeout.ResetTimeout();
            }
        }
    }

}
