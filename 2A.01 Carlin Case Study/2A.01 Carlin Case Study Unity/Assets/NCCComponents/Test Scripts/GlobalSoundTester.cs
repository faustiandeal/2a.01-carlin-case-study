﻿using NCCExternalComponentScripts.Proxies;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class GlobalSoundTester : MonoBehaviour
    {

        public GlobalSoundsComponent globalSounds;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                globalSounds.PlayAudioClip(GlobalSounds.TapRFID);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                globalSounds.PlayAudioClip(GlobalSounds.BadgeScaleUp);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                globalSounds.PlayAudioClip(GlobalSounds.BadgeToCorner);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                globalSounds.PlayAudioClip(GlobalSounds.ContentLiked);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                globalSounds.PlayAudioClip(GlobalSounds.DidYouForgetToTapIn);
            }
        }
    }

}
