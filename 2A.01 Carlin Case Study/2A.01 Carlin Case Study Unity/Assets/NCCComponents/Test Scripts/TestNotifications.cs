﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class InteractiveNotifications
    {
        public static string VisitorAcquired = "VisitorAcquired";
        public static string AnimateSavedToProfile = "AnimateSavedToProfile";
        public static string AnimateContentLiked = "AnimateLikedContent";
        public static string ExitAttract = "ExitAttract";
        public static string EnterAttract = "EnterAttract";
        public static string Reset = "Reset";
    }
}

