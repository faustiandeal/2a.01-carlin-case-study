﻿using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
	This script is used to test the different VideoPlayer components and is 
	also an example to show how these other components may be used.
 */
namespace TestingScripts
{
    public class VideoUITester : MonoBehaviour
    {

        public VideoPlayerController videoPlayerController;

        public VideoUIWrapper VideoUI;

        public bool playVideo = true;

        public GameObject progressBarGameObject;

        public string srtfilePath;

        private bool firstPlayFlag = true;
        // Use this for initialization
        void Start()
        {
            if (srtfilePath == null)
            {
                Debug.Log("Set valid srtFile path");
            }
            else
            {
                VideoUI.SetCaptionData(SRTParser.FilePathToCaptions(srtfilePath));
            }

            if(playVideo)
            {
                videoPlayerController.Play();
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (videoPlayerController.LocalVideoPlayer.isPlaying)
            {
                

                //VideoUI.ProgressValue = videoPlayerController.PositionSeconds / videoPlayerController.DurationSeconds;

                if (firstPlayFlag)
                {
                    firstPlayFlag = false;
                    VideoUI.StartCaptions();
                }
                VideoUI.SetCurrentTime(videoPlayerController.PositionSeconds);
                //Debug.Log(videoPlayerController.PositionSeconds);
            }
            else
            {
                //VideoUI.
            }
        }
    }
}
