﻿using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardWrapperTester : MonoBehaviour {

    public KeyboardWrapper keyboard;
	// Use this for initialization
	void Start () {

        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.S))
        {
            keyboard.SetText("");
        }
        else if(Input.GetKeyDown(KeyCode.P))
        {
            keyboard.Keyboard.SetPredictiveText(new string[3] { "@hotmail.com", "@gmail.com", "@yahoo.com" });
        }
    }
}
