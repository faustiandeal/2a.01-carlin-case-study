﻿using cortina.architecture;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using static NCCExternalComponentScripts.Wrappers.AttractWrapper;

namespace TestingScripts
{
    
    public class AttractMediationTester : MonoBehaviour
    {
        public enum TapIconLocations
        {
            Left, Below, Right
        }
        
        public Sprite visitorTestSprite;
        public string NameText = "Test Name";
        public TapIconLocations TapIconLocation = TapIconLocations.Below;

        private BadgeData data;

        public AttractWrapper attract;
        Facade facade;

        // Use this for initialization
        void Start()
        {

            data = new BadgeData() { ProfileName = NameText, ProfileImage = visitorTestSprite };

            facade = new Facade();

            facade.RegisterMediator(new AttractWrapperMediator() { Component = attract });

            facade.Initialize();

            //facade.SendNotification(InteractiveNotifications.Reset);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                //attract.AquireVisitor(data);
                facade.SendNotification(InteractiveNotifications.VisitorAcquired, data);
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                //attract.AnimateContentLiked();
                facade.SendNotification(InteractiveNotifications.AnimateContentLiked, "Liked!");
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                //attract.AnimateSavedToProfile(); 
                facade.SendNotification(InteractiveNotifications.AnimateSavedToProfile);
            }
            else if (Input.GetKeyDown(KeyCode.R))
            {
                attract.Show();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                attract.AnimateBadgeYPosition(true, 0);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                attract.AnimateBadgeYPosition(false, 500);
            }
            else if (Input.GetKeyDown(KeyCode.T))
            {
                switch (TapIconLocation)
                {
                    case TapIconLocations.Below:
                        attract.SetTapLocation(AttractWrapper.TapIconLocations.Below);
                        break;

                    case TapIconLocations.Right:
                        attract.SetTapLocation(AttractWrapper.TapIconLocations.Right);
                        break;

                    case TapIconLocations.Left:
                        attract.SetTapLocation(AttractWrapper.TapIconLocations.Left);
                        break;
                }
                
            }
        }
    }
}
