﻿using NCCExternalComponentScripts.Proxies;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class BadgeTester : MonoBehaviour
    {

        public BadgeWrapper BadgeWrapper;
        public Sprite VisitorTestSprite;
        public string NameText = "Test Name";

        private BadgeData data;

        private void Awake()
        {
            data = new BadgeData() { ProfileImage = VisitorTestSprite, ProfileName = NameText };
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                BadgeWrapper.SetBadgeData(data);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                BadgeWrapper.ShowFrame(1);
            }
            else if (Input.GetKeyDown(KeyCode.H))
            {
                BadgeWrapper.HideFrame(1);
            }
        }
    }

}
