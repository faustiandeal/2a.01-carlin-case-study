﻿using NCCExternalComponentScripts.Proxies;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class AttractTester : MonoBehaviour
    {

        public AttractWrapper attract;
        public Sprite visitorTestSprite;
        public string NameText = "Test Name";

        private BadgeData data;

        private void Awake()
        {
            attract.Ended += () =>
            {
                Debug.Log("Attract exited");
            };

            attract.Reset += () =>
            {
                Debug.Log("reset ressed");
                attract.Show();
            };

            data = new BadgeData() { ProfileImage = visitorTestSprite, ProfileName = NameText };

            //attract.Hide();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                attract.AquireVisitor(data);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                attract.Show();
            }
            else if (Input.GetKeyDown(KeyCode.H))
            {
                attract.Hide();
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                attract.AnimateSavedToProfile();
            }
            else if (Input.GetKeyDown(KeyCode.L))
            {
                attract.AnimateContentLiked();
            }
        }
    }

}
