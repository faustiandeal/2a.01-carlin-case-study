﻿using cortina.architecture;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class AttractWrapperMediator : Mediator<AttractWrapper>
    {
        public override void Initialize()
        {
            RegisterListener(InteractiveNotifications.VisitorAcquired, (o) =>
            {
                component.AquireVisitor((BadgeData)o);
            });
            RegisterListener(InteractiveNotifications.AnimateSavedToProfile, (o) =>
            {
                component.AnimateSavedToProfile((string)o);
            });
            RegisterListener(InteractiveNotifications.AnimateContentLiked, (o) =>
            {
                component.AnimateContentLiked((string)o);
            });
            RegisterListener(InteractiveNotifications.Reset, (o) =>
            {
                component.Show();
            });
            RegisterListener(InteractiveNotifications.ExitAttract, (o) =>
            {
                component.Hide();
            });

            component.Ended += () => {
                SendNotification(InteractiveNotifications.ExitAttract);
            };

            component.Reset += () => {
                SendNotification(InteractiveNotifications.Reset);
            };
        }
    }

}
