﻿using cortina.architecture;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class TimeoutWrapperMediator : Mediator<TimeoutWrapper>
    {

        public override void Initialize()
        {
            RegisterListener(InteractiveNotifications.Reset, (o) =>
            {
                component.SetProfileName("");
                component.EndTimeout();
                component.HideTimeout();
            });
            RegisterListener(InteractiveNotifications.ExitAttract, (o) =>
            {
                component.StartTimeout();
            });

            component.TimedOut += () => {
                SendNotification(InteractiveNotifications.Reset);
            };

            component.NewUserButtonPressed += () => {
                SendNotification(InteractiveNotifications.Reset);
            };
        }
    }
}

