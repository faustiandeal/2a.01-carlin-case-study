﻿using cortina.architecture;
using NCCExternalComponentScripts.Wrappers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestingScripts
{
    public class AttractTimeoutTester : MonoBehaviour
    {

        public Sprite visitorTestSprite;
        public string NameText = "Test Name";

        private BadgeData data;

        public AttractWrapper attract;
        public TimeoutWrapper timeout;
        Facade facade;

        // Use this for initialization
        void Start()
        {

            data = new BadgeData() { ProfileName = NameText, ProfileImage = visitorTestSprite };

            facade = new Facade();

            facade.RegisterMediator(new AttractWrapperMediator() { Component = attract });
            facade.RegisterMediator(new TimeoutWrapperMediator() { Component = timeout });

            facade.Initialize();

            facade.SendNotification(InteractiveNotifications.Reset);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                attract.AquireVisitor(data);
                timeout.SetProfileName(data.ProfileName);
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                attract.AnimateSavedToProfile();
            }
            else if (Input.GetKeyDown(KeyCode.T))
            {
                attract.AnimateContentLiked();
            }
        }
    }

}
