WRAPPERS

- Wrappers handle runtime loading of external asset bundle components. They describe changable properties that make each AV different. 

- Wrapper prefabs can and in many cases should have their instance broken (menu -> gameobject -> break prefab instance).  These prefabs contain very little heiarchy and script and so this has little impact on implementation.  Breaking the prefab instance has some assurance that importing a new asset package won't overwrite any settings (should not happen though if you modify your values in the scene). 

- Many wrappers share a few scriptable objects that describe colors and text.  Theses are generic databases that can be duplicated and replaced at will.  Each project av should have their own unique copy of these objects that pertain only to them.  Feel free to use the ones in the asset package as starting place however for new projects. 