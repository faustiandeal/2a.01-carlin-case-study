﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class GlobalVersionOverlay : MonoBehaviour {

    public string Version;
    public Color Color = Color.white;
    private GameObject canvasGO;

	// Use this for initialization
	void Start () {

        canvasGO = new GameObject("Canvas", typeof(Canvas), typeof(CanvasScaler));
        canvasGO.transform.SetParent(this.transform, false);

     

        /*Canvas canvas = canvasGO.GetComponent<Canvas>();
        canvas.sortingOrder = 999999;
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.transform.localScale = Vector3.one;*/

        GameObject textGO = new GameObject("Label", typeof(Text), typeof(ContentSizeFitter));
        textGO.transform.SetParent(canvasGO.transform);


        Text text = textGO.GetComponent<Text>();
        text.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        text.fontSize = 44;
        text.fontStyle = FontStyle.Bold;
        text.text = "version:  " + Version;
        text.color = Color;

        ContentSizeFitter fitter = textGO.GetComponent<ContentSizeFitter>();
        fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

        RectTransform textRect = textGO.GetComponent<RectTransform>();
        textRect.pivot = new Vector2(1, 0);
        textRect.anchorMin = new Vector2(1, 0);
        textRect.anchorMax = new Vector2(1, 0);
        textRect.anchoredPosition = new Vector3(-10, 10, 0);
        textRect.localScale = Vector3.one;
        Hide();
        //Invoke("Hide", 5f);
    }

    void Hide()
    {
        canvasGO.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("g"))
        {
            canvasGO.SetActive(true);
            CancelInvoke("Hide");
            Invoke("Hide", 5f);
        }
    }
}
