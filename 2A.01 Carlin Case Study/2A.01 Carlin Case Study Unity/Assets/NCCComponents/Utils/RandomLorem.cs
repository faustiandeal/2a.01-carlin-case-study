﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class RandomLorem {

    private const string lorem = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vel elementum libero. Duis tincidunt bibendum nunc id cursus. Nunc tristique pellentesque congue. Cras fermentum arcu eu condimentum sagittis. Donec mi sapien, pulvinar et egestas malesuada, aliquet et velit. Nunc et cursus enim. Duis vitae augue quis mi vestibulum pretium id et ante. Donec nec facilisis metus. Vivamus feugiat, neque vitae sodales scelerisque, dui orci scelerisque felis, eu eleifend ipsum ligula non velit. Donec varius vulputate lectus, a consequat mauris consectetur quis. Vestibulum at tincidunt leo, sit amet venenatis mi.

Suspendisse efficitur malesuada tellus id efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent iaculis porttitor metus, et vulputate quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus quis aliquet sem. Etiam a feugiat ipsum, non tempor felis. Proin condimentum erat sem, vitae posuere libero luctus vel. Pellentesque pretium imperdiet cursus. Donec varius nibh eget erat rutrum vulputate. Fusce a scelerisque enim. Maecenas semper ac neque vel accumsan. Donec ullamcorper semper lacus vitae bibendum. Donec pretium volutpat quam nec ultricies. Curabitur rutrum est vitae finibus iaculis. Praesent non dapibus diam, ac ullamcorper purus. Sed fringilla massa et sem vehicula bibendum.

Integer eget tempus nisi, a porttitor leo. Cras molestie luctus convallis. Curabitur eu libero vitae sapien ultricies consectetur. Etiam molestie metus at libero accumsan, at volutpat purus elementum. Nunc a condimentum tellus, tempor volutpat lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse ante quam, rhoncus non fermentum ut, ultricies nec odio. Phasellus vitae hendrerit mauris. Phasellus in molestie sem. Ut non nisl ut est ultrices consequat.

Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In pretium risus non laoreet sagittis. Curabitur mauris augue, porta nec efficitur et, elementum at urna. Ut blandit pretium tortor rhoncus bibendum. Morbi sed dignissim leo. Donec lobortis pulvinar turpis, ut porta eros condimentum a. Vivamus et laoreet orci. Etiam euismod justo ut nulla iaculis, et sodales nulla mattis. Cras non dolor tellus. Vestibulum ultricies nisl augue, a molestie lectus venenatis in. Maecenas nisi metus, convallis et massa sed, iaculis gravida eros. Morbi vel est vel enim volutpat vulputate vitae vel eros. Cras aliquam dictum quam et mattis.

Etiam sem neque, facilisis vitae ornare eu, posuere at mi. Integer tortor tortor, condimentum quis efficitur vitae, porttitor at lorem. Integer mi tortor, condimentum vitae ligula eget, finibus mollis orci. Fusce tincidunt dignissim nibh quis bibendum. Nunc nec diam eu ex tincidunt auctor. Duis vitae tortor quis nulla aliquam placerat ac sit amet ante. Ut porta elit tempus sodales imperdiet. Nunc placerat vitae urna id lacinia. Maecenas sit amet rutrum velit. Quisque vel urna sapien. Quisque mattis, ante sit amet feugiat aliquam, nisl tellus auctor libero, id scelerisque leo enim sed eros. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque tristique odio at sem sodales, in sagittis ante efficitur. Mauris at velit gravida, ultrices massa nec, ullamcorper nisi.

Nam mattis quis erat vitae tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras eu nibh pulvinar sapien sodales efficitur. Cras ut metus consequat, efficitur lacus sit amet, ullamcorper nulla. Ut libero justo, rhoncus ut nunc id, mattis luctus lorem. Fusce consectetur dolor at urna porta, eu molestie dolor suscipit. Nulla commodo dictum metus id laoreet. Ut pulvinar elit id lectus tincidunt efficitur. Pellentesque sed felis id nisl euismod egestas at vitae ipsum. In ullamcorper sem vel justo ultricies tincidunt.

Fusce tincidunt neque in bibendum dignissim. Aliquam quis tincidunt dui. Suspendisse potenti. Donec ultrices mi ligula, luctus tempus arcu varius quis. Integer in fringilla metus. Donec a vulputate erat. Etiam maximus, magna eu luctus accumsan, purus urna convallis tellus, quis rhoncus mauris mi in ex.Donec quis dui id urna lobortis cursus. Sed pellentesque nisl nec velit laoreet, id sodales est pellentesque. Mauris eu tincidunt risus, ac efficitur risus. Quisque id varius nisi. Vivamus mattis elementum libero, id pulvinar nulla malesuada quis. Pellentesque nec arcu justo. In hac habitasse platea dictumst. Nam dictum dolor sed nulla ultricies venenatis.

Phasellus vitae ante urna. Aliquam nec libero ex. Maecenas consectetur efficitur justo, et mattis quam tempus et. Donec at lacus facilisis, fringilla sem ac, placerat ex. Etiam molestie, quam ut vestibulum aliquam, nisl justo cursus risus, vel egestas est est rhoncus elit. Etiam bibendum, metus eget aliquet porttitor, est ligula tristique leo, vitae imperdiet arcu nibh sed neque. Nulla mi nisi, interdum a tristique quis, efficitur in quam. Quisque et feugiat diam, nec pellentesque mauris. Sed nec diam neque. Duis varius urna vitae quam imperdiet vulputate. Phasellus mattis ex quis blandit lobortis. Maecenas mattis molestie est, id gravida odio maximus at. Aliquam tincidunt, ex finibus fermentum egestas, lorem dui dapibus quam, nec vestibulum augue ipsum at dolor. Sed nunc nibh, eleifend vel quam nec, ullamcorper tempor leo.";

    /// <summary>
    /// Gets a random "Lorem Ipsum" title string with length in specified range
    /// </summary>
    /// <param name="minLength">minimum string length</param>
    /// <param name="maxLength">maximum string length</param>
    public static string GetTitle( int minLength, int maxLength ) {
        string randomString = GetRandomLorem( minLength, maxLength );

        // Convert to title case
        randomString = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase( randomString );

        // Strip punctuation and newlines
        randomString = randomString.Replace( ",", "" );
        randomString = randomString.Replace( ".", "" );
        randomString = randomString.Replace( ";", "" );
        randomString = randomString.Replace( "\n", " " );
        randomString = randomString.Replace( "\r", "" );

        return randomString.Trim( );
    }


    /// <summary>
    /// Generates a random "Lorem Ipsum" body text string with length in specified range
    /// </summary>
    /// <param name="minLength">minimum string length</param>
    /// <param name="maxLength">maximum string length</param>
    /// <param name="includeNewlines">should we include newlines in the text or just give a single paragraph?</param>
    public static string GetBodyText( int minLength, int maxLength, bool includeNewlines = false ) {
        string randomString = GetRandomLorem( minLength, maxLength );

        if ( !includeNewlines ) {
            randomString = randomString.Replace( "\n", " " );
            randomString = randomString.Replace( "\r", "" );
        }

        return randomString.Trim( );
    }


    private static string GetRandomLorem( int minLength, int maxLength ) {
        int randomLength = Random.Range( minLength, maxLength );
        int randomStart = Random.Range( 0, lorem.Length - randomLength );
        return lorem.Substring( randomStart, randomLength );
    }
}
