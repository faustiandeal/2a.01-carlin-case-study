﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NCCColorPalette
{
    public static readonly Color Yellow =   HexToColorConverter.HexToColor("C9FF00");
    public static readonly Color Orange =   HexToColorConverter.HexToColor("FF4200");
    public static readonly Color Blue =     HexToColorConverter.HexToColor("0038D6");
    public static readonly Color Pink =     HexToColorConverter.HexToColor("FF0093");
    public static readonly Color Cyan =     HexToColorConverter.HexToColor("12FFC4");

    public static readonly Color DarkYellow =   HexToColorConverter.HexToColor("99CC00");
    public static readonly Color DarkOrange =   HexToColorConverter.HexToColor("BF3100");
    public static readonly Color DarkBlue =     HexToColorConverter.HexToColor("002AA0");
    public static readonly Color DarkPink =     HexToColorConverter.HexToColor("BF006E");
    public static readonly Color DarkCyan =     HexToColorConverter.HexToColor("0DBF93");
}
