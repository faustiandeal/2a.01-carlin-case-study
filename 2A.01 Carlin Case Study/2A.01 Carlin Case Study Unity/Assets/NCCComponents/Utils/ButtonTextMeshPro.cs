﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]
public class ButtonTextMeshPro : MonoBehaviour, IPointerDownHandler
{
    public event Action<GameObject, PointerEventData> OnTouch;

    [HideInInspector]
    public Button Button;
    public TextMeshProUGUI Label;
    public Image DropShadowImage;

    public bool Interactable
    {
        get
        {
            return Button.IsInteractable();
        }
        set
        {
            SetInteractable(value);
        }
    }

    public string Text
    {
        get
        {
            return Label.GetParsedText();
        }
        set
        {
            Label.SetText(value);
        }
    }

    protected void Awake()
    {
        Button = GetComponent<Button>();
        if (Button == null)
        {
            Debug.LogError("Can't Find Button component on this GameObject!");
        }
        if (Label == null)
        {
            Debug.LogError("Can't Find TextMeshPro Label component!");
        }
        SetInteractable(Button.IsInteractable());
    }

    protected void SetInteractable(bool isInteractable)
    {
        Button.interactable = isInteractable;
        Label.color = new Color(Label.color.r, Label.color.g, Label.color.b, (isInteractable ? Button.colors.normalColor.a : Button.colors.disabledColor.a));
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnTouch != null)
        {
            OnTouch(this.gameObject, eventData);
        }
    }
}