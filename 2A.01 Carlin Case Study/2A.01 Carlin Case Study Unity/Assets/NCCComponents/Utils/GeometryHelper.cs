﻿using UnityEngine;
using System.Collections;

public class GeometryHelper : MonoBehaviour {

    public static float DegreeToRadians(float inAngle)
    {
        return Mathf.PI * inAngle / 180f;
    }

    public static float RadianToDegree(float inAngle)
    {
        return inAngle * (180f / Mathf.PI);
    }

    public static Vector2 CalculatePointOnEllipse(float originX, float originY, float xRadius, float yRadius, float angle)
    {
        float x = originX + xRadius * Mathf.Cos(DegreeToRadians(angle));
        float y = originY + yRadius * Mathf.Sin(DegreeToRadians(angle));

        return new Vector2(x, y);
    }
}
