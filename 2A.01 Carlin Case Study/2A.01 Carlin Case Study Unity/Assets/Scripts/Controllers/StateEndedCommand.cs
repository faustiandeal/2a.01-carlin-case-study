﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cortina.architecture;
using System;

public class StateEndedCommand : IPipelineCommand
{
    public void Execute(MediatorPipeline mm, object o)
    {
        NotificationPayloadObject data = (NotificationPayloadObject)o;
        Debug.LogWarning("[StateEndedCommand] " + data.Player + " " + (CarlinCaseFacade.States)data.Payload);
        CarlinCaseGameProxy proxy = mm.FindProxy<CarlinCaseGameProxy>(CarlinCaseGameProxy.Name);
        //Attract, Intro, TrunkOpening, MainMenu, CareerHistoryMenu, CareerHistoryFolder, DocumentSelected, JokeNotesIntro, JokeNotesFolder, JokeNoteBagSelected, JokeNoteSelected, DayPlanner
        switch ((CarlinCaseFacade.States)data.Payload)
        {
            case CarlinCaseFacade.States.Attract:
                proxy.SetPlayerState(data.Player, CarlinCaseFacade.States.Intro);
                proxy.SendUpdatedState(data.Player);
                break;
            case CarlinCaseFacade.States.Intro:
                proxy.SetPlayerState(data.Player, CarlinCaseFacade.States.TrunkOpening);
                proxy.SendUpdatedState(data.Player);
                break;
            case CarlinCaseFacade.States.TrunkOpening:
                proxy.SetPlayerState(data.Player, CarlinCaseFacade.States.MainMenu);
                proxy.SendUpdatedState(data.Player);
                break;
            case CarlinCaseFacade.States.MainMenu:
                CarlinCaseFacade.States newState = CarlinCaseFacade.States.CareerHistoryMenu;
                proxy.SetPlayerState(data.Player, CarlinCaseFacade.States.CareerHistoryMenu);
                proxy.SendUpdatedState(data.Player);
                break;
                //case CarlinCaseFacade.States.ChooseJoke:
                //    proxy.SetPlayerState(data.Player, CarlinCaseFacade.States.TellingJoke);
                //    if (proxy.PlayersAtSameState() )//|| proxy.Round % 2 == 0)
                //    {
                //        proxy.SendUpdatedState(proxy.PlayerTellingJoke());
                //    }
                //    break;
                //case CarlinCaseFacade.States.TellingJoke:
                //    proxy.SetPlayerState(Player.One, CarlinCaseFacade.States.JokeResult);
                //    proxy.SetPlayerState(Player.Two, CarlinCaseFacade.States.JokeResult);
                //    proxy.SendUpdatedStates();
                //    break;
                //case CarlinCaseFacade.States.JokeResult:
                //    CarlinCaseFacade.States newState = CarlinCaseFacade.States.ChooseJoke;
                //    if (proxy.GameIsOver())
                //    {
                //        newState = CarlinCaseFacade.States.VictoryPicture;
                //    }
                //    else if (proxy.Round % 2 == 1)
                //    {
                //        newState = CarlinCaseFacade.States.TellingJoke;
                //    }
                //    proxy.SetPlayerState(data.Player, newState);
                //    if (proxy.PlayersAtSameState()) // makes sure only increments once per round
                //    {
                //        proxy.Round++;
                //        proxy.SetPlayerState(Player.One, newState);
                //        proxy.SetPlayerState(Player.Two, newState);
                //        if (newState == CarlinCaseFacade.States.TellingJoke)
                //        {
                //            proxy.SendUpdatedState(proxy.PlayerTellingJoke());
                //        }
                //        else
                //        {
                //            proxy.SendUpdatedStates();
                //        }
                //    }
                //    break;
                //case CarlinCaseFacade.States.VictoryPicture:
                //    proxy.SetPlayerState(Player.One, CarlinCaseFacade.States.Conclusion);
                //    proxy.SetPlayerState(Player.Two, CarlinCaseFacade.States.Conclusion);
                //    proxy.SendUpdatedStates();
                //    break;
                //case CarlinCaseFacade.States.Conclusion:
                //default:
                //    newState = CarlinCaseFacade.States.Attract;
                //    proxy.SetPlayerState(Player.One, CarlinCaseFacade.States.Attract);
                //    proxy.SetPlayerState(Player.Two, CarlinCaseFacade.States.Attract);
                //    proxy.SendUpdatedStates();
                //    break;
        }
    }
}