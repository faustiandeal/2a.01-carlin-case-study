﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using TMPro;

public class CareerScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<CareerScrollerData> _data;

    public EnhancedScroller myScroller;
    public PlayerComponent PlayerComponent;
    public CareerFolderCellView folderCellViewPrefab;
    public CareerFolderComponent careerFolderComponentPrefab;
    public GameObject AllCareerHistoryFolders;
    [HideInInspector]
    public List<Transform> FolderTextPlacementList;
    public List<Sprite> folderImageListLeftTab;
    public List<Sprite> folderImageListMiddleTab;
    public List<Sprite> folderImageListRightTab;
    public List<Sprite> openFolderImageListLeftTab;
    //public List<Sprite> openFolderImageListMiddleTab;
    //public List<Sprite> openFolderImageListRightTab;
    [HideInInspector]
    public List<CareerFolderComponent> allFolders;
    private CareerHistoryDataModel careerData;
    private int tabPos = 0;
    private int selection = 0;

    void Start()
    {
        FolderTextPlacementList = folderCellViewPrefab.folderTextPlacementList;
        AllCareerHistoryFolders.GetComponent<DocumentContainer>().myViewer.CreateMyListOfDocuments();
        _data = new List<CareerScrollerData>();

        for (int i = 0; i < careerData.category.Count; i++)
        {
            CareerFolderComponent newFolder = CreateNewFolder(careerData.category[i]);
            Sprite folderColor = RandomizeFolderColors(tabPos,2);
            newFolder.myFolderColor.sprite = SetMyOpenFolderColor();
            //newFolder.SetUpMyButtonColor(selection);
            allFolders.Add(newFolder);
            float randomCanter = Random.Range(-2, 2);
            _data.Add(new CareerScrollerData()
            {
                folderName = careerData.category[i].title.ToUpper(),
                myFolder = newFolder,
                folderImage = folderColor,
                folderTextPlacementIndex = tabPos,
                myCanter = randomCanter
            });
            IncrementFolderIndex();
        }
        //Sprite randomImage = RandomizeFolderColors(tabPos);
        #region FAKE DATA FOR ALPHA
        //_data.Add(new CareerScrollerData() { folderName = "FAN MAIL", folderImage = RandomizeFolderColors(tabPos,1), folderTextPlacementIndex = tabPos, myCanter=Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "SET LISTS", folderImage = RandomizeFolderColors(tabPos,2), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "MILWAUKEE 1972", folderImage = RandomizeFolderColors(tabPos,0), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "OTHER HASSLES", folderImage = RandomizeFolderColors(tabPos,1), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "G.C.-1960's MATERIAL", folderImage = RandomizeFolderColors(tabPos,0), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "G.C.-CAREER FACTS", folderImage = RandomizeFolderColors(tabPos,2), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        ////_data.Add(new CareerScrollerData() { folderName = "SPECIAL EVENTS BITS" });
        //_data.Add(new CareerScrollerData() { folderName = "FILTH PT.1&2", folderImage = RandomizeFolderColors(tabPos, 0), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "FCC FILTH", folderImage = RandomizeFolderColors(tabPos,0), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "FCC V PACIFICA FOUNDATION", folderImage = RandomizeFolderColors(tabPos,2), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "TV STYLE BITS AND FEATURES", folderImage = RandomizeFolderColors(tabPos,1), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "NON TV STYLE SKETCHES AND FEATURES", folderImage = RandomizeFolderColors(tabPos,1), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "PHOTOS IN PERFORMANCE", folderImage = RandomizeFolderColors(tabPos,2), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "FAN MAIL", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "SET LISTS", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "MILWAUKEE 1972", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "OTHER HASSLES", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "G.C.-1960's MATERIAL", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "G.C.-CAREER FACTS", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        ////_data.Add(new CareerScrollerData() { folderName = "SPECIAL EVENTS BITS" });
        //_data.Add(new CareerScrollerData() { folderName = "FILTH PT.1&2", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "FCC FILTH", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "FCC V PACIFICA FOUNDATION", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "TV STYLE BITS AND FEATURES", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "NON TV STYLE SKETCHES AND FEATURES", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        //_data.Add(new CareerScrollerData() { folderName = "PHOTOS IN PERFORMANCE", folderImage = RandomizeFolderColors(tabPos), folderTextPlacementIndex = tabPos, myCanter = Random.Range(-2, 2) });
        //IncrementFolderIndex();
        #endregion

        tabPos = 0;
        myScroller.Delegate = this;
        myScroller.ReloadData();
    }

    public CareerFolderComponent CreateNewFolder(CareerHistoryCategoryDataModel data)
    {
        CareerFolderComponent newFolder = Instantiate<CareerFolderComponent>(careerFolderComponentPrefab, AllCareerHistoryFolders.transform, false);
        newFolder.name = data.title;
        newFolder.instructionalText.text = PlayerComponent.careerHistoryInstructionalText;
        newFolder.myContainer = AllCareerHistoryFolders.GetComponent<DocumentContainer>();
        //newFolder.myData = data;
        newFolder.CreateMyArtifacts(data);
        return newFolder;
    }

    public void PassDataToMe(CareerHistoryDataModel data)
    {
        careerData = data;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 100f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        CareerFolderCellView cellView = scroller.GetCellView(folderCellViewPrefab) as CareerFolderCellView;
        cellView.myController = this;
        cellView.SetData(_data[dataIndex]);

        return cellView;
    }

    private Sprite RandomizeFolderColors(int side, int selectColor)
    {
        Sprite result = null;
        switch (side)
        {
            case 0:     //left side folder tabs
                selection = Random.Range(0, folderImageListLeftTab.Count);
                //selection = selectColor;
                result = folderImageListLeftTab[selection];
                break;
            case 1:     //right side folder tabs
                selection = Random.Range(0, folderImageListRightTab.Count);
                //selection = selectColor;
                result = folderImageListRightTab[selection];
                break;
            case 2:     //middle folder tabs
                selection = Random.Range(0, folderImageListMiddleTab.Count);
                //selection = selectColor;
                result = folderImageListMiddleTab[selection];
                break;
        }
        return result;
    }

    private Sprite SetMyOpenFolderColor()
    {
        Sprite result = null;
        result = openFolderImageListLeftTab[selection];
        //switch(tabPos)
        //{
        //    case 0:
        //        result = openFolderImageListLeftTab[selection];
        //        break;
        //    case 1:
        //        result = openFolderImageListRightTab[selection];
        //        break;
        //    case 2:
        //        result = openFolderImageListMiddleTab[selection];
        //        break;
        //}
        return result;
    }

    private void IncrementFolderIndex()
    {
        tabPos += 1;
        if (tabPos > 2) tabPos = 0;
    }
}
