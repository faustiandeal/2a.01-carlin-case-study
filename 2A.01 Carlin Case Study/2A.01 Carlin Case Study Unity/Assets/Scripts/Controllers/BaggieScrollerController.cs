﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;

/// <summary>
/// This example shows how to simulate a grid with a fixed number of cells per row
/// The data is stored as normal, but the differences in this example are:
/// 
/// 1) The scroller is told the data count is the number of data elements divided by the number of cells per row
/// 2) The cell view is passed a reference to the data set with the offset index of the first cell in the row
public class BaggieScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public GameObject AllBaggieFolders;
    public BaseComponent FadeObject;
    public JokeNotesComponent JokeNotesComponent;
    private JokeNotesDataModel jokeNotesData;
    public BaggieFolderComponent baggieFolderComponentPrefab;
    public GameObject myScrollbar;
    [HideInInspector]
    public List<BaggieFolderComponent> baggieFolders;
    public Transform hiddenImageSpot;
    /// <summary>
    /// Internal representation of our data. Note that the scroller will never see
    /// this, so it separates the data from the layout using MVC principles.
    /// </summary>
    private List<BaggieRowCellData> _data;

    /// <summary>
    /// This is our scroller we will be a delegate for
    /// </summary>
    public EnhancedScroller myScroller;

    /// <summary>
    /// This will be the prefab of each cell in our scroller. The cell view will
    /// hold references to each row sub cell
    /// </summary>
    public EnhancedScrollerCellView BaggieCellViewPrefab;

    public int numberOfCellsPerRow = 4;

    /// <summary>
    /// Be sure to set up your references to the scroller after the Awake function. The 
    /// scroller does some internal configuration in its own Awake function. If you need to
    /// do this in the Awake function, you can set up the script order through the Unity editor.
    /// In this case, be sure to set the EnhancedScroller's script before your delegate.
    /// 
    /// In this example, we are calling our initializations in the delegate's Start function,
    /// but it could have been done later, perhaps in the Update function.
    /// </summary>
    void Start()
    {
        // tell the scroller that this script will be its delegate
        myScroller.Delegate = this;

        // load in a large set of data
        LoadData();
    }

    /// <summary>
    /// Populates the data with a lot of records
    /// </summary>
    private void LoadData()
    {
        AllBaggieFolders.GetComponent<DocumentContainer>().myViewer.CreateMyListOfDocuments();
        baggieFolders = new List<BaggieFolderComponent>();
        _data = new List<BaggieRowCellData>();

        for (int i = 0; i < jokeNotesData.category.Count; i++)
        {
            //List<Sprite> containedSprites = new List<Sprite>();
            //for (int j = 0; j < jokeNotesData.category[i].jokeNotesImages.Count; j++)
            //{
            //    string imageFilePathOld = CMSDataProxy.LocalMediaFilePath + jokeNotesData.category[i].jokeNotesImages[j].image;
            //    string imageFilePath = InsertToString(imageFilePathOld);
            //    Sprite mySprite;
            //    mySprite = SpriteLoader.LoadSprite(imageFilePath);
            //    containedSprites.Add(mySprite);
            //}
            string imageFilePath = CMSDataProxy.LocalMediaFilePath + jokeNotesData.category[i].ziplocBagImage;
            Sprite mySprite;
            mySprite = SpriteLoader.LoadSprite(imageFilePath,false);
            BaggieFolderComponent newFolder = CreateNewFolder(jokeNotesData.category[i]/*, containedSprites*/);
            baggieFolders.Add(newFolder);
            _data.Add(new BaggieRowCellData()
            {
                folderName = jokeNotesData.category[i].title.ToUpper(),
                myFolder = newFolder,
                myCanvasParent = FadeObject,
                myScrollbar = myScrollbar,
                myBaggieImage = mySprite,
                //myContainedImages = containedSprites,
                myJokeNotesComponent = JokeNotesComponent          
            });
            //_data[i].SetUpMyImages();
            //containedSprites.Clear();
        }

        //_data.Add(new BaggieRowCellData() { folderName = "RELIGION" });
        //_data.Add(new BaggieRowCellData() { folderName = "DEATH" });
        //_data.Add(new BaggieRowCellData() { folderName = "EUPHEMISM" });
        //_data.Add(new BaggieRowCellData() { folderName = "FIGS OF SPEECH" });
        //_data.Add(new BaggieRowCellData() { folderName = "GOOFIES" });
        //_data.Add(new BaggieRowCellData() { folderName = "LINGO" });
        //_data.Add(new BaggieRowCellData() { folderName = "NAMES" });
        //_data.Add(new BaggieRowCellData() { folderName = "RACE" });
        //_data.Add(new BaggieRowCellData() { folderName = "MISC ACCENTS" });
        //_data.Add(new BaggieRowCellData() { folderName = "ENGLISH LANGUAGE" });
        //_data.Add(new BaggieRowCellData() { folderName = "DICTIONARY WORDS" });

        //myScroller.Delegate = this;
        myScroller.ReloadData();
    }

    private string InsertToString(string givenString)
    {
        string addition = "-300x300";
        string newString = givenString.Insert(givenString.LastIndexOf("."), addition);
        return newString;
    }

    public BaggieFolderComponent CreateNewFolder(JokeNotesCategoryDataModel data/*, List<Sprite>myContainedSprites*/)
    {
        BaggieFolderComponent newFolder = Instantiate<BaggieFolderComponent>(baggieFolderComponentPrefab, AllBaggieFolders.transform, false);
        newFolder.myJokeNotesComponent = JokeNotesComponent;
        newFolder.name = data.title;
        newFolder.FadeObject = FadeObject;
        newFolder.myScrollbar = myScrollbar;
        newFolder.myContainer = AllBaggieFolders.GetComponent<DocumentContainer>();
        newFolder.baggieLabelText.text = data.title;
        //newFolder.transform.localPosition = new Vector3(0, 0, 0);
        newFolder.CreateMyArtifacts(data, newFolder.gameObject.transform/*, myContainedSprites*/);
        return newFolder;
    }

    public void PassDataToMe(JokeNotesDataModel data)
    {
        jokeNotesData = data;
    }

    #region EnhancedScroller Handlers

    /// <summary>
    /// This tells the scroller the number of cells that should have room allocated.
    /// For this example, the count is the number of data elements divided by the number of cells per row (rounded up using Mathf.CeilToInt)
    /// </summary>
    /// <param name="scroller">The scroller that is requesting the data size</param>
    /// <returns>The number of cells</returns>
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return Mathf.CeilToInt((float)_data.Count / (float)numberOfCellsPerRow);
    }

    /// <summary>
    /// This tells the scroller what the size of a given cell will be. Cells can be any size and do not have
    /// to be uniform. For vertical scrollers the cell size will be the height. For horizontal scrollers the
    /// cell size will be the width.
    /// </summary>
    /// <param name="scroller">The scroller requesting the cell size</param>
    /// <param name="dataIndex">The index of the data that the scroller is requesting</param>
    /// <returns>The size of the cell</returns>
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 400f;
    }

    /// <summary>
    /// Gets the cell to be displayed. You can have numerous cell types, allowing variety in your list.
    /// Some examples of this would be headers, footers, and other grouping cells.
    /// </summary>
    /// <param name="scroller">The scroller requesting the cell</param>
    /// <param name="dataIndex">The index of the data that the scroller is requesting</param>
    /// <param name="cellIndex">The index of the list. This will likely be different from the dataIndex if the scroller is looping</param>
    /// <returns>The cell for the scroller to use</returns>
    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        // first, we get a cell from the scroller by passing a prefab.
        // if the scroller finds one it can recycle it will do so, otherwise
        // it will create a new cell.
        BaggieCellView cellView = scroller.GetCellView(BaggieCellViewPrefab) as BaggieCellView;

        cellView.name = "Cell " + (dataIndex * numberOfCellsPerRow).ToString() + " to " + ((dataIndex * numberOfCellsPerRow) + numberOfCellsPerRow - 1).ToString();

        // pass in a reference to our data set with the offset for this cell
        cellView.SetData(ref _data, dataIndex * numberOfCellsPerRow);

        // return the cell to the scroller
        return cellView;
    }

    #endregion
}

//public class BaggieScrollerController : MonoBehaviour, IEnhancedScrollerDelegate 
//{
    //private List<BaggieScrollerData> _data;

    //public EnhancedScroller myScroller;
    //public BaggieFolderCellView folderCellViewPrefab;
    //public BaggieFolderComponent baggieFolderComponentPrefab;
    //public GameObject AllBaggieFolders;
    //private JokeNotesDataModel jokeNotesData;

    //void Start () 
    //{
    //    AllBaggieFolders.GetComponent<DocumentContainer>().myViewer.CreateMyListOfDocuments();
    //    _data = new List<BaggieScrollerData>();

    //    for (int i = 0; i < jokeNotesData.category.Count; i++)
    //    {
    //        BaggieFolderComponent newFolder = CreateNewFolder(jokeNotesData.category[i]);
    //        _data.Add(new BaggieScrollerData()
    //        {
    //            folderName = jokeNotesData.category[i].title,
    //            myFolder = newFolder
    //        });
    //    }

    //    _data.Add(new BaggieScrollerData() { folderName = "RELIGION" });
    //    _data.Add(new BaggieScrollerData() { folderName = "DEATH" });
    //    _data.Add(new BaggieScrollerData() { folderName = "EUPHEMISM" });
    //    _data.Add(new BaggieScrollerData() { folderName = "FIGS OF SPEECH" });
    //    _data.Add(new BaggieScrollerData() { folderName = "GOOFIES" });
    //    _data.Add(new BaggieScrollerData() { folderName = "LINGO" });
    //    _data.Add(new BaggieScrollerData() { folderName = "NAMES" });
    //    _data.Add(new BaggieScrollerData() { folderName = "RACE" });
    //    _data.Add(new BaggieScrollerData() { folderName = "MISC ACCENTS" });
    //    _data.Add(new BaggieScrollerData() { folderName = "ENGLISH LANGUAGE" });
    //    _data.Add(new BaggieScrollerData() { folderName = "DICTIONARY WORDS" });

    //    myScroller.Delegate = this;
    //    myScroller.ReloadData();
    //}

    //public BaggieFolderComponent CreateNewFolder(JokeNotesCategoryDataModel data)
    //{
    //    BaggieFolderComponent newFolder = Instantiate<BaggieFolderComponent>(baggieFolderComponentPrefab, AllBaggieFolders.transform, false);
    //    newFolder.name = data.title;
    //    newFolder.myContainer = AllBaggieFolders.GetComponent<DocumentContainer>();
    //    newFolder.baggieLabelText.text = data.title;
    //    //newFolder.transform.localPosition = new Vector3(0, 0, 0);
    //    newFolder.CreateMyArtifacts(data, newFolder.gameObject.transform);
    //    return newFolder;
    //}

    //public void PassDataToMe(JokeNotesDataModel data)
    //{
    //    jokeNotesData = data;
    //}

    //public int GetNumberOfCells(EnhancedScroller scroller)
    //{
    //    return _data.Count;
    //}

    //public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    //{
    //    return 500f;
    //}

    //public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    //{
    //    BaggieFolderCellView cellView = scroller.GetCellView(folderCellViewPrefab) as BaggieFolderCellView;

    //    cellView.SetData(_data[dataIndex]);

    //    return cellView;
    //}

    //public void Update()
    //{
    //    //DoAlphaFade();
    //}

    //private void DoAlphaFade()
    //{
    //    if (myScroller.IsScrolling)
    //    {
    //        if (myScroller.LinearVelocity > 0)
    //        {
    //            Debug.Log("Scrolling Down ");
    //            Image image = myScroller._activeCellViews[0].gameObject.GetComponent<Image>();
    //            var tempColor = image.color;
    //            tempColor.a -= (myScroller.LinearVelocity*Time.deltaTime)*.005f;
    //            if (tempColor.a < 0) tempColor.a = 0;
    //            myScroller._activeCellViews[0].GetComponent<Image>().color = tempColor;

    //            var tempColor2 = myScroller._activeCellViews[myScroller._activeCellViews.Count - 1].GetComponent<Image>().color;
    //            tempColor2.a = 1f;
    //            myScroller._activeCellViews[myScroller._activeCellViews.Count - 1].GetComponent<Image>().color = tempColor2;
    //        }
    //        if(myScroller.LinearVelocity <0)
    //        {
    //            Debug.Log("Scrolling Up ");
    //            Image image = myScroller._activeCellViews[0].gameObject.GetComponent<Image>();
    //            var tempColor = image.color;
    //            if (tempColor.a < 0) tempColor.a = 0;
    //            tempColor.a += (myScroller.LinearVelocity * Time.deltaTime) * .005f;

    //            myScroller._activeCellViews[0].GetComponent<Image>().color = tempColor;
    //        }
    //    }
    //}

//}
