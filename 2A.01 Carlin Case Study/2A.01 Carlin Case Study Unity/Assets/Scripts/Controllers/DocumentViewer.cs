﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Pointers;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using TMPro;
using NCCExternalComponentScripts.Wrappers;

public class DocumentViewer : BaseComponent
{
    public float zoomSpeed = .1f;
    public float maxZoom = 10f;
    public DocViewerImageToView documentInView;
    public Image arrowRight;
    public Image arrowLeft;
    public GameObject swipedFromLeft;
    private Vector3 swipedFromLeftOriginalPosition;
    private Vector2 swipedFromLeftOriginalSize;
    public GameObject swipedFromRight;
    private Vector3 swipedFromRightOriginalPosition;
    private Vector2 swipedFromRightOriginalSize;
    public VideoPlayerComponent myVideoPlayerComponent;
    public TextMeshProUGUI myVideoTitle;
    public GameObject pinchToZoomImage;
    public GameObject imageCloseButton;
    public Button mainMenuButton;
    private Vector3 initialScale = new Vector3(1, 1, 1);
    private Vector3 initialPosition;
    private Vector2 initialSize;
    //private Sprite mySprite;
    private Texture2D mySprite;
    //[HideInInspector]
    //public List<Sprite> myDocuments;
    [HideInInspector]
    public List<Texture2D> myDocuments;
    [HideInInspector]
    public List<string> myDocumentDetails;
    private int currentDocNumber = 0;
    public AttractWrapper myTimeOut;
    [HideInInspector]
    public SwipedDocument currentDoc;
    public Material ddsMaterial;
    private Material originalMaterial;

    public void ResetDocumentScaleAndPosition()
    {
        documentInView.transform.localPosition = initialPosition;
        documentInView.transform.localScale = initialScale;
        documentInView.resetting = false;
    }

    private void SetRatio(GameObject doc)
    {
        RawImage docImg = doc.GetComponent<RawImage>();
        AspectRatioFitter myAspect = doc.GetComponent<AspectRatioFitter>();
        float ratio = (float)docImg.texture.width / (float)docImg.texture.height;
        myAspect.aspectRatio = ratio;
        if (ratio > 1)
        {
            float width = 1024;
            float height = width / ratio;
            docImg.rectTransform.sizeDelta = new Vector2(width, height);
        }
        else
        {
            docImg.rectTransform.sizeDelta = new Vector2(1024.0f, 860.65f);
        }
        myAspect.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
    }

    private void SlideImages(Texture2D newImage/*Sprite newImage*/, bool right)
    {
        //myTimeOut.Hide();
        pinchToZoomImage.SetActive(false);
        if (right)
        {
            //swipedFromLeft.GetComponent<Image>().sprite = newImage;
            swipedFromLeft.GetComponent<RawImage>().texture = newImage;
            SetRatio(swipedFromLeft);
            LeanTween.moveLocal(swipedFromLeft, Vector3.zero, .25f);
            LeanTween.moveLocal(documentInView.gameObject, swipedFromRightOriginalPosition, .25f).setOnComplete(() =>
            {
                swipedFromLeft.transform.localPosition = swipedFromLeftOriginalPosition;
                SetMyImage(newImage, false);
                documentInView.transform.localScale = initialScale;
                documentInView.gameObject.transform.localPosition = Vector3.zero;
                pinchToZoomImage.SetActive(true);
            });
        }
        else
        {
            //swipedFromRight.GetComponent<Image>().sprite = newImage;
            swipedFromRight.GetComponent<RawImage>().texture = newImage;
            SetRatio(swipedFromRight);
            LeanTween.moveLocal(swipedFromRight, Vector3.zero, .25f);
            LeanTween.moveLocal(documentInView.gameObject, swipedFromLeftOriginalPosition, .25f).setOnComplete(() =>
            {
                swipedFromRight.transform.localPosition = swipedFromRightOriginalPosition;
                SetMyImage(newImage, false);
                documentInView.transform.localScale = initialScale;
                documentInView.gameObject.transform.localPosition = Vector3.zero;
                pinchToZoomImage.SetActive(true);
            });
        }
    }

    public void SwipedRight()
    {
        if (currentDocNumber > 0)
        {
            SlideImages(myDocuments[currentDocNumber - 1], true);
        }
    }

    public void SwipedLeft()
    {
        if (currentDocNumber < myDocuments.Count - 1)
        {
            SlideImages(myDocuments[currentDocNumber + 1], false);
        }
    }

    private void ToggleArrows()
    {
        if (myVideoPlayerComponent != null && myVideoPlayerComponent.myVideoPlayer != null)
        {
            if (myVideoPlayerComponent.myVideoPlayer.isPlaying)
            {
                arrowLeft.gameObject.SetActive(false);
                arrowRight.gameObject.SetActive(false);
                return;
            }
        }
        if (currentDocNumber == 0)
        {
            arrowLeft.gameObject.SetActive(false);
        }
        else
        {
            arrowLeft.gameObject.SetActive(true);
        }
        if (currentDocNumber == myDocuments.Count - 1)
        {
            arrowRight.gameObject.SetActive(false);
        }
        else
        {
            arrowRight.gameObject.SetActive(true);
        }
    }

    public void OnSwiped(FlickGesture flicker)
    {
        if (myVideoPlayerComponent != null && myVideoPlayerComponent.myVideoPlayer != null)
        {
            if (myVideoPlayerComponent.myVideoPlayer.isPlaying)
            {
                return;
            }
        }
        //ADD CODE HERE TO CHECK TO SEE IF VIDEO IS PLAYING
        //->
        if (flicker.State == Gesture.GestureState.Recognized)
        {
            if (flicker.ScreenFlickVector.x > 0.0f)  //flicked Right
            {
                if (currentDocNumber > 0)
                {
                    SwipedRight();
                    //SlideImages(myDocuments[currentDocNumber - 1], true);
                }
            }
            else //flicked Left
            {
                if (currentDocNumber < myDocuments.Count - 1)
                {
                    SwipedLeft();
                    //SlideImages(myDocuments[currentDocNumber + 1], false);
                }
            }
        }
    }
    public void CreateMyListOfDocuments()
    {
        myDocuments = new List<Texture2D>();
        myDocumentDetails = new List<string>();
    }

    public void OnAwake()
    {
        initialScale = new Vector3(1, 1, 1);
        initialPosition = documentInView.transform.localPosition;
        initialSize = documentInView.GetComponent<RectTransform>().sizeDelta;
        swipedFromLeftOriginalSize = swipedFromLeft.GetComponent<RectTransform>().sizeDelta;
        swipedFromRightOriginalSize = swipedFromRight.GetComponent<RectTransform>().sizeDelta;
        //myTimeOut = GameObject.Find("Attract Wrapper").GetComponent<AttractWrapper>();
    }

    public void Start()
    {
        swipedFromLeftOriginalPosition = swipedFromLeft.transform.localPosition;
        swipedFromRightOriginalPosition = swipedFromRight.transform.localPosition;
        if (myVideoPlayerComponent != null && myVideoPlayerComponent.myVideoPlayer != null)
        {
            myVideoPlayerComponent.myVideoPlayer.OnComplete += OnStoppedPlaying;
        }
        originalMaterial = documentInView.GetComponent<RawImage>().material;
    }

    public void OnStoppedPlaying()
    {
        this.Hide();
        myVideoPlayerComponent.ClearVideoTitleAndSubText();
        myTimeOut.UseTimeout = true;
    }

    //public void AddADocument(Sprite doc)
    //{
    //    myDocuments.Add(SpriteLoader.LoadSprite(doc.name));
    //}

    public void AddADocument(Texture2D doc)
    {
        myDocuments.Add(SpriteLoader.LoadTextureDXT(doc.name, TextureFormat.DXT5));
    }

    public void AddDetailsToDocument(string details)
    {
        myDocumentDetails.Add(details);
    }

    public void SetMyImage(Texture2D document/*Sprite document*/, bool isResized)
    {
        string name = document.name;
        if (isResized)
        {
            name = RemoveFromString(document.name);
        }
        for (int i = 0; i < myDocuments.Count; i++)
        {
            if (myDocuments[i].name == name)
            {
                //mySprite = documentInView.GetComponent<Image>().sprite = document;
                RawImage docImg = documentInView.GetComponent<RawImage>();
                docImg.texture = document;
                if (documentInView.amBestOf)
                {
                    if (docImg.texture.name.Contains("jpeg") || docImg.texture.name.Contains("jpg"))
                    {
                        docImg.material = originalMaterial;
                    }
                    else
                    {
                        docImg.material = ddsMaterial;
                    }
                }
                mySprite = document;
                SetRatio(documentInView.gameObject);
                documentInView.myTransformer.Type = TransformGesture.TransformType.Scaling;
                currentDocNumber = i;
                ToggleArrows();
                SetUpPinchIconAndCloseButton();
                if (documentInView.imageDetails != null)
                {
                    documentInView.imageDetails.text = myDocumentDetails[i];
                }
                return;
            }
        }
    }

    private void LateUpdate()
    {
        if ((base.Canvas.alpha > 0) && (mySprite != null))
        {
            SetUpPinchIconAndCloseButton();
        }
    }

    //public Sprite GetFirstDocument()
    //{
    //    if (myDocuments.Count > 0)
    //    {
    //        return myDocuments[0];
    //    }
    //    return null;
    //}

    public Texture2D GetFirstDocument()
    {
        if(myDocuments.Count >0)
        {
            return myDocuments[0];
        }
        return null;
    }

    private void SearchAndDisplayDoc()
    {
        if (currentDoc.baggieDoc)
        {
            if (currentDoc.baggieLocation.childCount > 0)
            {
                for (int i = 0; i < currentDoc.baggieLocation.childCount; i++)
                {
                    if (myDocuments[currentDocNumber].name == currentDoc.baggieLocation.GetChild(i).GetComponent<RawImage>().texture.name)
                    {
                        currentDoc.baggieLocation.GetChild(i).GetComponent<SwipedDocument>().MoveToFront();
                        return;
                    }
                }
            }
        }
        if (currentDoc.originalLocation.childCount > 0)
        {
            for (int i = 0; i < currentDoc.originalLocation.childCount; i++)
            {
                if (myDocuments[currentDocNumber].name == currentDoc.originalLocation.GetChild(i).GetComponent<RawImage>().texture.name)
                {
                    currentDoc.originalLocation.GetChild(i).GetComponent<SwipedDocument>().MoveToFront();
                    return;
                }
            }
        }
        if (currentDoc.swipedLocation.childCount > 0)
        {
            for (int i = 0; i < currentDoc.swipedLocation.childCount; i++)
            {
                if (myDocuments[currentDocNumber].name == currentDoc.swipedLocation.GetChild(i).GetComponent<RawImage>().texture.name)
                {
                    currentDoc.swipedLocation.GetChild(i).GetComponent<SwipedDocument>().MoveToFront();
                    return;
                }
            }
        }
    }

    public void SetUpPinchIconAndCloseButton()
    {
        float scaleX = documentInView.gameObject.transform.localScale.x;
        float scaleY = documentInView.gameObject.transform.localScale.y;
        Vector3 imgPos = documentInView.gameObject.transform.localPosition;
        //float imgWidth = mySprite.bounds.extents.x;
        //float imgHeight = mySprite.bounds.extents.y;
        float imgWidth = mySprite.width;
        float imgHeight = mySprite.height;
        float aspectRatio = (imgWidth) /
            (imgHeight);
        RectTransform imgRect = documentInView.gameObject.GetComponent<RectTransform>();
        float adjustmentX = 0;
        float adjustmentY = 0;
        if (aspectRatio < 1) //width less than height
        {
            adjustmentX = scaleX * ((imgRect.rect.height * aspectRatio) / 2.0f);//(imgWidth - diff);
            adjustmentY = -(scaleY * (imgRect.rect.height / 2.0f));
        }
        else
        {
            adjustmentX = scaleX * (imgRect.rect.width / 2.0f);
            adjustmentY = -scaleY * (imgRect.rect.width / aspectRatio) / 2.0f;
        }
        pinchToZoomImage.gameObject.transform.localPosition = new Vector3(adjustmentX, adjustmentY, 0) + imgPos;
        imageCloseButton.transform.localPosition = new Vector3(-1 * adjustmentX, -1 * adjustmentY, 0) + imgPos;
    }

    private string RemoveFromString(string givenString)
    {
        string addition = "-300x300";
        string newString = givenString.Remove(givenString.LastIndexOf(".") - addition.Length, addition.Length);
        return newString;
    }

    public void SetMyVideo(string videoURL)
    {
        myVideoPlayerComponent.myVideoPlayer.Load(videoURL, RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL);
    }

    public void ShowMyVideoPlayer()
    {
        pinchToZoomImage.SetActive(false);
        imageCloseButton.SetActive(false);
        documentInView.Hide();
        myVideoPlayerComponent.Show();
        LeanTween.delayedCall(.50f, () =>
        {
            myVideoPlayerComponent.myVideoPlayer.Play();
            myVideoPlayerComponent.myVideoUIWrapper.StartCaptions();
            myVideoPlayerComponent.myVideoPlayer.FadeVideo(1.0f);
            ToggleArrows();
            myTimeOut.UseTimeout = false;
        });
    }

    public void ShowMyDocumentImage()
    {
        pinchToZoomImage.SetActive(true);
        imageCloseButton.SetActive(true);
        if (myVideoPlayerComponent != null)
        {
            myVideoPlayerComponent.myVideoPlayer.FadeVideo(0.0f, 0.0f);
            myVideoPlayerComponent.Hide();
        }
        documentInView.Show();
    }

    public override void Hide(float time = 0.25f)
    {
        //myDocuments.Clear();
        if (currentDoc != null)
        {
            SearchAndDisplayDoc();
            currentDoc = null;
        }
        Input.multiTouchEnabled = false;
        if (myVideoPlayerComponent != null)
        {
            myVideoPlayerComponent.OnClose();
            myTimeOut.UseTimeout = true;
        }
        if (documentInView.imageDetails != null)
        {
            documentInView.imageDetails.text = null;
        }
        pinchToZoomImage.gameObject.SetActive(true);
        //ToggleArrows();
        ResetDocumentScaleAndPosition();
        mainMenuButton.gameObject.SetActive(true);
        base.Hide();
    }

    public override void Show(float time = 0.25F)
    {
        Input.multiTouchEnabled = true;
        base.Show(time);
    }
}