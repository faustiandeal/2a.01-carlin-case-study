﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TouchScript.Gestures;
using TouchScript.Tutorial;

public class DayPlannerArtifactComponent : BaseComponent
{
    public AspectRatioFitter myAspectFitter;
    [HideInInspector]
    public DocumentContainer myContainer;
    [HideInInspector]
    public DayPlannerComponent dayPlannerComponent;
    //public Image myCalendarImage;
    public RawImage myCalendarImage;
    [HideInInspector]
    public string calendarDate;
    private Color myCalendarColor;
    public DayPlannerContent contentPrefab;
    public List<DayPlannerContent> myContentList;
    public bool activeCalendar = false;
    private bool zoomed = false;
    private CalendarPullGesture gesture;
    private float currentMag = 0.0f;
    private float previousMag = 0.0f;

    public void SetUpMyData(DayPlannerArtifactsModel data, List<ContentPositionComponent> myContentPositions)
    {
        List<DayPlannerPositionContentDataModel> posContentList = new List<DayPlannerPositionContentDataModel>();
        posContentList.Add(data.topLeft);
        posContentList.Add(data.bottomLeft);
        posContentList.Add(data.topRight);
        posContentList.Add(data.bottomRight);
        for (int i = 0; i < posContentList.Count; i++)
        {
            if (!string.IsNullOrEmpty(posContentList[i].media))
            {
                DayPlannerContent newContent = Instantiate<DayPlannerContent>(contentPrefab, myContentPositions[i].gameObject.transform, false);
                newContent.transform.localPosition = new Vector3(0, 0, 0);
                newContent.Hide();
                newContent.SetUpMyData(posContentList[i], myContentPositions[i]);
                newContent.myContainer = myContainer;
                //if (!newContent.hasVideo)
                //{
                //    myContainer.myViewer.AddADocument(newContent.mediaImage.sprite);
                //    myContainer.myViewer.myDocumentDetails.Add(posContentList[i].descriptiveText);
                //}
                myContentList.Add(newContent);
            }
        }
        calendarDate = data.calendarDate;
        string imageFilePath = CMSDataProxy.LocalMediaFilePath + data.associatedImage;
        //myCalendarImage.sprite = SpriteLoader.LoadSprite(imageFilePath);
        myCalendarImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
        //if (myCalendarImage.sprite.bounds.extents.x > myCalendarImage.sprite.bounds.extents.y)
        float ratio = (float)myCalendarImage.texture.width / (float)myCalendarImage.texture.height;
        if(ratio > 1)
        {
            myAspectFitter.aspectRatio = ratio;
            myAspectFitter.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        }
        else
        {
            myAspectFitter.aspectRatio = ratio;
            myAspectFitter.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        }
    }

    public void ShowMyContent()
    {
        for (int i = 0; i < myContentList.Count; i++)
        {
            //dayPlannerComponent.contentPositions[i].transform.localPosition = new Vector3(FindContentAdjustment(i), dayPlannerComponent.contentPositions[i].transform.localPosition.y, dayPlannerComponent.contentPositions[i].transform.localPosition.z);
            myContentList[i].transform.parent.localPosition = new Vector3(FindContentAdjustment(myContentList[i].myContentPos), myContentList[i].transform.parent.localPosition.y, myContentList[i].transform.parent.localPosition.z);
            myContentList[i].ShowMyContentDateAndSubText();
            myContentList[i].Show();
            SetNewContentImagePos(myContentList[i], myContentList[i].myContentPos);
            //dayPlannerComponent.contentPositions[i].Show();
            myContentList[i].myContentPos.Show();
            if (!myContentList[i].hasVideo)
            {
                //myContainer.myViewer.AddADocument(myContentList[i].mediaImage.sprite);
                myContainer.myViewer.AddADocument((Texture2D)myContentList[i].mediaImage.texture);
                myContainer.myViewer.myDocumentDetails.Add(myContentList[i].details);
            }
        }
    }

    private void SetNewContentImagePos(DayPlannerContent content, ContentPositionComponent pos)
    {
        //if (myContentList[posLayout].mediaImage.sprite == null) return;
        if (content.mediaImage.texture == null) return;
        float adj = 0.0f;
        float adjY = 0.0f;
        //float aspectRatio = (myContentList[posLayout].mediaImage.sprite.bounds.extents.x * 2) /
        //    (myContentList[posLayout].mediaImage.sprite.bounds.extents.y * 2);
        float width = (float)content.mediaImage.texture.width;
        float height = (float)content.mediaImage.texture.height;
        float aspectRatio = (width) /
            (height);
        switch (pos.name)
        {
            case "ContentPosition1": //topLeft
                if (aspectRatio < 1.0f)
                {
                    float newWidth = 250 * aspectRatio;
                    adj = (250 - newWidth) / 2.0f;
                }
                break;
            case "ContentPosition2": //bottomLeft
                if (aspectRatio < 1.0f)
                {
                    float newWidth = 250 * aspectRatio;
                    adj = (250 - newWidth) / 2.0f;
                }
                break;
            case "ContentPosition3": //topRight
                if (aspectRatio < 1.0f)
                {
                    float newWidth = 250 * aspectRatio;
                    adj = (250 - newWidth) / -2.0f;
                }
                break;
            case "ContentPosition4": //bottomRight
                if (aspectRatio < 1.0f)
                {
                    float newWidth = 250 * aspectRatio;
                    adj = (250 - newWidth) / -2.0f;
                }
                break;
        }
        if(aspectRatio>1.0f)
        {
            float newHeight = 250 / aspectRatio;
            adjY = (250 - newHeight) / 2.0f;
        }
        content.mediaImage.transform.localPosition = new Vector3(adj, -adjY, 0);
    }

    private float FindContentAdjustment(ContentPositionComponent pos)
    {
        float adjustment = 25.0f + (myCalendarImage.rectTransform.rect.width / 2.0f) + (pos.GetComponent<RectTransform>().rect.width/*myContentList[posLayout].mediaImage.rectTransform.rect.width*/ / (2.0f));
        if(myCalendarImage.rectTransform.rect.width>1240.0f)
        {
            adjustment = adjustment - 15.0f;
        }
        if(pos.name == "ContentPosition1")
        {
            adjustment = -adjustment;
        }
        if(pos.name == "ContentPosition2")
        {
            adjustment = -adjustment;
        }
        
        return adjustment;
    }

    public void HideMyContent(float time)
    {
        for (int i = 0; i < myContentList.Count; i++)
        {
            myContentList[i].Hide(0.0f);
            //dayPlannerComponent.contentPositions[i].Hide(0.0f);
            myContentList[i].myContentPos.Hide(0.0f);
            myContentList[i].myContentPos.myContentSubText.text = null;
            myContentList[i].myContentPos.myContentSubText.text = null;
            dayPlannerComponent.contentPositions[i].transform.localPosition = new Vector3(0.0f, dayPlannerComponent.contentPositions[i].transform.localPosition.y, dayPlannerComponent.contentPositions[i].transform.localPosition.z);
        }        
        myContainer.myViewer.myDocuments.Clear();
        myContainer.myViewer.myDocumentDetails.Clear();
    }

    private void OnEnable()
    {
        gesture = GetComponent<CalendarPullGesture>();
        gesture.Pressed += pressedHandler;
        gesture.Pulled += pulledHandler;
        gesture.Released += releasedHandler;
        gesture.Cancelled += cancelledHandler;
        //myFlicker.StateChanged += pressedHandler;
    }

    private void OnDisable()
    {
        gesture.Pressed -= pressedHandler;
        gesture.Pulled -= pulledHandler;
        gesture.Released -= releasedHandler;
        gesture.Cancelled -= cancelledHandler;
        //myFlicker.StateChanged -= pressedHandler;
    }

    private void pressedHandler(object sender, System.EventArgs e)
    {
        myCalendarColor = myCalendarImage.color;
    }

    private void pulledHandler(object sender, System.EventArgs e)
    {
        if ((gesture.Force.x > 0.0f && (!dayPlannerComponent.HasAnotherCalendar(dayPlannerComponent.activeCalendarValue + 1)))||
        (gesture.Force.x < 0.0f && (!dayPlannerComponent.HasAnotherCalendar(dayPlannerComponent.activeCalendarValue - 1))))
        {
            gesture.Cancel();
            return;
        }
        currentMag = gesture.Force.magnitude;
        if (Mathf.Approximately(currentMag,previousMag)) return;
        if(myCalendarImage.color.a <= 0)
        {
            gesture.Cancel();
        }
        if (currentMag < previousMag)
        {
            FadeCalendarColors(false);
        }
        else
        {
            FadeCalendarColors(true);
        }
        previousMag = currentMag;
        //Debug.Log("Pulled " + gesture.Position);
    }

    private void FadeCalendarColors(bool fadeOut)
    {
        Color newColor = myCalendarImage.color;
        if (fadeOut)
        {
            newColor.a = newColor.a - Time.deltaTime;
        }
        else
        {
            newColor.a = newColor.a + Time.deltaTime;
        }
        myCalendarImage.color = newColor;
        for (int i = 0; i < myContentList.Count; i++)
        {
            myContentList[i].mediaImage.color = newColor;
        }
    }

    private void releasedHandler(object sender, System.EventArgs e)
    {
        if (currentMag < previousMag)
        {
            ResetCalendarColors();
            return;
        }
        if (gesture.Force.magnitude > 0.0f)
        {
            dayPlannerComponent.SwipeIcon.SetActive(false);
            ResetCalendarColors();
            if (gesture.Force.x > 0.0f)
            {
                dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue + 1);
                //float swipeLeft = 1.0f / dayPlannerComponent.myScrollbar.numberOfSteps;
                //dayPlannerComponent.myScrollbar.value = dayPlannerComponent.myScrollbar.value-swipeLeft;
            }
            else
            {
                dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue - 1);
                //float swipeRight = 1.0f / dayPlannerComponent.myScrollbar.numberOfSteps;
                //dayPlannerComponent.myScrollbar.value = dayPlannerComponent.myScrollbar.value + swipeRight;
            }
        }
    }

    private void cancelledHandler(object sender, System.EventArgs e)
    {
        if ((myCalendarImage.color.a > 0.0f) && (myCalendarImage.color.a <= 1.0f))
        {
            ResetCalendarColors();
            return;
        }
        if (gesture.PreviousForce.magnitude> 0.0f)
        {
            dayPlannerComponent.SwipeIcon.SetActive(false);
            ResetCalendarColors();
            if (gesture.PreviousForce.x > 0.0f)
            {
                dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue + 1);
                //float swipeLeft = 1.0f / dayPlannerComponent.myScrollbar.numberOfSteps;
                //dayPlannerComponent.myScrollbar.value = dayPlannerComponent.myScrollbar.value - swipeLeft;
            }
            else
            {
                dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue - 1);
                //float swipeRight = 1.0f / dayPlannerComponent.myScrollbar.numberOfSteps;
                //dayPlannerComponent.myScrollbar.value = dayPlannerComponent.myScrollbar.value + swipeRight;
            }
        }
    }

    private void ResetCalendarColors()
    {
        myCalendarImage.color = myCalendarColor;
        for (int i = 0; i < myContentList.Count; i++)
        {
            myContentList[i].mediaImage.color = myCalendarColor;
        }
        currentMag = 0.0f;
        previousMag = 0.0f;
    }

    //public void OnSwiped(FlickGesture flicker)
    //{
    //    if (flicker.State == Gesture.GestureState.Recognized)
    //    {
    //        dayPlannerComponent.SwipeIcon.SetActive(false);
    //        if (flicker.ScreenFlickVector.x > 0.0f)  //flicked Right
    //        {
    //            dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue - 1);
    //            dayPlannerComponent.myScrollbar.value -= 1;
    //        }
    //        else //flicked Left
    //        {
    //            dayPlannerComponent.SwitchCalendar(dayPlannerComponent.activeCalendarValue + 1);
    //            dayPlannerComponent.myScrollbar.value += 1;
    //        }
    //    }
    //}

    public void OnTapped(TapGesture tapped)
    {
        if (tapped.State == Gesture.GestureState.Recognized)
        {
            if (LeanTween.isTweening(gameObject)) return;
            LeanTween.cancel(gameObject);
            if (zoomed)
            {
                LeanTween.scale(this.gameObject, Vector3.one, .25f);
                zoomed = false;
            }
            else
            {
                LeanTween.scale(this.gameObject, new Vector3(1.5f, 1.5f, 1.5f), .25f);
                zoomed = true;
            }
        }
    }
}
