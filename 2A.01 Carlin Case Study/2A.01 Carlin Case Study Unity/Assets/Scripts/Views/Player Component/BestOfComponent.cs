﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NCCExternalComponentScripts.Proxies;
using UI.Pagination;

public class BestOfComponent : BaseComponent {

    //public PagedRect myPages;
    //private Vector3 initialScale = new Vector3(1, 1, 1);
    //public Vector3 initialPosition;
    public DocumentContainer myContainer;
    private Material originalMaterial;
    public Material ddsMaterial;

    public void SetUpMyData(List<BestOfDataModel> data)
    {
        myContainer.myViewer.CreateMyListOfDocuments();
        for (int i = 0; i < data.Count; i++)
        {
            string imageFilePath = CMSDataProxy.LocalMediaFilePath + data[i].image;
            //myContainer.myViewer.AddADocument(SpriteLoader.LoadSprite(imageFilePath));
            Texture2D tex = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            myContainer.myViewer.AddADocument(tex);
            myContainer.myViewer.myDocumentDetails.Add(data[i].text);
        }
    }
    public override void Hide(float time = 0.25f)
    {
        myContainer.myViewer.Hide();
        base.Hide();
        //myPages.ShowFirstPage();
    }

    public override void Show(float time = 0.25f)
    {
        DocumentViewer docView = myContainer.myViewer;
        //Page currentPage = myPages.Pages[myPages.CurrentPage - 1];
        //Transform currentTrans = currentPage.transform.GetChild(0);
        //Image testImage = currentTrans.GetComponent<Image>();
        docView.SetMyImage(docView.GetFirstDocument(), false);//(testImage.sprite, false);
        docView.ResetDocumentScaleAndPosition();
        docView.transform.SetAsLastSibling();
        docView.mainMenuButton.gameObject.SetActive(false);
        docView.Show();
        base.Show();
    }
}
