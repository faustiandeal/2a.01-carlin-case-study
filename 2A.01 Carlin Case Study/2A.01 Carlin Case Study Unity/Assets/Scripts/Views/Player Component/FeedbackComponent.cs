﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FeedbackComponent : BaseComponent
{
    public TextMeshProUGUI FeedBackText;

    public void SetText(string text)
    {
        FeedBackText.text = text;
    }
}
