﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PickAJokeComponent : BaseComponent {

    //public event Action<JokeDataModel> JokeKeyPicked;

    public TextMeshProUGUI SelectJokeLine1;
    public TextMeshProUGUI SelectJokeLine2;

    public TextMeshProUGUI Joke1Setup;
    public TextMeshProUGUI Joke1Punchline;

    public TextMeshProUGUI Joke2Setup;
    public TextMeshProUGUI Joke2Punchline;

    public Image JokeBox1;
    public Image JokeBox2;

    public CanvasGroup JokeButton1Canvas;
    public CanvasGroup JokeButton2Canvas;

    public JokeBoxButton JokeBox1Button;
    public JokeBoxButton JokeBox2Button;

    public Color SelectedBoxColor;

    //private JokeDataModel jokeOneData;
    //private JokeDataModel jokeTwoData;

    private string line1Text = "";
    private string line2Text = "";
    private string playerName = "";

    public void SetTextData(string name, string line1, string line2)
    {
        line1Text = line1;
        line2Text = line2;
        playerName = name;
        SelectJokeLine1.text = playerName + ", " + line1;
        SelectJokeLine2.text = line2;
    }

    private void SetButtonsInteractable(bool interactable)
    {
        JokeButton1Canvas.interactable = JokeButton1Canvas.blocksRaycasts = interactable;
        JokeButton2Canvas.interactable = JokeButton2Canvas.blocksRaycasts = interactable;
    }
}
