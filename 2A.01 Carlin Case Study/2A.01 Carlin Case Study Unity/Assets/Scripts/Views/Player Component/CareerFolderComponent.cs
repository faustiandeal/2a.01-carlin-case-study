﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CareerFolderComponent : BaseComponent {

    [HideInInspector]
    public AttractComponent Attract;
    [HideInInspector]
    public PlayerComponent myPlayerComponent;
    public Toggle TranscribeButton;
    public GameObject TranscriptionBubble;
    //[HideInInspector]
    //public Color originalButtonColor;
    //public Color[] openCarreerFolderColors = new Color[5];
    //public Color selectedButtonColor;
    public TextMeshProUGUI transcriptionText;
    public SwipedDocument swipedDocumentPrefab;
    public TextMeshProUGUI myTitle;
    public TextMeshProUGUI instructionalText;
    public Image myFolderColor;
    public Transform appearingDocument;
    public Transform swipedLocation;
    public Image swipeIcon;
    public Sprite transcribeButtonOn;
    public Sprite transcribeButtonOff;
    //public CareerHistoryCategoryDataModel myData;
    [HideInInspector]
    public DocumentContainer myContainer;
    [HideInInspector]
    public bool iAmOpen = false;
    private List<SwipedDocument> myArtifacts;
    private int lastChildCount = 0;
    private CareerHistoryCategoryDataModel myData;
    [HideInInspector]
    public bool texturesLoaded = false;
    [HideInInspector]
    public bool bytesLoaded = false;
    private List<TexData> myTextureBytes;

    public void CreateMyArtifacts(CareerHistoryCategoryDataModel data)
    {
        myData = data;
        myTitle.text = data.title;
        myArtifacts = new List<SwipedDocument>();
        myTextureBytes = new List<TexData>();
        for (int i = data.artifacts.Count -1; i >=0; i--)
        {
            SwipedDocument newArtifact = Instantiate<SwipedDocument>(swipedDocumentPrefab, appearingDocument, false);
            newArtifact.swipedLocation = swipedLocation;
            newArtifact.originalLocation = appearingDocument;
            newArtifact.myContainer = myContainer;
            newArtifact.originalLocalPosition = newArtifact.transform.localPosition;
            float randomCanter = 0.0f;//Random.Range(-2, 2);
            newArtifact.myCanter = randomCanter;
            newArtifact.transform.localEulerAngles = new Vector3(0,0,randomCanter);
            //string imageFilePath = CMSDataProxy.LocalMediaFilePath + data.artifacts[i].image;
            //newArtifact.myImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            //newArtifact.myImage = newArtifact.GetComponent<Image>();
            //newArtifact.myImage.sprite = SpriteLoader.LoadSpriteDXT(imageFilePath, TextureFormat.DXT1);
           //newArtifact.myImage.sprite = SpriteLoader.LoadSprite(imageFilePath);
            //newArtifact.myContainer.myViewer.AddADocument(newArtifact.myImage.sprite);
            //newArtifact.myContainer.myViewer.myDocumentDetails.Add("");
            if(data.artifacts[i].transcribedText != null && data.artifacts[i].transcribedText != "")
            {
                newArtifact.SetTranscribedText(data.artifacts[i].transcribedText);
            }
            myArtifacts.Add(newArtifact);
        }
        swipeIcon.gameObject.transform.SetAsLastSibling();
    }

    public void ResupplyMe()
    {
        TranscribeButtonPressed();
        foreach (SwipedDocument doc in myArtifacts)
        {
            //doc.SwipedLeft(true);
            doc.Resupply();
        }
    }

    public void LoadTextures()
    {
        Thread t = new Thread(new ThreadStart(ThreadLoad));
        t.Start();
    }

    private void ThreadLoad()
    {
        for (int i = myArtifacts.Count - 1; i >= 0; i--)
        {
            //DO SOMETHING ABOUT RESET OR MAIN MENU PUSHED?
            string imageFilePath = CMSDataProxy.LocalMediaFilePath + myData.artifacts[i].image;
            myTextureBytes.Add(SpriteLoader.CopyTextureToMemory(imageFilePath, TextureFormat.DXT5));
        }
        bytesLoaded = true;
    }

    public void LoadTextureFromBytes()
    {
        for (int i = myArtifacts.Count - 1; i >= 0; i--)
        {
            string filePath = CMSDataProxy.LocalMediaFilePath + myData.artifacts[i].image;
            if (filePath.Contains(".png") && !filePath.Contains(".dds"))
            {
                filePath = filePath.Replace("media", "temp") + ".dds";
            }
            if (filePath.Contains(".jpg") || filePath.Contains(".jpeg"))
            {
                myArtifacts[i].myImage.texture = SpriteLoader.LoadTexture(filePath, false);
                continue;
            }
            TextureFormat textureFormat = myTextureBytes[i].myTextureFormat;
            Texture2D texture = new Texture2D(myTextureBytes[i].width, myTextureBytes[i].height, textureFormat, false);
            //texture.alphaIsTransparency = true;
            texture.name = filePath;
            texture.LoadRawTextureData(myTextureBytes[i].myBytes);
            texture.Apply();
            myArtifacts[i].myImage.texture = texture;
        }
        texturesLoaded = true;
    }

    public IEnumerator ApplyMyImages(System.Action<bool> callback)
    {
        for (int i = myArtifacts.Count - 1; i >= 0; i--)
        {
            //DO SOMETHING ABOUT RESET OR MAIN MENU PUSHED?
            string imageFilePath = CMSDataProxy.LocalMediaFilePath + myData.artifacts[i].image;
            myArtifacts[i].myImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            yield return null;
        }
        callback(true);
    }

    public void Update()
    {
        if (appearingDocument.childCount > 0)
        {
            swipeIcon.gameObject.SetActive(false);
            if (appearingDocument.childCount != lastChildCount)
            {
                lastChildCount = appearingDocument.childCount;
                if (lastChildCount > 0)
                {
                    SwipedDocument theDoc = appearingDocument.GetChild(appearingDocument.childCount - 1).gameObject.GetComponent<SwipedDocument>();
                    if ((theDoc != null) && (theDoc.HasTranscribedText()))
                    {
                        HideButton();
                        ShowButton();
                    }
                    else
                    {
                        HideButton();
                    }
                }
            }
        }
        else
        {
            if (swipedLocation.childCount == 0)
            {
                swipeIcon.gameObject.SetActive(true);
            }
            lastChildCount = 0;
            HideButton();
        }
    }

    private void HideButton()
    {
        if (TranscribeButton.isOn)
        {
            TranscribeButton.isOn = false;
            TranscribeButtonPressed();
        }
        TranscribeButton.gameObject.SetActive(false);
        TranscribeButton.transform.localPosition = Vector3.zero;
    }

    private void ShowButton()
    {
        if (lastChildCount > 0)
        {
            RawImage document = appearingDocument.GetChild(appearingDocument.childCount-1).GetComponent<RawImage>();
            TranscribeButton.gameObject.transform.localPosition = appearingDocument.transform.localPosition;
            float imgWidth = document.rectTransform.sizeDelta.x;//document.bounds.extents.x;
            float imgHeight = document.rectTransform.sizeDelta.y;//document.bounds.extents.y;
            float aspectRatio = (imgWidth) /
                (imgHeight);
            RectTransform imgRect = appearingDocument.GetChild(appearingDocument.childCount-1).gameObject.GetComponent<RectTransform>();
            float adjustmentX = 0;
            float adjustmentY = 0;
            if (aspectRatio < 1) //width less than height
            {
                adjustmentX = (imgRect.rect.height * aspectRatio) / 2.0f;//(imgWidth - diff);
                adjustmentY = -((imgRect.rect.height / 2.0f));
            }
            else
            {
                adjustmentX = (imgRect.rect.width / 2.0f);
                adjustmentY = -(imgRect.rect.width / aspectRatio) / 2.0f;
            }
            TranscribeButton.gameObject.transform.localPosition = new Vector3(TranscribeButton.gameObject.transform.localPosition.x+ adjustmentX, TranscribeButton.gameObject.transform.localPosition.y+ adjustmentY, 0);
            TranscribeButton.gameObject.SetActive(true);
        }
    }

    public void TranscribeButtonPressed()
    {
        Image myImage = TranscribeButton.gameObject.GetComponent<Image>();
        if (TranscribeButton.isOn)
        {
            TextMeshProUGUI myText = TranscribeButton.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            myText.color = TranscribeButton.colors.normalColor;
            myImage.sprite = transcribeButtonOn;
            transcriptionText.text = appearingDocument.GetChild(appearingDocument.childCount - 1).gameObject.GetComponent<SwipedDocument>().GetTranscribedText();
            ShadowImages(true);
            TranscriptionBubble.gameObject.SetActive(true);
        }
        else
        {
            myImage.sprite = transcribeButtonOff;
            TextMeshProUGUI myText = TranscribeButton.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            myText.color = TranscribeButton.colors.pressedColor;
            ShadowImages(false);
            TranscriptionBubble.gameObject.SetActive(false);
        }
    }

    public void TranscribeBubbleClose()
    {
        TranscribeButton.isOn = false;
        TranscribeButtonPressed();
    }

    public void PopulateMyDocuments()
    {
        for(int i= 0;i < myArtifacts.Count;i++)
        {
            //string imageFilePath = CMSDataProxy.LocalMediaFilePath + myData.artifacts[i].image;
            //myArtifacts[i].myImage.sprite = SpriteLoader.LoadSprite(imageFilePath);
            //myArtifacts[i].myImage.sprite.texture.Apply();
            myArtifacts[i].myContainer.myViewer.AddADocument((Texture2D)myArtifacts[i].myImage.texture);
            myArtifacts[i].myContainer.myViewer.myDocumentDetails.Add("");
            myArtifacts[i].Show();
        }
    }

    private void ShadowImages(bool dark)
    {
        for (int i = 0; i < swipedLocation.childCount; i++)
        {
            //Image imgSprite = swipedLocation.GetChild(i).GetComponent<Image>();
            RawImage imgSprite = swipedLocation.GetChild(i).GetComponent<RawImage>();
            if (dark)
            {
                Color newColor = new Color(.8f, .8f, .8f);
                imgSprite.color = newColor;
            }
            else
            {
                imgSprite.color = Color.white;
            }
        }
    }

    //private void TransformTransBubble()
    //{
    //    RectTransform imgRect = swipedLocation.GetChild(0).gameObject.GetComponent<RectTransform>();
    //    RectTransform bubbleRect = TranscriptionBubble.gameObject.GetComponent<RectTransform>();
    //    bubbleRect.sizeDelta = new Vector2(bubbleRect.sizeDelta.x/*transcriptionText.rectTransform.rect.width + 5.0f*/, transcriptionText.rectTransform.rect.height + 5.0f);
    //    TranscriptionBubble.transform.localPosition = Vector3.zero;
    //    float adjustmentX = swipedLocation.GetChild(0).gameObject.transform.localPosition.x - (imgRect.rect.width / 2.0f) - (bubbleRect.rect.width / 2.0f);
    //    TranscriptionBubble.transform.localPosition = new Vector3(adjustmentX, swipedLocation.GetChild(0).transform.localPosition.y, 0);
    //}

    private void Awake()
    {
        myPlayerComponent = FindObjectOfType<PlayerComponent>();
        Attract = FindObjectOfType<AttractComponent>();
        Attract.Reset += Reset;
    }

    public void CloseButtonPressed()
    {
        if (iAmOpen)
        {
            myPlayerComponent.myAudioPlayer.PlayOneShot(myPlayerComponent.closeFolderClip);
            myPlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha = 0.0f;
            myContainer.myViewer.myDocuments.Clear();
            myContainer.myViewer.myDocumentDetails.Clear();
            for (int i = myArtifacts.Count - 1; i >= 0; i--)
            {
                myArtifacts[i].myImage.texture = null;
            }
        }
    }

    public override void Hide(float time = 0.25f)
    {
        CloseButtonPressed();
        ResupplyMe();
        base.Hide();
        //Resources.UnloadUnusedAssets();
    }

    private void Reset()
    {
        //CloseButtonPressed();
        Hide();
        //ResupplyMe();
        iAmOpen = false;
    }
}
