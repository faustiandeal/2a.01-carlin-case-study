﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BaggieFolderComponent : BaseComponent {

    public float documentHeight = 500f;
    public float documentWidth = 500f;
    public Toggle TranscribeButton;
    public GameObject TranscriptionBubble;
    //public Color originalButtonColor;
    //public Color selectedButtonColor;
    public TextMeshProUGUI transcriptionText;
    public SwipedDocument swipedDocumentPrefab;
    public TextMeshProUGUI baggieLabelText;
    public Image swipeIcon;
    public Sprite swipeBothImage;
    public Sprite swipeRightImage;
    public Sprite swipeLeftImage;
    public Sprite transcribeButtonOn;
    public Sprite transcribeButtonOff;
    public Transform swipedLocation;
    public Transform contentCreationPoint;
    public Transform contentViewedPoint;
    [HideInInspector]
    public BaseComponent FadeObject;
    [HideInInspector]
    public GameObject myScrollbar;
    [HideInInspector]
    public DocumentContainer myContainer;
    [HideInInspector]
    public JokeNotesComponent myJokeNotesComponent;
    private List<SwipedDocument> myArtifacts;
    private int lastChildCount = 0;
    [HideInInspector]
    public bool iAmOpen = false;
    private bool firstSwipe = true;
    private JokeNotesCategoryDataModel myData;

    public void CreateMyArtifacts(JokeNotesCategoryDataModel data, Transform myParent/*, List<Sprite>myContainedSprites*/)
    {
        myData = data;
        myArtifacts = new List<SwipedDocument>();
        for (int i = data.jokeNotesImages.Count -1; i >=0; i--)
        {
            SwipedDocument newArtifact = Instantiate<SwipedDocument>(swipedDocumentPrefab, contentCreationPoint, false);
            newArtifact.baggieDoc = true;
            newArtifact.baggieLocation = contentViewedPoint;
            newArtifact.swipedLocation = swipedLocation;
            newArtifact.originalLocation = contentCreationPoint;
            newArtifact.originalLocalPosition = newArtifact.transform.localPosition;
            newArtifact.transform.localScale = new Vector3(.75f, .75f, 1.0f);
            newArtifact.myContainer = myContainer;
            float randomCanter = 0.0f;//Random.Range(-2, 2);
            newArtifact.myCanter = randomCanter;
            newArtifact.transform.localEulerAngles = new Vector3(0, 0, randomCanter);
            //string imageFilePath = CMSDataProxy.LocalMediaFilePath + data.jokeNotesImages[i].image;
            //newArtifact.myImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            //newArtifact.myImage = newArtifact.GetComponent<Image>();
            //newArtifact.myImage.sprite = SpriteLoader.LoadSprite(imageFilePath);
            newArtifact.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, documentHeight);
            newArtifact.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, documentWidth);
            if(data.jokeNotesImages[i].transcribedText != null && data.jokeNotesImages[i].transcribedText != "")
            {
                newArtifact.SetTranscribedText(data.jokeNotesImages[i].transcribedText);
            }
            myArtifacts.Add(newArtifact);
        }
    }

    public void ResupplyMe()
    {
        TranscribeButtonPressed();
        foreach (SwipedDocument doc in myArtifacts)
        {
            //doc.SwipedLeft(true);
            doc.Resupply();
        }
        firstSwipe = true;
    }

    public IEnumerator ApplyMyImages(System.Action<bool> callback)
    {
        for (int i = myArtifacts.Count - 1; i >= 0; i--)
        {
            string imageFilePath = CMSDataProxy.LocalMediaFilePath + myData.jokeNotesImages[i].image;
            myArtifacts[i].myImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            yield return null;
        }
        callback(true);
    }

    public void Update()
    {
        if(iAmOpen)
        {
            if(firstSwipe)
            {
                contentCreationPoint.GetChild(contentCreationPoint.childCount - 1).GetComponent<SwipedDocument>().SwipedRight();
                firstSwipe = false;
            }
        }
        if (contentViewedPoint.childCount == 0)
        {
            swipeIcon.gameObject.SetActive(true);
        }
        else
        {
            swipeIcon.gameObject.SetActive(false);
        }
        if (swipedLocation.childCount > 0)
        {
            if (swipedLocation.childCount != lastChildCount)
            {
                lastChildCount = swipedLocation.childCount;
                SwipedDocument theDoc = swipedLocation.GetChild(swipedLocation.childCount - 1).gameObject.GetComponent<SwipedDocument>();
                if ((theDoc != null) && (theDoc.HasTranscribedText()))
                {
                    ShowButton();
                }
                else
                {
                    HideButton();
                }
            }
        }
        else
        {
            lastChildCount = 0;
            HideButton();
        }
    }

    private void HideButton()
    {
        if (TranscribeButton.isOn)
        {
            TranscribeButton.isOn = false;
            TranscribeButtonPressed();
        }
        TranscribeButton.gameObject.SetActive(false);
        TranscribeButton.transform.localPosition = Vector3.zero;
    }

    private void ShowButton()
    {
        RawImage swipedImg = swipedLocation.GetChild(0).gameObject.GetComponent<RawImage>();
        float imgWidth = swipedImg.rectTransform.sizeDelta.x;//swipedImg.sprite.bounds.extents.x / 2.0f;
        float imgHeight = swipedImg.rectTransform.sizeDelta.y;//swipedImg.sprite.bounds.extents.y / 2.0f;
        RectTransform buttonRect = TranscribeButton.gameObject.GetComponent<RectTransform>();
        float adjustmentX = swipedLocation.GetChild(0).gameObject.transform.localPosition.x - (imgWidth/2.0f);
        float adjustmentY = swipedLocation.GetChild(0).gameObject.transform.localPosition.y - (imgHeight/2.0f) - (buttonRect.rect.height/2.0f);
        TranscribeButton.gameObject.transform.localPosition = new Vector3(adjustmentX, adjustmentY, 0);
        TranscribeButton.gameObject.SetActive(true);
    }

    public void TranscribeButtonPressed()
    {
        Image myImage = TranscribeButton.gameObject.GetComponent<Image>();
        if (TranscribeButton.isOn)
        {
            TextMeshProUGUI myText = TranscribeButton.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            myText.color = TranscribeButton.colors.normalColor;
            myImage.sprite = transcribeButtonOn;
            transcriptionText.text = swipedLocation.GetChild(swipedLocation.childCount - 1).gameObject.GetComponent<SwipedDocument>().GetTranscribedText();
            TransformTransBubble();
            ShadowImages(true);
            TranscriptionBubble.gameObject.SetActive(true);
        }
        else
        {
            myImage.sprite = transcribeButtonOff;
            TextMeshProUGUI myText = TranscribeButton.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            myText.color = TranscribeButton.colors.pressedColor;
            ShadowImages(false);
            TranscriptionBubble.gameObject.SetActive(false);
        }
    }

    public void TranscribeBubbleClose()
    {
        TranscribeButton.isOn = false;
        TranscribeButtonPressed();
    }

    private void ShadowImages(bool dark)
    {
        for (int i = 0; i < contentCreationPoint.childCount; i++)
        {
            RawImage imgSprite = contentCreationPoint.GetChild(i).GetComponent<RawImage>();
            if (dark)
            {
                Color newColor = new Color(.8f, .8f, .8f);
                imgSprite.color = newColor;
            }
            else
            {
                imgSprite.color = Color.white;
            }
        }
    }

    private void TransformTransBubble()
    {
        RectTransform imgRect = swipedLocation.GetChild(0).gameObject.GetComponent<RectTransform>();
        RectTransform bubbleRect = TranscriptionBubble.gameObject.GetComponent<RectTransform>();
        bubbleRect.sizeDelta = new Vector2(bubbleRect.sizeDelta.x/*transcriptionText.rectTransform.rect.width + 5.0f*/, transcriptionText.rectTransform.rect.height + 5.0f);
        TranscriptionBubble.transform.localPosition = Vector3.zero;
        float adjustmentX = swipedLocation.GetChild(0).gameObject.transform.localPosition.x - (imgRect.rect.width / 2.0f) -(bubbleRect.rect.width/2.0f);
        TranscriptionBubble.transform.localPosition = new Vector3(adjustmentX, swipedLocation.GetChild(0).transform.localPosition.y, 0);
    }

    public void PopulateMyDocViewer()
    {
        for (int i = 0; i < myArtifacts.Count; i++)
        {
            //myArtifacts[i].myContainer.myViewer.AddADocument(myArtifacts[i].myImage.sprite);
            myArtifacts[i].myContainer.myViewer.AddADocument((Texture2D)myArtifacts[i].myImage.texture);
            myArtifacts[i].myContainer.myViewer.myDocumentDetails.Add("");
            myArtifacts[i].Show();
        }
    }

    public override void Hide(float time = 0.25f)
    {
        if (iAmOpen)
        {
            myContainer.myViewer.myDocuments.Clear();
            myContainer.myViewer.myDocumentDetails.Clear();
            iAmOpen = false;
            base.Hide(time);
            LeanTween.delayedCall(time, () =>
            {
                ResupplyMe();
                FadeObject.Show();
                myScrollbar.SetActive(true);
                myJokeNotesComponent.instructionalText.text = myJokeNotesComponent.myPlayerComponent.textData.jokeNotesInstructionalText;
                for (int i = myArtifacts.Count - 1; i >= 0; i--)
                {
                    myArtifacts[i].myImage.texture = null;
                }
                //Resources.UnloadUnusedAssets();
            });
        }
    }
}
