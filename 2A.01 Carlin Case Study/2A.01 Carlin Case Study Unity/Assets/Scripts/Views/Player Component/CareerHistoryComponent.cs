﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CareerHistoryComponent : BaseComponent {

    public TextMeshProUGUI DescriptionText;
    public TextMeshProUGUI InstructionalText;
    public CareerScrollerController ScrollerController;
    public DocumentViewer myViewer;
    public CanvasGroup menuFadeImageCanvas;

    public void SetUpMyData(CareerHistoryDataModel data)
    {
        DescriptionText.text = data.description;
        ScrollerController.PassDataToMe(data);
    }

    public override void Hide(float time = 0.25f)
    {
        myViewer.Hide();
        ScrollerController.allFolders.ForEach(o =>
        {
            o.Hide();
            o.ResupplyMe();
        });
        base.Hide();
    }
}
