﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using NCCExternalComponentScripts.Wrappers;
using TMPro;

public class VideoPlayerComponent : BaseComponent {

    //public VideoPlayer myVideoPlayer;
    //public VideoUIWrapper myVideoWrapper;
    //public VideoUIComponent myVideoComponent;
    public BaseVideoPlayer myVideoPlayer;
    //public VideoGUIDisplay myDisplay;
    public TextMeshProUGUI mySubText;
    public TextMeshProUGUI myTitle;
    public VideoUIWrapper myVideoUIWrapper;
    private bool isDraggingScrubber = false;


    public void SetUpMySubText(string textToDisplay, string srt)
    {
        mySubText.text = textToDisplay;
        myVideoUIWrapper.SetCaptionData(SRTParser.FilePathToCaptions(srt));
    }

    public void SetUpMyTitle(string textToDisplay)
    {
        myTitle.text = textToDisplay;
    }

    public void ClearVideoTitleAndSubText()
    {
        myTitle.text = "";
        mySubText.text = "";
    }

    public void OnClose()
    {
        if (myVideoPlayer != null)
        {
            myVideoPlayer.FadeVideo(0.0f, 0.0f);
            myVideoPlayer.UnloadVideo();
        }
    }
    void Start()
    {
        if (myVideoUIWrapper != null)
        {
            myVideoUIWrapper.ScrubberGrabbed += () =>
            {
                isDraggingScrubber = true;
                myVideoPlayer.Pause();
            };

            myVideoUIWrapper.ScrubberReleased += () =>
            {
                isDraggingScrubber = false;
                myVideoPlayer.Play();
            };
        }
    }
    private void Update()
    {
        if (myVideoUIWrapper != null && Canvas.alpha>0.0f)
        {
            if (!isDraggingScrubber)
            {
                myVideoUIWrapper.ProgressValue = NormalizedPosition;
            }
            else
            {
                NormalizedPosition = myVideoUIWrapper.ProgressValue;
            }
            myVideoUIWrapper.SetCurrentTime(myVideoPlayer.PositionSeconds);
        }
    }

    public float NormalizedPosition
    {
        get
        {
            return myVideoPlayer.NormalizedPosition;
        }
        set
        {
            //Debug.Log("Frame Before: " + LocalVideoPlayer.frame);
            //LocalVideoPlayer.frame = (long)(LocalVideoPlayer.frameCount * value);
            myVideoPlayer.NormalizedPosition = value;
            //Debug.Log("Frame After: " + LocalVideoPlayer.frame + " Value: " + value);
        }
    }

    //private void OnGUI()
    //{
    //    if (GUI.Button(new Rect(0f, 0f, 100f, 20f), "Load Video"))
    //    {
    //        //player.Load("AVProVideoSamples/BigBuckBunny_720p30.mp4", RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder);
    //        myVideoPlayer.Load("CMS_data/cache/media/id_485_8016_165_Monologue_George_Carlin_-_SNL_TEMP.mov", RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL);
    //    }

    //    if (GUI.Button(new Rect(0f, 30f, 100f, 20f), "Play Video"))
    //    {
    //        myVideoPlayer.Play();
    //    }
    //}
}
