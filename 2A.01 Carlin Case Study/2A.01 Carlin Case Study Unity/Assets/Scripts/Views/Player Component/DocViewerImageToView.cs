﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TouchScript.Behaviors;
using TouchScript.Gestures.TransformGestures;

public class DocViewerImageToView : BaseComponent
{

    public DocumentViewer myViewer;
    public TextMeshProUGUI imageDetails;
    public Transform myScale;
    public AspectRatioFitter myAspect;
    [HideInInspector]
    public ScreenTransformGesture myTransformer;
    public bool amDayPlanner = false;
    public bool amBestOf = false;
    public GameObject pinchZoomIcon;
    private float maxZoomScale = 3.0f;
    private float originalScale = 0.0f;
    private Transform originalTransform;
    //private Image myImage;
    private RawImage myImage;
    [HideInInspector]
    public bool resetting = false;

    public void Awake()
    {
        originalTransform = myScale;
        originalScale = myScale.localScale.magnitude;
        myTransformer = GetComponent<ScreenTransformGesture>();
        //myImage = GetComponent<Image>();
        myImage = GetComponent<RawImage>();
        //Material myMaterial = myImage.material;
    }

    public void OnTransform(TouchScript.Gestures.TransformGestures.ScreenTransformGesture scaledEvent)
    {
        if (pinchZoomIcon != null)
        {
            pinchZoomIcon.gameObject.SetActive(false);
        }
        if (myScale.localScale.magnitude > new Vector3(maxZoomScale, maxZoomScale, 1.0f).magnitude)
        {
            if (scaledEvent.DeltaScale > 1.0f)
            {
                scaledEvent.Cancel();
            }
        }
        if (myScale.localScale.magnitude < new Vector3(1.0f, 1.0f, 1.0f).magnitude)
        {
            if (scaledEvent.DeltaScale < 1.0f)
            {
                scaledEvent.Cancel();
            }
        }
        if (myViewer != null)
        {
            myViewer.SetUpPinchIconAndCloseButton();
        }
    }

    public void LateUpdate()
    {
        if(myViewer.Canvas.alpha > 0.0f && (myImage.texture != null))
        {
            if(this.transform.localScale.magnitude > originalScale)
            {
                myTransformer.Type = TransformGesture.TransformType.Translation | TransformGesture.TransformType.Scaling;
            }
            else
            {
                myTransformer.Type = TransformGesture.TransformType.Scaling;
            }
            if (myViewer != null)
            {
                myViewer.SetUpPinchIconAndCloseButton();
            }
            if(!LeanTween.isTweening(this.gameObject) && !resetting)
            {
                RectTransform rt = (RectTransform)myViewer.transform;
                if ((myScale.localPosition.x > (rt.rect.width/2.0f)) || (myScale.localPosition.x < -1 * (rt.rect.width/2.0f))
                    || (myScale.localPosition.y > (rt.rect.height/2.0f)) || (myScale.localPosition.y < -1 * (rt.rect.height/2.0f)))
                {
                    resetting = true;
                    LeanTween.moveLocal(this.gameObject, Vector3.zero, .25f).setOnComplete(() =>
                    {
                        resetting = false;
                    });
                }
            }
        }
    }
}
