﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class MainGameOverlayComponent : MonoBehaviour {

    public event Action ReadyButtonPressed;

    public PlayerComponent componentPlayer;

    public CanvasGroup IntroMenuCanvas;
    public CanvasGroup IntroBodyTextCanvas;
    public CanvasGroup IntroSwipeTextCanvas;
    public CanvasGroup SwipeIcon;

    public CanvasGroup TrunkOpening;

    public CanvasGroup MainMenu;

    [HideInInspector]
    public TextMeshProUGUI IntroBodyText;
    [HideInInspector]
    public TextMeshProUGUI IntroSwipeText;

    //public TextMeshProUGUI RoundText; 

    //public void ShowReadyComponent()
    //{
    //    LeanTween.cancel(ReadyCanvas.gameObject);
    //    LeanTween.alphaCanvas(ReadyCanvas, 1, 0.5f).setEase(LeanTweenType.easeOutQuad).setOnComplete(() => {

    //        ReadyCanvas.interactable = ReadyCanvas.blocksRaycasts = true;
    //    });
    //}

    //public void HideReadyComponent()
    //{
    //    ReadyCanvas.interactable = ReadyCanvas.blocksRaycasts = false;
    //    LeanTween.cancel(ReadyCanvas.gameObject);
    //    LeanTween.alphaCanvas(ReadyCanvas, 0, 0.5f).setEase(LeanTweenType.easeInQuad);
    //}

    //public void ShowWaitingForOpponent()
    //{
    //    LeanTween.cancel(WaitingForOpponentTextCanvas.gameObject);
    //    LeanTween.cancel(OpponentOverlayCanvas.gameObject);
    //    WaitingForOpponentTextCanvas.alpha = 1;

    //    LeanTween.alphaCanvas(OpponentOverlayCanvas, 1, 0.5f).setEase(LeanTweenType.easeOutQuad);
    //}

    //public void OpponentFound()
    //{
    //    LeanTween.cancel(WaitingForOpponentTextCanvas.gameObject);
    //    LeanTween.alphaCanvas(WaitingForOpponentTextCanvas, 0, 0.25f).setEase(LeanTweenType.easeInQuad).setOnComplete(() =>
    //    {
    //        LeanTween.cancel(WaitingForOpponentTextCanvas.gameObject);
    //        LeanTween.alphaCanvas(WaitingForOpponentTextCanvas, 1, 0.25f).setEase(LeanTweenType.easeOutQuad);
    //    });
    //}

    //public void HideWaitingForOpponent()
    //{
    //    LeanTween.cancel(OpponentOverlayCanvas.gameObject);
    //    LeanTween.cancel(WaitingForOpponentTextCanvas.gameObject);
    //    LeanTween.alphaCanvas(OpponentOverlayCanvas, 0, 0.5f).setEase(LeanTweenType.easeInQuad);
    //}

    //public void ShowDividingLine()
    //{
    //    //LeanTween.cancel(DividingLineCanvas.gameObject);
    //    LeanTween.alphaCanvas(DividingLineCanvas, 1, 0.5f).setEase(LeanTweenType.easeOutQuad);
    //}

    //public void HideDividingLine()
    //{
    //    //LeanTween.cancel(DividingLineCanvas.gameObject);
    //    LeanTween.alphaCanvas(DividingLineCanvas, 0, 0.5f).setEase(LeanTweenType.easeInQuad);
    //}

    //public void ShowRoundText()
    //{
    //    LeanTween.cancel(RoundTextCanvas.gameObject);
    //    LeanTween.moveLocalY(DividingLineCanvas.gameObject, -120F, 0.5f).setEase(LeanTweenType.easeOutQuad);
    //    LeanTween.alphaCanvas(RoundTextCanvas, 1, 0.7f).setEase(LeanTweenType.easeOutQuad).setDelay(0.35f);
    //}

    //public void HideRoundText()
    //{
    //    LeanTween.cancel(RoundTextCanvas.gameObject);
    //    LeanTween.moveLocalY(DividingLineCanvas.gameObject, 0f, 0.5f).setEase(LeanTweenType.easeOutQuad);
    //    LeanTween.alphaCanvas(RoundTextCanvas, 0, 0.5f).setEase(LeanTweenType.easeInQuad);
    //}

    #region DataSetters

    public void SetTextData(ConfigurableTextDataModel data)
    {
        //IntroBodyText.text = data.IntroBody;
        //IntroCalloutText.text = data.IntroCallToAction;
    }

    #endregion


    #region ButtonEvents

    public void ImReadyButtonPressed()
    {
        if (ReadyButtonPressed != null) ReadyButtonPressed();
    }

    #endregion
}
