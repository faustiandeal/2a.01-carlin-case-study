﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuComponent : BaseComponent {

    public GameObject TrunkLid;
    public GameObject TrunkBottom;
    public GameObject JokeNotesFolder;
    public GameObject CareerHistoryFolder;
    public GameObject DayPlannerFolder;
    public BaseComponent DayPlannerShadow;
    public BaseComponent JokeNotesShadow;
    //public BaseComponent spotlightObject;
    public TextMeshProUGUI DescriptiveText;
    public TextMeshProUGUI TapInstructions;
    public TextMeshProUGUI TapInstructions2;

    public override void Show(float time = 0.25F)
    {
        DescriptiveText.gameObject.GetComponent<BaseComponent>().Show(.25f);
        TapInstructions.gameObject.GetComponent<BaseComponent>().Show(.50f);
        TapInstructions2.gameObject.GetComponent<BaseComponent>().Show(.50f);
        DayPlannerShadow.Show(.50f);
        JokeNotesShadow.Show(.50f);
        //spotlightObject.Show(.50f);
        base.Show(time);
    }

    public override void Hide(float time = 0.25F)
    {
        DescriptiveText.gameObject.GetComponent<BaseComponent>().Hide();
        TapInstructions.gameObject.GetComponent<BaseComponent>().Hide();
        TapInstructions2.gameObject.GetComponent<BaseComponent>().Hide();
        DayPlannerShadow.Hide();
        JokeNotesShadow.Hide();
        //spotlightObject.Hide();
        base.Hide(time);
    }
}
