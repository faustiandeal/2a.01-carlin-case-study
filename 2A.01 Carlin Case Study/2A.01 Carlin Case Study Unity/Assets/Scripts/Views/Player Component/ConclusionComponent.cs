﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConclusionComponent : BaseComponent {

    public TextMeshProUGUI ConclusionText;
    public TextMeshProUGUI WinnerName;

    public TextMeshProUGUI FeedbackText;
    public TextMeshProUGUI PlayerScore;
    public TextMeshProUGUI OpponentScore;

    public Image CaptureImage;

    public void SetData(string conclusion, ConclusionDataObject data)
    {
        ConclusionText.text = conclusion;
        if (data.Capture != null)
        {
            CaptureImage.sprite = Sprite.Create(data.Capture, new Rect(0, 0, data.Capture.width, data.Capture.height), new Vector2(0.5f, 0.5f), 1);
        }

        FeedbackText.text = data.Feedback;
        PlayerScore.text = data.PlayerScore;
        OpponentScore.text = data.OpponentScore;
    }
}

public class ConclusionDataObject
{
    public bool IsWinner;
    public Texture2D Capture;
    public string PlayerScore;
    public string OpponentScore;
    public string Feedback;
}