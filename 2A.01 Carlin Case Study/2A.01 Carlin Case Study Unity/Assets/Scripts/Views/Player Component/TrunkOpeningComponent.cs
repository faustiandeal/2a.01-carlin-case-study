﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TrunkOpeningComponent : BaseComponent {

    public PlayerComponent myPlayerComponent;
    public GameObject TrunkLid;
    public GameObject TrunkBottom;
    public GameObject JokeNotesFolder;
    public GameObject CareerHistoryFolder;
    public GameObject DayPlannerFolder;
    //public GameObject CursoryGlanceFolder;
    public Transform trunkFinalPos;
    public Transform trunkBottomFinalPos;
    public Transform jokeNotesUpPos;
    public Transform jokeNotesFinalPos;
    public Transform careerHistoryFinalPos;
    public Transform dayPlannerUpPos;
    public Transform dayPlannerFinalPos;
    //public Transform cursoryGlanceFinalPos;
    [HideInInspector]
    public float animationTime = 2.0f;
    private Vector3 trunkTopOriginalPos;
    private Vector3 trunkBottomOriginalPos;
    private Vector3 jokeNotesOriginalPos;
    private Vector3 dayPlannerOriginalPos;
    private Vector3 careerHistoryOriginalPos;
    //private Vector3 cursoryGlanceOriginalPos;

    public void Start()
    {
        trunkTopOriginalPos = TrunkLid.transform.position;
        trunkBottomOriginalPos = TrunkBottom.transform.position;
        jokeNotesOriginalPos = JokeNotesFolder.transform.position;
        dayPlannerOriginalPos = DayPlannerFolder.transform.position;
        careerHistoryOriginalPos = CareerHistoryFolder.transform.position;
        //cursoryGlanceOriginalPos = CursoryGlanceFolder.transform.position;
    }

    public void AnimateTrunk()
    {
        LeanTween.moveLocal(TrunkLid, trunkFinalPos.localPosition, animationTime).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.scale(TrunkLid, trunkFinalPos.localScale, animationTime).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.moveLocal(TrunkBottom, trunkBottomFinalPos.localPosition, animationTime).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.scale(TrunkBottom, trunkBottomFinalPos.localScale, animationTime).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.moveLocal(CareerHistoryFolder, careerHistoryFinalPos.localPosition, animationTime).setEase(LeanTweenType.easeInOutQuad);
        //LeanTween.move(CursoryGlanceFolder, cursoryGlanceFinalPos, animationTime).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.moveLocal(JokeNotesFolder, jokeNotesUpPos.localPosition, animationTime/ 2.0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.moveLocal(DayPlannerFolder, dayPlannerUpPos.localPosition, animationTime / 2.0f).setEase(LeanTweenType.easeInOutQuint);
        LeanTween.delayedCall(animationTime / 2.0f, () =>
        {
            LeanTween.moveLocal(DayPlannerFolder, dayPlannerFinalPos.localPosition, animationTime / 2.0f).setEase(LeanTweenType.easeInOutQuad);
            LeanTween.moveLocal(JokeNotesFolder, jokeNotesFinalPos.localPosition, animationTime / 2.0f).setEase(LeanTweenType.easeInOutQuad);
        });
    }

    public void ResetPositions()
    {
        TrunkLid.transform.position = trunkTopOriginalPos;
        TrunkLid.transform.localScale = Vector3.one;
        TrunkBottom.transform.position = trunkBottomOriginalPos;
        TrunkBottom.transform.localScale = Vector3.one;
        JokeNotesFolder.transform.position = jokeNotesOriginalPos;
        DayPlannerFolder.transform.position = dayPlannerOriginalPos;
        CareerHistoryFolder.transform.position = careerHistoryOriginalPos;
        //CursoryGlanceFolder.transform.position = cursoryGlanceOriginalPos;
    }
}
