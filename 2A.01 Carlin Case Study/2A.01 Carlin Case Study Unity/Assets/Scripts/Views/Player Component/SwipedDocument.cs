﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchScript.Gestures;
using NCCExternalComponentScripts.Wrappers;

public class SwipedDocument : BaseComponent
{

    public DocumentContainer myContainer;
    public Material ddsMaterial;
    private Material originalMaterial;
    [HideInInspector]
    public bool baggieDoc = false;
    //[HideInInspector]
    //public Image myImage;
    public RawImage myImage;
    public AspectRatioFitter myAspect;
    [HideInInspector]
    public Transform swipedLocation;
    [HideInInspector]
    public Transform baggieLocation;
    [HideInInspector]
    public Transform originalLocation;
    [HideInInspector]
    public Vector3 originalLocalPosition;
    [HideInInspector]
    public float myCanter;
    //[HideInInspector]
    private string myTranscribedText = null;
    private float swipedMoveSpeed = .25f;
    private Vector3 viewedRotation = new Vector3(0, 0, 75.0f);
    private static bool lastSwipeComplete = true;

    public void OnSwiped(FlickGesture flicker)
    {
        if (flicker.State == Gesture.GestureState.Recognized)
        {
            if (!lastSwipeComplete) return;
            if (gameObject.transform.parent == swipedLocation)
            {
                if (baggieDoc)
                {
                    if (flicker.ScreenFlickVector.x > 0.0f)  //flicked Right
                    {
                        SwipedRight();
                        return;
                    }
                    SwipedLeft();
                    return;
                }
                if (flicker.ScreenFlickVector.x < 0.0f)  //flicked Left
                {
                    SwipedLeft();
                }
            }
            if (gameObject.transform.parent == originalLocation)
            {
                if (flicker.ScreenFlickVector.x > 0.0f)  //flicked Right
                {
                    SwipedRight();
                }
            }
        }
    }

    public void SetTranscribedText(string textToSet)
    {
        myTranscribedText = textToSet;
    }

    public string GetTranscribedText()
    {
        return myTranscribedText;
    }

    public bool HasTranscribedText()
    {
        if ((myTranscribedText != null) && (myTranscribedText != ""))
        {
            return true;
        }
        return false;
    }

    public void SwipedRight()
    {
        lastSwipeComplete = false;
        myContainer.myPlayerComponent.myAudioPlayer.PlayOneShot(myContainer.myPlayerComponent.swipeFileClip);
        if (!baggieDoc)
        {
            int nextIndex = swipedLocation.GetSiblingIndex();
            if (nextIndex > originalLocation.GetSiblingIndex())
            {
                originalLocation.SetSiblingIndex(nextIndex);
            }
        }
        else
        {
            if (baggieDoc)
            {
                if (gameObject.transform.parent == swipedLocation)
                {
                    LeanTween.cancel(gameObject);
                    //LeanTween.scale(gameObject, baggieLocation.localScale, swipedMoveSpeed);
                    LeanTween.rotateLocal(gameObject, viewedRotation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                    LeanTween.move(this.gameObject, baggieLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                    {
                        gameObject.transform.SetParent(baggieLocation, false);
                        gameObject.transform.localPosition = Vector3.zero;
                        gameObject.transform.localEulerAngles = new Vector3(0, 0, myCanter);
                        if (originalLocation.childCount > 0)
                        {
                            GameObject obj = originalLocation.GetChild(originalLocation.childCount - 1).gameObject;
                            LeanTween.cancel(obj);
                            LeanTween.scale(obj, swipedLocation.localScale, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                            LeanTween.move(obj, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                            {
                                obj.transform.SetParent(swipedLocation, false);
                                obj.transform.localPosition = new Vector3(0, 0, 0);
                                lastSwipeComplete = true;
                                return;
                            });
                        }
                        else
                        {
                            lastSwipeComplete = true;
                            return;
                        }
                    });
                }
                else
                {
                    if (swipedLocation.childCount > 0)
                    {
                        GameObject obj = swipedLocation.GetChild(0).gameObject;
                        LeanTween.cancel(obj);
                        //LeanTween.scale(obj, baggieLocation.localScale, swipedMoveSpeed);
                        LeanTween.rotateLocal(obj, viewedRotation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                        LeanTween.move(obj, baggieLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                        {
                            obj.transform.SetParent(baggieLocation, false);
                            obj.transform.localPosition = Vector3.zero;
                            obj.transform.localEulerAngles = new Vector3(0, 0, myCanter);
                            LeanTween.cancel(gameObject);
                            LeanTween.scale(gameObject, swipedLocation.localScale, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                            LeanTween.move(this.gameObject, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                            {
                                gameObject.transform.SetParent(swipedLocation, false);
                                transform.localPosition = new Vector3(0, 0, 0);
                                lastSwipeComplete = true;
                                return;
                            });
                        });
                    }
                    else
                    {
                        LeanTween.cancel(gameObject);
                        LeanTween.scale(gameObject, swipedLocation.localScale, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                        LeanTween.move(this.gameObject, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                        {
                            gameObject.transform.SetParent(swipedLocation, false);
                            transform.localPosition = new Vector3(0, 0, 0);
                            lastSwipeComplete = true;
                            return;
                        });
                    }
                }
                return;
            }
            //swipedLocation.GetChild(0).GetComponent<SwipedDocument>().SwipedLeft(false);
        }
        //not a baggie doc
        LeanTween.cancel(gameObject);
        LeanTween.move(this.gameObject, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
        {
            gameObject.transform.SetParent(swipedLocation, false);
            transform.localPosition = new Vector3(0, 0, 0);
            lastSwipeComplete = true;
            return;
        });
    }

    public void SwipedLeft()
    {
        lastSwipeComplete = false;
        myContainer.myPlayerComponent.myAudioPlayer.PlayOneShot(myContainer.myPlayerComponent.swipeFileClip);
        LeanTween.cancel(gameObject);
        if (baggieDoc)
        {
            if(gameObject.transform.parent == baggieLocation)
            {
                if (swipedLocation.childCount > 0)
                {
                    GameObject obj = swipedLocation.GetChild(swipedLocation.childCount - 1).gameObject;
                    LeanTween.cancel(obj);
                    LeanTween.scale(obj, new Vector3(.75f, .75f, 1.0f), swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                    LeanTween.move(obj, originalLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                    {
                        obj.transform.SetParent(originalLocation, false);
                        obj.transform.localPosition = originalLocalPosition;
                        obj.transform.localEulerAngles = new Vector3(0, 0, myCanter);
                        LeanTween.rotateLocal(gameObject, new Vector3(0, 0, 0), swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                        LeanTween.move(this.gameObject, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                        {
                            gameObject.transform.SetParent(swipedLocation, false);
                            gameObject.transform.localPosition = new Vector3(0, 0, 0);
                            lastSwipeComplete = true;
                            return;
                        });
                    });
                }
                else
                {
                    LeanTween.rotateLocal(gameObject, new Vector3(0, 0, 0), swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                    LeanTween.move(this.gameObject, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                    {
                        gameObject.transform.SetParent(swipedLocation, false);
                        gameObject.transform.localPosition = new Vector3(0, 0, 0);
                        lastSwipeComplete = true;
                        return;
                    });
                }
                return;
            }
            LeanTween.scale(gameObject, new Vector3(.75f, .75f, 1.0f), swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
        }
        LeanTween.move(this.gameObject, originalLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
        {
            gameObject.transform.SetParent(originalLocation, false);
            gameObject.transform.localPosition = originalLocalPosition;
            gameObject.transform.localEulerAngles = new Vector3(0, 0, myCanter);
            if (baggieDoc)
            {
                if (baggieLocation.childCount > 0)
                {
                    GameObject obj = baggieLocation.GetChild(baggieLocation.childCount - 1).gameObject;
                    LeanTween.cancel(obj);
                    //LeanTween.scale(obj, swipedLocation.localScale, swipedMoveSpeed);
                    LeanTween.rotateLocal(obj, new Vector3(0,0,0), swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad);
                    LeanTween.move(obj, swipedLocation, swipedMoveSpeed).setEase(LeanTweenType.easeInOutQuad).setOnComplete(() =>
                    {
                        obj.transform.SetParent(swipedLocation, false);
                        obj.transform.localPosition = new Vector3(0, 0, 0);
                        lastSwipeComplete = true;
                        return;
                    });
                }
                else
                {
                    lastSwipeComplete = true;
                    return;
                }
            }
            else
            {
                lastSwipeComplete = true;
                return;
            }
        });
    }

    public void MoveToFront()
    {
        if(baggieDoc)
        {
            if(swipedLocation.childCount>0)
            {
                Transform doc = swipedLocation.GetChild(0);
                doc.SetParent(originalLocation, false);
                doc.localPosition = new Vector3(0, 0, 0);
                gameObject.transform.SetParent(swipedLocation, false);
                gameObject.transform.SetAsLastSibling();
                gameObject.transform.localPosition = new Vector3(0, 0, 0);
                return;
            }
            else
            {
                gameObject.transform.SetParent(swipedLocation, false);
                gameObject.transform.SetAsLastSibling();
                gameObject.transform.localPosition = new Vector3(0, 0, 0);
            }
        }
        else
        {
            gameObject.transform.SetParent(originalLocation, false);
            gameObject.transform.SetAsLastSibling();
            gameObject.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void Resupply()
    {
        if(baggieDoc)
        {
            gameObject.transform.localScale = new Vector3(.75f, .75f, 1.0f);
        }
        gameObject.transform.SetParent(originalLocation, false);
        gameObject.transform.localPosition = originalLocalPosition;
        gameObject.transform.localEulerAngles = new Vector3(0, 0, myCanter);
        lastSwipeComplete = true;
    }

    public void OnTapped(TapGesture tapped)
    {
        if (tapped.State == Gesture.GestureState.Recognized)
        {
            DocumentViewer docView = myContainer.myViewer;
            //docView.SetMyImage(this.myImage.sprite, false);
            docView.SetMyImage((Texture2D)this.myImage.texture, false);

            docView.ResetDocumentScaleAndPosition();
            docView.transform.SetAsLastSibling();
            docView.mainMenuButton.gameObject.SetActive(false);
            docView.Show();
            lastSwipeComplete = true;
            if (originalLocation != null)
            {
                docView.currentDoc = this;
            }
        }
    }

    public override void Show(float time = 0.25F)
    {
        myAspect.aspectRatio = (float)myImage.texture.width / (float)myImage.texture.height;
        if (myAspect.aspectRatio > 1)
        {
            myAspect.aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
        }
        else
        {
            myAspect.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        }
        base.Show(time);
    }

    public override void Hide(float time)
    {
        base.Hide(0.0f);
    }
}
