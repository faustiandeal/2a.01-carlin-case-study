﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DayPlannerComponent : BaseComponent
{
    public TextMeshProUGUI instructionalText;
    public TornPaperComponent myTornPaper;
    public DocumentViewer myViewer;
    //public Scrollbar myScrollbar;
    public Transform myCalendarParent;
    public TextMeshProUGUI calendarDateText;
    public DayPlannerArtifactComponent calendarPrefab;
    public TextMeshProUGUI DescriptiveText;
    public List<ContentPositionComponent> contentPositions;
    public DocumentContainer myContainer;
    public GameObject SwipeIcon;
    private List<DayPlannerArtifactComponent> myArtifactList;
    [HideInInspector]
    public int activeCalendarValue = 0;

    public void SetUpMyData(DayPlannersDataModel data)
    {
        calendarDateText.CrossFadeAlpha(0.0f, 0.0f, false);
        DescriptiveText.text = data.description;
        myContainer.myViewer.CreateMyListOfDocuments();
        myArtifactList = new List<DayPlannerArtifactComponent>();
        for (int i = 0; i < data.artifacts.Count; i++)
        {
            DayPlannerArtifactComponent artifact = Instantiate<DayPlannerArtifactComponent>(calendarPrefab, myCalendarParent, false);
            artifact.transform.localPosition = new Vector3(0, 0, 0);
            artifact.Hide();
            artifact.myContainer = myContainer;
            artifact.dayPlannerComponent = this;
            myArtifactList.Add(artifact);
        }
        for (int i = 0; i < myArtifactList.Count; i++)
        {
            myArtifactList[i].SetUpMyData(data.artifacts[i], contentPositions);
        }
        //myScrollbar.numberOfSteps = myArtifactList.Count;
        //Set the First Artifact active
        //SwitchCalendar(0);
    }

    public void SwitchCalendar(int newCalendarValue)
    {
        if (newCalendarValue >= 0 && newCalendarValue < myArtifactList.Count)
        {
            int previousCalendarValue = activeCalendarValue;
            activeCalendarValue = newCalendarValue;
            if (previousCalendarValue != newCalendarValue)
            {
                myArtifactList[previousCalendarValue].HideMyContent(0.0f);
                myArtifactList[previousCalendarValue].Hide(0.0f);
            }
            myArtifactList[newCalendarValue].ShowMyContent();
            myArtifactList[newCalendarValue].Show();
            myArtifactList[newCalendarValue].activeCalendar = true;
            myContainer.myPlayerComponent.myAudioPlayer.PlayOneShot(myContainer.myPlayerComponent.swipeCalendarClip);
            calendarDateText.CrossFadeAlpha(0.0f, 0.0f, false);
            calendarDateText.CrossFadeAlpha(1.0f, 1.0f, false);
            calendarDateText.text = myArtifactList[newCalendarValue].calendarDate;
        }
    }

    public bool HasAnotherCalendar(int newCalendarValue)
    {
        if(newCalendarValue >= 0 && newCalendarValue < myArtifactList.Count)
        {
            return true;
        }
        return false;
    }

    //public void ScrollChangeCalendar()
    //{
    //    float value = (myScrollbar.numberOfSteps-1) * myScrollbar.value;
    //    SwitchCalendar(Mathf.RoundToInt(value));
    //}
    public override void Show(float time = 0.25F)
    {
        SwitchCalendar(0);
        base.Show(time);
    }

    public override void Hide(float time = 0.25f)
    {
        //SwitchCalendar(0);
        myViewer.myDocuments.Clear();
        myViewer.myDocumentDetails.Clear();
        for (int i = 0; i < myArtifactList.Count; i++)
        {
            for (int j = 0; j < myArtifactList[i].myContentList.Count; j++)
            {
                myArtifactList[i].myContentList[j].mediaImage.texture = null;
            }
        }
        SpriteLoader.textureDictionary.Clear();
        Resources.UnloadUnusedAssets();
        SwipeIcon.SetActive(true);
        myViewer.Hide();
        base.Hide();
    }
}
