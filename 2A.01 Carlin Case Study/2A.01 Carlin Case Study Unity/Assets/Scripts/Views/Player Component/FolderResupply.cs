﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FolderResupply : BaseComponent {

    public List<SwipedDocument> Documents;

    public void ResupplyMe()
    {
        foreach(SwipedDocument doc in Documents)
        {
            doc.Show();
        }
    }
}
