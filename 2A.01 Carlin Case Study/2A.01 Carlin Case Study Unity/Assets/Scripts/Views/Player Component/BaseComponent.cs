﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseComponent : MonoBehaviour {

    public CanvasGroup Canvas;
    private float AnimationTime = 0.25f;

    public virtual void Show(float time = 0.25f)
    {
        //this.gameObject.SetActive(false);
        LeanTween.cancel(Canvas.gameObject);
        LeanTween.alphaCanvas(Canvas, 1, time).setEase(LeanTweenType.easeOutQuad).setOnComplete(() => 
        {
            Canvas.interactable = Canvas.blocksRaycasts = true;
        });
    }

    public virtual void Hide(float time = 0.25f)
    {
        CancelInvoke();
        Canvas.interactable = Canvas.blocksRaycasts = false;
        LeanTween.cancel(Canvas.gameObject);
        LeanTween.alphaCanvas(Canvas, 0, time).setEase(LeanTweenType.easeInQuad);
    }
}
