﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IntroComponent : BaseComponent {

    public TextMeshProUGUI IntroText;
    public TextMeshProUGUI SwipeText;
    public Image TrunkImage;
    public GameObject SwipeUnlock;
    public PlayerComponent myPlayerComponent;

    //public void ShowLockSwipeText()
    //{
    //    LeanTween.cancel(IntroSwipeTextCanvas.gameObject);
    //    LeanTween.alphaCanvas(IntroSwipeTextCanvas, 1, 0.5f).setEase(LeanTweenType.easeOutQuad);
    //}

    //public void HideLockSwipeText()
    //{
    //    LeanTween.cancel(SwipeIcon.gameObject);
    //    LeanTween.alphaCanvas(SwipeIcon, 0, 0.5f).setEase(LeanTweenType.easeInQuad).setOnComplete(() => { SwipeIcon.interactable = SwipeIcon.blocksRaycasts = false; });
    //}
}
