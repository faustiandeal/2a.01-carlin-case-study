﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContentPositionComponent : BaseComponent {

    public DayPlannerComponent DayPlannerComponent;
    [HideInInspector]
    public DayPlannerContent myContent;
    public TextMeshProUGUI myContentDate;
    public TextMeshProUGUI myContentSubText;
}
