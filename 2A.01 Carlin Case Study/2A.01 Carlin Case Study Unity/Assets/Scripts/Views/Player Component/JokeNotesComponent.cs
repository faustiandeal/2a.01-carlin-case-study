﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NCCExternalComponentScripts.Proxies;

public class JokeNotesComponent : BaseComponent {

    public Image DescriptionPopup;
    public BaggieScrollerController ScrollerController;
    public DocumentViewer myViewer;
    public TornPaperComponent tornPaper;
    public DocumentContainer myDocContainer;
    public PlayerComponent myPlayerComponent;
    public TextMeshProUGUI instructionalText;
    public GameObject GridScroller;
    public MainMenuComponent MainMenu;
    [HideInInspector]
    public bool folderIsOpen = false;

    public void SetUpMyData(JokeNotesDataModel data)
    {
        string imageFilePath = CMSDataProxy.LocalMediaFilePath + data.descriptionImagePopUp;
        //DescriptionPopup.sprite = SpriteLoader.LoadSprite(imageFilePath, false);
        tornPaper.myText.text = data.description;
        ScrollerController.PassDataToMe(data);
    }

    public void CloseButtonPressed()
    {
        if (folderIsOpen)
        {
            CloseFolders();
            return;
        }
        this.Hide();
    }

    private void CloseFolders()
    {
        if (folderIsOpen)
        {
            int index = -1;
            for (int i = 0; i < ScrollerController.baggieFolders.Count; i++)
            {
                if (ScrollerController.baggieFolders[i].iAmOpen)
                {
                    ScrollerController.baggieFolders[i].Hide();
                    index = i;
                    break;
                }
            }
            folderIsOpen = false;
        }
    }

    public override void Hide(float time = 0.25f)
    {
        myViewer.Hide();
        CloseFolders();
        GridScroller.SetActive(true);
        base.Hide();
        tornPaper.Show();
        MainMenu.Show();
    }
}
