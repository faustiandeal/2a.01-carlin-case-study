﻿using UnityEngine;
using System.Collections;
using cortina.architecture;
using System;
using cortina.language;
using VisitorTracking.Model;

public class PlayerMediator : Mediator<PlayerComponent>
{
    public override void Initialize()
    {
        RegisterListener(CarlinCaseNotifications.CMSDataLoaded, (o) =>
        {
            Debug.Log("[PlayerMediator] CMSDataLoaded");
            CarlinCaseDataModel data = (CarlinCaseDataModel)o;
            component.SetUpData(data);
            //component.SetTextData(data.ConfigurableText);
            //component.SetJokeData(data.Jokes.ToArray());
            //component.ResetAll();
        });

        RegisterListener(CarlinCaseNotifications.CMSTextDataLoaded, (o) =>
        {
            Debug.Log("[PlayerMediator] CMSTextDataLoaded");
            CarlinCaseTextDataModel data = (CarlinCaseTextDataModel)o;
            component.SetTextData(data);
        });

        RegisterListener(CarlinCaseNotifications.Restarted, (o) =>
        {
            Debug.Log("[PlayerMediator] Restarted");
            component.ResetAll();
        });

        RegisterListener(CarlinCaseNotifications.StateEnded, (o) =>
        {
            NotificationPayloadObject data = (NotificationPayloadObject)o;
            CarlinCaseFacade.States newState = (CarlinCaseFacade.States)data.Payload;
            CarlinCaseGameProxy lbProxy = context.FindProxy<CarlinCaseGameProxy>(CarlinCaseGameProxy.Name);
            CMSDataProxy cmsProxy = context.FindProxy<CMSDataProxy>(CMSDataProxy.Name);
            Debug.Log("[PlayerMediator] StateEnded "+newState.ToString());
            if (data.Player == component.PlayerNumber)
            {
                switch (newState)
                {
                    //Attract, Intro, TrunkOpening, MainMenu, CareerHistoryMenu, CareerHistoryFolder, DocumentSelected, JokeNotesIntro, JokeNotesFolder, JokeNoteBagSelected, JokeNoteSelected, DayPlanner
                    case CarlinCaseFacade.States.Attract:
                        component.HideAttract();
                        break;
                    case CarlinCaseFacade.States.Intro:
                        component.HideIntro();
                        break;
                    case CarlinCaseFacade.States.TrunkOpening:
                        component.HideTrunkOpening();
                        break;
                    case CarlinCaseFacade.States.MainMenu:
                        
                        break;
                }
            }
        });

        RegisterListener(CarlinCaseNotifications.StateChanged, (o) =>
        {
            NotificationPayloadObject data = (NotificationPayloadObject)o;
            CarlinCaseFacade.States newState = (CarlinCaseFacade.States)data.Payload;
            CarlinCaseGameProxy lbProxy = context.FindProxy<CarlinCaseGameProxy>(CarlinCaseGameProxy.Name);
            CMSDataProxy cmsProxy = context.FindProxy<CMSDataProxy>(CMSDataProxy.Name);
            Debug.Log("[PlayerMediator] StateChanged "+newState.ToString());
            if (data.Player == component.PlayerNumber)
            {
                switch (newState)
                {
                    //Attract, Intro, TrunkOpening, MainMenu, CareerHistoryMenu, CareerHistoryFolder, DocumentSelected, JokeNotesIntro, JokeNotesFolder, JokeNoteBagSelected, JokeNoteSelected, DayPlanner
                    case CarlinCaseFacade.States.Attract:
                        component.ShowAttract();
                        break;

                    case CarlinCaseFacade.States.Intro:
                        component.ShowIntro();
                        break;

                    case CarlinCaseFacade.States.TrunkOpening:
                        component.ShowTrunkOpening();
                        component.TrunkOpening.AnimateTrunk();
                        LeanTween.delayedCall(component.TrunkOpening.animationTime, () =>
                        {
                            SendStateEndedMessage(CarlinCaseFacade.States.TrunkOpening);
                        });
                        break;

                    case CarlinCaseFacade.States.MainMenu:
                        if(component.resetCalled)
                        {
                            component.ResetAll();
                            return;
                        }
                        component.ShowMainMenu();
                        LeanTween.delayedCall(1.0f, () =>
                        {
                            component.TrunkOpening.ResetPositions();
                        });
                        break;

                    case CarlinCaseFacade.States.CareerHistoryMenu:
                        break;

                    case CarlinCaseFacade.States.CareerHistoryFolder:
                        break;

                    case CarlinCaseFacade.States.DocumentSelected:
                        break;

                    case CarlinCaseFacade.States.JokeNotesIntro:
                        break;

                    case CarlinCaseFacade.States.JokeNotesFolder:
                        break;

                    case CarlinCaseFacade.States.JokeNoteBagSelected:
                        break;

                    case CarlinCaseFacade.States.JokeNoteSelected:
                        break;

                    case CarlinCaseFacade.States.DayPlanner:
                        break;


                    //case CarlinCaseFacade.States.Gameface:
                    //    component.SmileCalibrate.Show();
                    //    component.SmileCalibrate.StartGameFaceTimer();
                    //    break;
                    //case CarlinCaseFacade.States.SmileCalibration:
                    //    //component.SmileCalibration.Show();
                    //    break;
                    //case CarlinCaseFacade.States.SetupComplete:
                        
                    //    break;
                    //case CarlinCaseFacade.States.GameIntro:
                        
                    //    component.MainGame.ShowSelfOverlay();
                    //    component.MainGame.HideWaitingForOpponent();

                    //    LeanTween.delayedCall(0.25f, () =>
                    //    {
                    //        SendStateEndedMessage(CarlinCaseFacade.States.GameIntro);
                    //    });
                    //    break;
                    //case CarlinCaseFacade.States.ChooseJoke:
                    //    component.MainGame.CountdownLineblocker.gameObject.SetActive(false);
                    //    component.MainGame.HideSelfOverlay();
                    //    component.PlayerScore.Show();
                    //    component.OpponentScore.Show();
                        
                    //    component.MainGame.ShowRoundText();
                    //    component.MainGame.UpdateRoundText(lbProxy.DisplayRound.ToString(), lbProxy.TotalDIsplayRounds.ToString());
                    //    component.Feedback.Hide();
                    //    component.ShowJokeSelection(lbProxy.DisplayRound);
                    //    break;
                    //case CarlinCaseFacade.States.TellingJoke:
                    //    lbProxy.PlayerLaughedThisRound = false;
                    //    LeanTween.delayedCall(0.25f, () =>
                    //    {
                    //        component.HideJokeSelection();
                    //        component.MainGame.ShowSelfOverlay();
                    //        component.MyTurn = true;
                    //        component.AttackPrep.Show();
                    //        component.AttackPrep.StartDelayHide(CarlinCaseGameProxy.STANDARD_DELAY);
                    //        LeanTween.delayedCall(CarlinCaseGameProxy.STANDARD_DELAY, () =>
                    //        {
                    //            component.MainGame.CountdownLineblocker.gameObject.SetActive(true);
                    //        });
                    //    });
                        
                    //    break;
                    //case CarlinCaseFacade.States.JokeResult:
                    //    LeanTween.delayedCall(0.7f, () =>
                    //    {
                    //        component.MainGame.CountdownLineblocker.gameObject.SetActive(false);
                    //    });
                        
                    //    component.DefendPrep.Hide();

                    //    if(component.PlayerNumber == lbProxy.PlayerTellingJoke())
                    //    {
                    //        component.Feedback.FeedBackText.text = (lbProxy.PlayerLaughedThisRound) ? cmsProxy.CMSData.ConfigurableText.AttackingJokeSuccess : cmsProxy.CMSData.ConfigurableText.DefendingJokeSuccess;
                    //    }
                    //    else
                    //    {
                    //        component.Feedback.FeedBackText.text = (lbProxy.PlayerLaughedThisRound) ? cmsProxy.CMSData.ConfigurableText.AttackingJokeSuccess : cmsProxy.CMSData.ConfigurableText.DefendingJokeSuccess;
                    //    }
                        
                    //    component.Feedback.Show();
                    //    switch (component.PlayerNumber)
                    //    {
                    //        case Player.One:
                    //            component.PlayerScore.UpdateScore(lbProxy.PlayerOneModel.CurrentScore);
                    //            component.OpponentScore.UpdateScore(lbProxy.PlayerTwoModel.CurrentScore);
                    //            break;

                    //        case Player.Two:
                    //            component.PlayerScore.UpdateScore(lbProxy.PlayerTwoModel.CurrentScore);
                    //            component.OpponentScore.UpdateScore(lbProxy.PlayerOneModel.CurrentScore);
                    //            break;
                    //    }
                    //    LeanTween.delayedCall(CarlinCaseGameProxy.STANDARD_DELAY, () =>
                    //    {
                    //        component.Feedback.Hide();
                    //        SendStateEndedMessage(CarlinCaseFacade.States.JokeResult);
                    //    });
                    //    break;
                    //case CarlinCaseFacade.States.VictoryPicture:
                    //    if(lbProxy.IsGameATie())
                    //    {
                    //        LeanTween.delayedCall(0.25f, () =>
                    //        {
                    //            component.VictoryPicture.Hide();
                    //            SendStateEndedMessage(CarlinCaseFacade.States.VictoryPicture);
                    //        });
                    //    }
                    //    else
                    //    {
                    //        if (lbProxy.IsPlayerWinner(data.Player))
                    //        {
                    //            component.VictoryPicture.Show();
                    //            component.VictoryPicture.StartVictoryPictureTimer();
                    //        }
                    //        else
                    //        {

                    //        }
                    //    }            
                    //    break;

                    //case CarlinCaseFacade.States.Conclusion:
                    //    ConclusionDataObject conclusionData = new ConclusionDataObject();
                    //    conclusionData = lbProxy.GetConclusionDataObjectForPlayer(component.PlayerNumber);
                    //    conclusionData.Feedback = (conclusionData.IsWinner) ? cmsProxy.CMSData.ConfigurableText.TakeawayVictory : cmsProxy.CMSData.ConfigurableText.TakeawayDefeat;
    
                    //    component.Conclusion.SetData(cmsProxy.CMSData.ConfigurableText.Conclusion, conclusionData);
                    //    component.MainGame.CountdownLineblocker.gameObject.SetActive(false);
                    //    component.MainGame.HideRoundText();
                    //    component.PlayerScore.Hide();
                    //    component.OpponentScore.Hide();
                    //    component.MainGame.HideDividingLine();
                    //    component.MainGame.HideSelfOverlay();
                    //    component.Conclusion.Show();
                    //    component.OpponentWebCam.Hide();
                    //    component.SelfWebCam.Hide();
                    //    LeanTween.delayedCall(CarlinCaseGameProxy.STANDARD_DELAY * 2, () =>
                    //    {
                    //        component.Conclusion.Hide();
                    //        SendStateEndedMessage(CarlinCaseFacade.States.Conclusion);
                    //    });
                    //    break;
                }
            }
        });

        //RegisterListener(CarlinCaseNotifications.JokeComplete, (o) =>
        //{
        //    Debug.Log("[PlayerMediator] JokeComplete");
        //    bool playerlaughed = (bool)o;
        //});

        RegisterListener(CarlinCaseNotifications.NewVisitorTappedIn, (o) =>
        {
            SendNotification(CarlinCaseNotifications.StateEnded, new NotificationPayloadObject() { Player = component.PlayerNumber, Payload = null });
            Debug.Log("[PlayerMediator] NewVisitorTappedIn");
            component.PlayerTappedIn((string)o);
        });

        component.LockOpened += () =>
        {
            //component.MainGame.HideLockSwipeText();
            SendStateEndedMessage(CarlinCaseFacade.States.Intro);
        };

        //component.GameFacePictureTaken += (gamefaceTexture) => 
        //{
        //    CarlinCaseGameProxy lbProxy = context.FindProxy<CarlinCaseGameProxy>(CarlinCaseGameProxy.Name);
        //    switch (component.PlayerNumber)
        //    {
        //        case Player.One:
        //            lbProxy.PlayerOneModel.GameFaceTexture = gamefaceTexture;
        //            break;

        //        case Player.Two:
        //            lbProxy.PlayerTwoModel.GameFaceTexture = gamefaceTexture;
        //            break;
        //    }
        //    component.SmileCalibrate.Hide();
        //    SendStateEndedMessage(CarlinCaseFacade.States.Gameface);
        //};

        //component.VictoryFacePictureTaken += (victoryfaceTexture) =>
        //{
        //    CarlinCaseGameProxy lbProxy = context.FindProxy<CarlinCaseGameProxy>(CarlinCaseGameProxy.Name);
        //    switch (component.PlayerNumber)
        //    {
        //        case Player.One:
        //            lbProxy.PlayerOneModel.VictoryFaceTexture = victoryfaceTexture;
        //            break;

        //        case Player.Two:
        //            lbProxy.PlayerTwoModel.VictoryFaceTexture = victoryfaceTexture;
        //            break;
        //    }
        //    component.VictoryPicture.Hide();
        //    SendStateEndedMessage(CarlinCaseFacade.States.VictoryPicture);
        //};

        //component.SmileCalibration += () => 
        //{
        //    SendStateEndedMessage(CarlinCaseFacade.States.SmileCalibration);
        //};

        component.MainGame.ReadyButtonPressed += () =>
        {
            //component.MainGame.HideSelfOverlay();
            //component.MainGame.HideReadyComponent();
            SendStateEndedMessage(CarlinCaseFacade.States.Intro);
        };

        //component.AttackPrepEnded += () => 
        //{
        //    component.TellJoke((int)CarlinCaseGameProxy.STANDARD_TIMER);
        //};

        //component.JokeEnded += (bool opponentLaughed) => 
        //{
        //    if (opponentLaughed)
        //    {
        //        SendNotification(CarlinCaseNotifications.JokeComplete, opponentLaughed);
        //    }
        //    SendStateEndedMessage(CarlinCaseFacade.States.TellingJoke);
        //};

        component.Attract.Ended += () =>
        {
            component.resetCalled = false;
            SendStateEndedMessage(CarlinCaseFacade.States.Attract);
        };
    }

    private void Component_LockOpened()
    {
        throw new NotImplementedException();
    }

    private void SendStateEndedMessage(CarlinCaseFacade.States state)
    {
        SendNotification(CarlinCaseNotifications.StateEnded, new NotificationPayloadObject() { Player = component.PlayerNumber, Payload = state });
    }
}