﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cortina.architecture;
using NCCExternalComponentScripts.Proxies;
using System;
using LitJson;
using System.IO;

public class CarlinCaseFacade : MonoBehaviour
{
    public enum States
    {
        Attract, Intro, TrunkOpening, MainMenu, CareerHistoryMenu, CareerHistoryFolder, DocumentSelected, JokeNotesIntro, JokeNotesFolder, JokeNoteBagSelected, JokeNoteSelected, DayPlanner
    }

    [SerializeField]
    PlayerComponent player1Component;

    [SerializeField]
    States startingState = States.Attract;
    Facade facade = new Facade();

    CarlinCaseGameProxy gameProxy;
    CMSDataProxy cmsDataProxy;

    private void Awake()
    {
        Application.runInBackground = true;
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        //Screen.SetResolution(5760, 1080, false);
        QualitySettings.SetQualityLevel(5);
        //configComponent.LoadConfig();
    }

    public void Start()
    {
        //LeanTween.init(500000);
        Cursor.visible = false;
        //player1Component.SetBackgroundImage(SpriteLoader.LoadSprite(Path.Combine("Config/", configComponent.Data.Player1Background)));

        // Mediators
        facade.RegisterMediator(new PlayerMediator() { Component = player1Component });

        // Register Models as Proxies
        cmsDataProxy = new CMSDataProxy();
        facade.RegisterProxy(CMSDataProxy.Name, cmsDataProxy);
        gameProxy = new CarlinCaseGameProxy();
        facade.RegisterProxy(CarlinCaseGameProxy.Name, gameProxy);

        // Commands
        facade.RegisterCommand(CarlinCaseNotifications.StateEnded, new StateEndedCommand());
        //facade.RegisterCommand(MainContentNotifications.GuessMade, new UpdateGameStatusCommand());

        facade.Initialize();

        //facade.RegisterListener(CarlinCaseNotifications.JokeComplete, (o) =>
        //{
        //    Debug.LogWarning("[CarlinCaseFacade] JokeComplete");
        //    bool opponentLaughed = (bool)o;
        //    gameProxy.PlayerLaughedThisRound = opponentLaughed;
        //    if (opponentLaughed)
        //    {
        //        gameProxy.AddToPlayerCurrentScore(gameProxy.PlayerTellingJoke());
        //    }
        //});

        facade.RegisterListener(AttractComponent.Notifications.Reset, (o) =>
        {
            Debug.Log("REST PRESSED");
        });

        facade.RegisterListener(CarlinCaseNotifications.StateChanged, (o) =>
        {
            Debug.LogWarning("[CarlinCaseFacade] State Changed");
            NotificationPayloadObject data = (NotificationPayloadObject)o;

            if((CarlinCaseFacade.States)data.Payload == CarlinCaseFacade.States.Attract)
            {
                gameProxy.ResetVisitorData();
            }

        });

        facade.RegisterListener(CarlinCaseNotifications.Restarted, (o) =>
        {
            Debug.LogWarning("[CarlinCaseFacade] Restarted");
            Resources.UnloadUnusedAssets();
        });

        facade.SendNotification(CarlinCaseNotifications.CMSDataLoaded, cmsDataProxy.CMSData);
        facade.SendNotification(CarlinCaseNotifications.CMSTextDataLoaded, cmsDataProxy.CMSData2);
        
#if !UNITY_EDITOR
        //SpriteLoader.PreloadAllImagesInFolder(CMSDataProxy.LocalMediaFilePath);
#endif

        //facade.RegisterListener(MainContentNotifications.NewGameRequested, (o) =>
        //{
        //    Debug.Log("[MainContentFacade] NewGameRequested");
        //    OnResetAll();
        //    SetMogulData(proxy.Data.Moguls[Random.Range(0, 3)]); // TODO post-alpha swap for proxy.Data.Moguls.Length-1)]);
        //    facade.SendNotification(MainContentNotifications.StateEnded, MainContentFacade.States.Attract);
        //});

        facade.RegisterListener(CarlinCaseNotifications.Restarted, (o) =>
        {
            Debug.Log("[MainContentFacade] Restarted");
            OnResetAll();
            Resources.UnloadUnusedAssets();
        });

        //facade.RegisterListener(MainContentNotifications.StateChanged, (o) =>
        //{
        //    Debug.LogWarning("[MainContentFacade] StateChanged");
        //    States newState = (States)o;
        //    if (newState == States.Attract)
        //    {
        //    }
        //});
    }

    void OnResetAll()
    {
        facade.SendNotification(CarlinCaseNotifications.StateChanged, startingState);
    }

    void SetData(CarlinCaseDataModel data)
    {
        facade.SendNotification(CarlinCaseNotifications.CMSDataLoaded, data);
    }

    public PlayerComponent GetPlayerComponent()
    {
        return player1Component;
    }
}

public class CarlinCaseNotifications
{
    public const string Restarted = "Restarted";
    public const string CMSDataLoaded = "CMSDataLoaded";
    public const string CMSTextDataLoaded = "CMSTextDataLoaded";
    public const string StateEnded = "StateEnded";
    public const string StateChanged = "StateChanged";
    public const string NewVisitorTappedIn = "NewVisitorTappedIn";
    public const string ReceivedVisitorRelatedContent = "ReceivedVisitorRelatedContent";
    //public const string JokeComplete = "JokeComplete";
}

public class NotificationPayloadObject
{
    public Player Player; // Applicable Player Number for object
    public object Payload;
}