﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ComedyVisitorProfileDataModels;
using VisitorTracking.Model;
using NCCExternalComponentScripts.Proxies;
using NCCExternalComponentScripts.Wrappers;
using TouchScript;
using TouchScript.Gestures;

public class PlayerComponent : MonoBehaviour
{
    public event Action LockOpened;
    public event Action ReadyForJokePressed;

    public Player PlayerNumber;

    public Image BackgroundImage;

    public AttractWrapper Attract;
    public IntroComponent IntroMenu;
    public MainGameOverlayComponent MainGame;
    public TrunkOpeningComponent TrunkOpening;
    public MainMenuComponent MainMenu;
    public BestOfComponent BestOfMenu;
    public JokeNotesComponent JokeNotesMenu;
    public CareerHistoryComponent CareerHistoryMenu;
    public DayPlannerComponent DayPlannerMenu;
    public BaseComponent currentMenu;
    public AudioSource myAudioPlayer;
    public AudioClip lockOpenClip;
    public AudioClip trunkOpenClip;
    public AudioClip swipeFileClip;
    public AudioClip openFolderClip;
    public AudioClip closeFolderClip;
    public AudioClip swipeCalendarClip;
    [HideInInspector]
    public string careerHistoryInstructionalText;
    [HideInInspector]
    public bool resetCalled = false;

    public CarlinCaseTextDataModel textData;
    private static System.Random rnd = new System.Random();

    #region DataSetters

    public void SetBackgroundImage(Sprite s)
    {
        BackgroundImage.sprite = s;
    }

    public void SetTextData(CarlinCaseTextDataModel data)
    {
        textData = data;
        IntroMenu.IntroText.text = data.mainIntroText;
        IntroMenu.SwipeText.text = data.mainIntroTextPt2;
        MainMenu.DescriptiveText.text = data.mainMenuText;
        //MainMenu.TapInstructions.text = data.mainMenuInstructionalText;
        CareerHistoryMenu.InstructionalText.text = data.careerHistoryTapText;
        careerHistoryInstructionalText = data.careerHistorySwipeText;
        JokeNotesMenu.instructionalText.text = data.jokeNotesInstructionalText;
        DayPlannerMenu.instructionalText.text = data.dayPlannerInstructionalText;
    }

    public void SetUpData(CarlinCaseDataModel data)
    {
        CareerHistoryMenu.SetUpMyData(data.careerHistory);
        DayPlannerMenu.SetUpMyData(data.dayPlanners);
        JokeNotesMenu.SetUpMyData(data.jokeNotes);
        BestOfMenu.SetUpMyData(data.bestOf);
    }

    #endregion

    #region Sequence

    public void ShowAttract()
    {
        Attract.Show();
    }

    public void HideAttract()
    {
        //Attract.Hide();
    }

    public void ShowIntro()
    {
        Resources.UnloadUnusedAssets();
        IntroMenu.Show();
    }

    public void HideIntro()
    {
        IntroMenu.Hide();
    }

    public void OnLockFlicked(FlickGesture flicker)
    {
        if (flicker.State == Gesture.GestureState.Recognized)
        {
            if (LockOpened != null)
            {
                myAudioPlayer.PlayOneShot(lockOpenClip);
                //Debug.Log("Lock Swiped");
                LockOpened();
                return;
            }
        }
    }

    public void OnTrunkTapped()
    {
        if (LockOpened != null)
        {
            myAudioPlayer.PlayOneShot(lockOpenClip);
            //Debug.Log("Lock Swiped");
            LockOpened();
            return;
        }
    }

    public void ShowTrunkOpening()
    {
        myAudioPlayer.PlayOneShot(trunkOpenClip);
        TrunkOpening.Show();
    }

    public void HideTrunkOpening()
    {
        TrunkOpening.Hide(0.0f);
    }

    public void ShowMainMenu()
    {
        MainMenu.Show(0.0f);
    }

    public void HideMainMenu()
    {
        MainMenu.Hide();
    }

    public void ShowJokeNotesMenu()
    {
        JokeNotesMenu.Show();
    }

    public void HideJokeNotesMenu()
    {
        JokeNotesMenu.Hide();
    }

    public void ShowCareerHistoryMenu()
    {
        CareerHistoryMenu.Show();
    }

    public void HideCareerHistoryMenu()
    {
        CareerHistoryMenu.Hide(0.25f);
    }

    public void PlayerTappedIn(string name)
    {
        //PlayerName.text = name;
    }

    public void BestOfButtonPressed()
    {
        MainMenu.Hide();
        BestOfMenu.Show();
    }

    public void MainMenuButtonPressed()
    {
        //IntroMenu.Show();
        TrunkOpening.Hide();
        MainMenu.Show();
        JokeNotesMenu.Hide();
        CareerHistoryMenu.Hide(0.25f);
        DayPlannerMenu.Hide();
        BestOfMenu.Hide();
        SpriteLoader.textureDictionary.Clear();
        Resources.UnloadUnusedAssets();
        //Attract.Show();
    }

    #endregion

    public void ResetAll()
    {
        Reset();
        //AttackPrep.Hide();
        //DefendPrep.Hide();
        //DisplayJoke.Hide();
    }

    private void Awake()
    {
        Attract.Reset += Reset;
        Input.multiTouchEnabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown("m"))
        {
            Cursor.visible = !Cursor.visible;
        }
    }

    private void Reset()
    {
        resetCalled = true;
        if (TrunkOpening.gameObject.GetComponent<CanvasGroup>().interactable) return;
        Input.multiTouchEnabled = false;
        IntroMenu.Show();
        TrunkOpening.Hide();
        TrunkOpening.ResetPositions();
        JokeNotesMenu.Hide();
        CareerHistoryMenu.Hide(0.25f);
        DayPlannerMenu.Hide();
        BestOfMenu.Hide();
        MainMenu.Hide();
        Attract.Show();
        //unload assets
        SpriteLoader.textureDictionary.Clear();
        Resources.UnloadUnusedAssets();
    }
}

public enum Player
{
    One
}