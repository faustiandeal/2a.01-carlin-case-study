﻿using System;
using System.Collections.Generic;
using UnityEngine;
using cortina;
using cortina.VisitorProfileSystem;
using System.Text;
using VisitorTracking.Model;

public class CYPNCCVPSComponent : NCCKioskClient
{
    public static CYPNCCVPSComponent _Instance;

    public static event Action<string> OnRFIDTappedIn;
    public static event Action<CreateVistiorResponse> OnProfileCreated;

    public static string AvId = "1A02";

    private void Awake()
    {
        if (_Instance == null)
        {
            _Instance = this;
        }
    }

    protected override void CustomMessageReceived(string command, byte[] payload)
    {
        base.CustomMessageReceived(command, payload);
        ThreadDispatcher.Invoke(() =>
        {
            string dataString = (payload == null ? " " : Encoding.UTF8.GetString(payload));
            switch (command)
            {
                case VPSCommands.GetVisitorProfile:
                    VPSPayloadObjectDTO rfidVPSPayload = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    if (OnRFIDTappedIn != null)
                    {
                        OnRFIDTappedIn((string)rfidVPSPayload.Data);
                    }
                    break;
                case VPSCommands.CreateVisitorProfile:
                    VPSPayloadObjectDTO payloadObjectDto = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    CreateVistiorResponse createVistiorResponse = LitJson.JsonMapper.ToObject<CreateVistiorResponse>((string)payloadObjectDto.Data);
                    if (OnProfileCreated != null)
                    {
                        OnProfileCreated(createVistiorResponse);
                    }
                    break;
                default:
                    //Debug.LogWarning("Received unrecognized command: `" + command + "`");
                    //Debug.Log(data);
                    break;
            }
        });
    }

    public void CreateVisitor(VisitorCreatePost newVisitorPost)
    {
        SendMessageToApplicationManager(VPSCommands.CreateVisitorProfile, newVisitorPost);
    }
}
