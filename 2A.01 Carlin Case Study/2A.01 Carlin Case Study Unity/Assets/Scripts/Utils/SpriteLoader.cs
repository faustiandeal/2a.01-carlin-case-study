﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;

/// <summary>
/// Given a file path to a png or jpeg file, will return a Sprite. 
/// Can also be utilized to preload sprites for later use in a program to eliminate lag time.
/// Version: 1.0.2
/// Author: Rich S, Charles D
/// </summary>
/// <param name="filePath">The file path of the image to be loaded.</param>
/// <param name="isDDS">Whether the image is a precompressed DXT image or not.</param>
/// <param name="nonReadable">Non-readable images will be compressed when loaded into DXT format.</param>
/// <param name="storeInDictionary">If enabled, calling LoadSprite again on the same image will pull it from the dicitionary rather than load it again.</param>
/// <param name="pixelsPerUnit">1.0f is 1:1 ratio resolution for Unity images.</param>

public class SpriteLoader
{ 
    static Dictionary<string, Sprite> spriteDictionary = new Dictionary<string, Sprite>();
    public static Dictionary<string, Texture2D> textureDictionary = new Dictionary<string, Texture2D>();
    //static int texturesLoaded = 0;

    public static Sprite LoadSprite(string filePath, bool useDDS = false, bool nonReadable = false, bool storeInDictionary = true, float pixelsPerUnit = 1.0f)
    {
        //Debug.Log("File Loaded: " + filePath);
        if (spriteDictionary.ContainsKey(filePath))
        { 
            return spriteDictionary[filePath];
        }
        else
        {
            Sprite NewSprite = new Sprite();
            Texture2D SpriteTexture = null;
            if (useDDS)
            {
                if(filePath.Contains(".png"))
                {
                    string ddsFilePath = filePath.Replace("media", "temp") + ".dds";
                    //string ddsFilePath = filePath.Replace("media", "temp");
                    //ddsFilePath = ddsFilePath.Replace(".png", ".dds");
                    SpriteTexture = LoadTextureDXT(ddsFilePath, TextureFormat.DXT1);
                }
                if(filePath.Contains(".jpg") || filePath.Contains(".jpeg"))
                {
                    SpriteTexture = LoadTexture(filePath, nonReadable);
                }
            }
            else
            {
                SpriteTexture = LoadTexture(filePath, nonReadable);
            }
            if (SpriteTexture != null)
            {
                NewSprite = Sprite.Create(SpriteTexture, new Rect(0, 0, SpriteTexture.width, SpriteTexture.height), Vector2.zero, pixelsPerUnit);
                NewSprite.name = filePath;
                if (storeInDictionary)
                {
                    spriteDictionary.Add(filePath, NewSprite);
                }
            }
            else
            {
                Debug.LogError("Texture is null! " + filePath);
            }

            return NewSprite;
        }
    }

    public static Texture2D LoadTexture(string filePath, bool nonReadable)
    {
        Texture2D tex2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex2D.LoadImage(fileData, true);
        }
        else
        {
            Debug.LogError("File does not exist! " + filePath);
        }
        //if (!nonReadable)   //can only compress readable images
        //{
        //    tex2D.Compress(false);
        //}
        tex2D.name = filePath;
        return tex2D;
    }

    // folder path should include `/` at the end
    public static void PreloadAllImagesInFolder(string folderPath)
    {
        DirectoryInfo info = new DirectoryInfo(folderPath);
        FileInfo[] fileInfo = info.GetFiles();
        foreach (FileInfo file in fileInfo)
        {
            if (file.Extension.ToLower() == ".png" || file.Extension.ToLower() == ".jpeg" || file.Extension.ToLower() == ".jpg")
            {
                LoadSprite(folderPath + file.Name);
            }
        }
    }
    public static Texture2D LoadTextureDXT(string filePath, TextureFormat textureFormat)
    {
        if (textureFormat != TextureFormat.DXT1 && textureFormat != TextureFormat.DXT5)
            throw new Exception("Invalid TextureFormat. Only DXT1 and DXT5 formats are supported by this method.");
        if (filePath.Contains(".png") && !filePath.Contains(".dds"))
        {
            filePath = filePath.Replace("media", "temp") + ".dds";
            //filePath = filePath.Replace("media", "temp");
            //filePath = filePath.Replace(".png", ".dds");
        }
        if (filePath.Contains(".jpg") || filePath.Contains(".jpeg"))
        {
            return LoadTexture(filePath, false);
        }
        if (textureDictionary.ContainsKey(filePath))
        {
            return textureDictionary[filePath];
        }
        byte[] ddsBytes = null;
        if (File.Exists(filePath))
        {
            ddsBytes = File.ReadAllBytes(filePath);
        }
        if(ddsBytes == null)
        {
            Debug.Log(filePath.ToString());
            filePath = filePath.Replace("temp", "media");
            filePath = filePath.Remove(filePath.Length - 4);
            return LoadTexture(filePath, false);
        }
        byte ddsSizeCheck = ddsBytes[4];
        if (ddsSizeCheck != 124)
            throw new Exception("Invalid DDS DXTn texture. Unable to read");  //this header byte should be 124 for DDS image files
        string b4 = ddsBytes[87].ToString();
        if(b4!="53")
        {
            textureFormat = TextureFormat.DXT1;
            //StreamWriter writer = new StreamWriter("CMS_data/cache/media/alphaFiles.txt", true);
            //writer.WriteLine(filePath);
            //writer.Close();
        }
        int height = ddsBytes[13] * 256 + ddsBytes[12];
        int width = ddsBytes[17] * 256 + ddsBytes[16];

        int DDS_HEADER_SIZE = 128;
        byte[] dxtBytes = new byte[ddsBytes.Length - DDS_HEADER_SIZE];
        Buffer.BlockCopy(ddsBytes, DDS_HEADER_SIZE, dxtBytes, 0, ddsBytes.Length - DDS_HEADER_SIZE);
        Texture2D texture = new Texture2D(width, height, textureFormat, false);
        //texture.alphaIsTransparency = true;
        texture.name = filePath;
        texture.LoadRawTextureData(dxtBytes);
        texture.Apply();
        textureDictionary.Add(filePath, texture);
        //texturesLoaded++;
        return (texture);
    }

    public static TexData CopyTextureToMemory(string filePath, TextureFormat textureFormat)
    {
        if (textureFormat != TextureFormat.DXT1 && textureFormat != TextureFormat.DXT5)
            throw new Exception("Invalid TextureFormat. Only DXT1 and DXT5 formats are supported by this method.");
        if (filePath.Contains(".png") && !filePath.Contains(".dds"))
        {
            filePath = filePath.Replace("media", "temp") + ".dds";
        }
        byte[] ddsBytes = null;
        if (File.Exists(filePath))
        {
            ddsBytes = File.ReadAllBytes(filePath);
        }
        if (ddsBytes == null)
        {
            Debug.Log(filePath);
            throw new Exception("Empty byte array or file does not exist " + filePath);
        }
        byte ddsSizeCheck = ddsBytes[4];
        if (ddsSizeCheck != 124)
            throw new Exception("Invalid DDS DXTn texture. Unable to read");  //this header byte should be 124 for DDS image files

        TexData myTexData = new TexData();
        string b4 = ddsBytes[87].ToString();
        if (b4 != "53")
        {
            textureFormat = TextureFormat.DXT1;
            //StreamWriter writer = new StreamWriter("CMS_data/cache/media/alphaFiles.txt", true);
            //writer.WriteLine(filePath);
            //writer.Close();
        }
        int height = ddsBytes[13] * 256 + ddsBytes[12];
        int width = ddsBytes[17] * 256 + ddsBytes[16];
        int DDS_HEADER_SIZE = 128;
        byte[] dxtBytes = new byte[ddsBytes.Length - DDS_HEADER_SIZE];
        Buffer.BlockCopy(ddsBytes, DDS_HEADER_SIZE, dxtBytes, 0, ddsBytes.Length - DDS_HEADER_SIZE);
        myTexData.height = height;
        myTexData.width = width;
        myTexData.myTextureFormat = textureFormat;
        myTexData.myBytes = dxtBytes;
        return myTexData;
    }
}