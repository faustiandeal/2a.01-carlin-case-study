﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexData{

    public int width = 0;
    public int height = 0;
    public TextureFormat myTextureFormat;
    public byte[] myBytes;
}
