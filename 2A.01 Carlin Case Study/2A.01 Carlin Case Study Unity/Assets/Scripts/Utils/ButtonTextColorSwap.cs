﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ButtonTextColorSwap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IPointerEnterHandler {

    public TextMeshProUGUI myText;
    public Color textPressedColor;
    private bool iGotPressed = false;
    private Color textNormalColor;

	// Use this for initialization
	void Start () {
        textNormalColor = myText.color;
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        iGotPressed = true;
        myText.color = textPressedColor;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        iGotPressed = false;
        myText.color = textNormalColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(iGotPressed)
        {
            myText.color = textNormalColor;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(iGotPressed)
        {
            myText.color = textPressedColor;
        }
    }
	
	
}
