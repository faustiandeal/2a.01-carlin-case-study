﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(CanvasGroup))]
public class FramedImageComponent : MonoBehaviour
{
    public Image FrameImage;
    public Image MainImage;

    CanvasGroup canvasGroup;
    public CanvasGroup CanvasGroup
    {
        get
        {
            if (canvasGroup == null)
            {
                canvasGroup = GetComponent<CanvasGroup>();
            }
            return canvasGroup;
        }
    }

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        if (canvasGroup == null)
        {
            Debug.LogError("Can't find CanvasGroup!");
        }
    }

    public void SetMainImage(Sprite sprite, float width = -1, float height = -1)
    {
        //Debug.Log(sprite.rect);
        MainImage.sprite = sprite;
        if (width > -1 && height > -1)
        {
            SetSize(new Vector2(width, height));
            //RectTransform thisRt = GetComponent<RectTransform>();
            //RectTransform mainImageRt = MainImage.gameObject.GetComponent<RectTransform>();
            //if (thisRt != null && mainImageRt != null)
            //{
            //    thisRt.sizeDelta = new Vector2(
            //        sprite.rect.width - (mainImageRt.sizeDelta.x / 2),
            //        sprite.rect.height - (mainImageRt.sizeDelta.y / 2));
            //}
        }
    }

    public void SetSize(Vector2 size)
    {
        //Debug.LogWarning("setting width and height: "+size);
        RectTransform thisRt = GetComponent<RectTransform>();
        RectTransform mainImageRt = MainImage.gameObject.GetComponent<RectTransform>();
        if (thisRt != null && mainImageRt != null)
        {
            //Debug.Log(thisRt.sizeDelta);
            //Debug.Log((mainImageRt.sizeDelta.x / 2));
            //thisRt.sizeDelta = size - (mainImageRt.sizeDelta / 2);
            thisRt.sizeDelta = new Vector2(
                size.x - (mainImageRt.sizeDelta.x / 2),
                size.y - (mainImageRt.sizeDelta.y / 2));
            //Debug.Log(thisRt.sizeDelta);
        }
    }

    public void SetSizeFromMainImageSprite()
    {
        RectTransform thisRt = GetComponent<RectTransform>();
        RectTransform mainImageRt = MainImage.gameObject.GetComponent<RectTransform>();
        if (thisRt != null && mainImageRt != null)
        {
            //Debug.Log(thisRt.sizeDelta);
            //Debug.Log(((float)MainImage.sprite.rect.width / 2));
            float ratio = MainImage.sprite.rect.width / MainImage.sprite.rect.height;
            //thisRt.sizeDelta = size - (mainImageRt.sizeDelta / 2);
            if (MainImage.sprite.rect.width < MainImage.sprite.rect.height)
            {
                thisRt.sizeDelta = new Vector2(
                    mainImageRt.rect.width - (mainImageRt.sizeDelta.x / 2),
                    (mainImageRt.rect.width / ratio) - (mainImageRt.sizeDelta.y / 2));
            }
            else
            {
                thisRt.sizeDelta = new Vector2(
                    (mainImageRt.rect.height * ratio) - (mainImageRt.sizeDelta.x / 2),
                    mainImageRt.rect.height - (mainImageRt.sizeDelta.y / 2));
            }
            //Debug.Log(thisRt.sizeDelta);
        }
        else
        {
            Debug.LogError("SOmething is null!");
        }
    }

    public void ToggleVisibility(bool isVisible, bool immediate = false)
    {
        LeanTween.cancel(this.gameObject);
        if (immediate)
        {
            CanvasGroup.alpha = isVisible ? 1 : 0;
        }
        else
        {
            LeanTween.alphaCanvas(CanvasGroup, isVisible ? 1 : 0, 0.5f);
        }
    }
}