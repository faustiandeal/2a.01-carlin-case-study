﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // Required when Using UI elements.

public class AlphaButton : MonoBehaviour
{
    public float AlphaThreshold = 1.0f;
    private Image myImage;

    void Start()
    {
        myImage = GetComponentInChildren<Image>();
        myImage.alphaHitTestMinimumThreshold = AlphaThreshold;
    }
}