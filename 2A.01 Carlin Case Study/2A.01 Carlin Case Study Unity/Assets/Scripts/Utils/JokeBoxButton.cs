﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JokeBoxButton : MonoBehaviour, IPointerClickHandler
{
    public event Action JokeBoxClicked;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (JokeBoxClicked != null) JokeBoxClicked();
    }
}
