﻿//using UnityEngine;
//using System.Collections;

//public class HexToColorConverter : MonoBehaviour {

//    // from: http://wiki.unity3d.com/index.php?title=HexConverter
//    public static Color HexToColor(string hex)
//    {
//        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
//        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
//        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
//        byte a = (hex.Length >= 8) ? byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber) : (byte)0xFF;

//        return new Color32(r, g, b, a);
//    }

//    public static string ColorToHex( Color32 color, bool provideAlpha = false ) {
//        string formatString = provideAlpha ? "{0:X2}{1:X2}{2:X2}{3:X2}" : "{0:X2}{1:X2}{2:X2}";
//        return string.Format(formatString, color.r, color.g, color.b, color.a);
//    }
//}
