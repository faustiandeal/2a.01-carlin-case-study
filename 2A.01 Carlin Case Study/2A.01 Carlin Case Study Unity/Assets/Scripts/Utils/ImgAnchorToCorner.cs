﻿using UnityEngine;
using System.Collections;
using System;

public class ImgAnchorToCorner : MonoBehaviour
{
    [HideInInspector]
    public RectTransform FollowRect;
    public RectTransform BoundingBox;

    [Tooltip("Padding to keep within the bounding box")]
    public Vector2 Offset;

    public enum CornerToAnchorTo
    {
        TopRight, BottomLeft, TopLeft, BottomRight
    }

    [Tooltip("Default Corner is Top Right.")]
    public CornerToAnchorTo CornerOfImage;

    Vector3[] imageCorners = new Vector3[4];
    Vector3[] boundingCorners = new Vector3[4];

    int tiedCorner
    {
        get
        {
            switch (CornerOfImage)
            {
                case CornerToAnchorTo.BottomLeft:
                    return 0;
                case CornerToAnchorTo.TopLeft:
                    return 1;
                case CornerToAnchorTo.TopRight:
                    return 2;
                case CornerToAnchorTo.BottomRight:
                    return 3;
                default:
                    return 2;


            }

        }
    }

    private void Awake()
    {

    }


    void LateUpdate()
    {

        if (FollowRect == null) return;

        FollowRect.GetWorldCorners(imageCorners);
        BoundingBox.GetWorldCorners(boundingCorners);

        Vector3 p = imageCorners[tiedCorner];
        p.x += Offset.x;
        p.y += Offset.y;
        this.transform.position = p;

        if (transform.position.x + Offset.x > boundingCorners[3].x)
        {
            p.x = boundingCorners[3].x - Offset.x;
            this.transform.position = p;
        }
        else if (transform.position.x - Offset.x < boundingCorners[0].x)
        {
            p.x = boundingCorners[0].x + Offset.x;
            this.transform.position = p;
        }
        else
        {
            this.transform.position = p;
        }

        if (transform.position.y + Offset.y > boundingCorners[2].y)
        {
            p.y = boundingCorners[2].y + Offset.y;
            this.transform.position = p;
        }
        else if (transform.position.y - Offset.y < boundingCorners[0].y)
        {
            p.y = boundingCorners[0].y - Offset.y;
            this.transform.position = p;
        }


    }


}
