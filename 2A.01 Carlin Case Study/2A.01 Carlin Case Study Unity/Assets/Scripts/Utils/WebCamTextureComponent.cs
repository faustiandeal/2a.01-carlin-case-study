﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class WebCamTextureComponent : MonoBehaviour
{
    [HideInInspector]
    public RawImage RawImage;
    [HideInInspector]
    public WebCamTexture WebcamTexture;

    public void Init(string deviceName, int width, int height)
    {
        RawImage = GetComponent<RawImage>();
        // loop through devices
        // WebCamTexture.devices
        WebcamTexture = new WebCamTexture(deviceName, width, height);
        WebcamTexture.requestedFPS = 60; // doesn't work..maybe?
        RawImage.texture = WebcamTexture;
        RawImage.material.mainTexture = WebcamTexture;
    }

    // For Debugging
    //private void Start()
    //{
    //    ToggleWebcamTexture(true);
    //}

    public void ToggleWebcamTexture(bool isActive)
    {
        if (WebcamTexture == null)
        {
            return;
        }
        if (isActive)
        {
            WebcamTexture.Play();
        }
        else
        {
            WebcamTexture.Stop();
        }
    }
}