﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using System.Collections;
using TMPro;

/// <summary>
/// This is the sub cell of the row cell
/// </summary>
public class BaggieRowCellView : MonoBehaviour
{
    public GameObject container;
    public Image imagesInBag;
    public TextMeshProUGUI FolderNameText;
    private BaggieFolderComponent FolderToOpen;
    private JokeNotesComponent myJokeNotesComponent;
    private BaseComponent myCanvasParent;
    private GameObject myScrollbar;
    /// <summary>
    /// This function just takes the Demo data and displays it
    /// </summary>
    /// <param name="data"></param>
    public void SetData(BaggieRowCellData data)
    {
        // this cell was outside the range of the data, so we disable the container.
        // Note: We could have disable the cell gameobject instead of a child container,
        // but that can cause problems if you are trying to get components (disabled objects are ignored).
        container.SetActive(data != null);

        if (data != null)
        {
            // set the text if the cell is inside the data range
            //text.text = "test text";//data.someText;
            FolderNameText.text = data.folderName;
            myJokeNotesComponent = data.myJokeNotesComponent; 
            FolderToOpen = data.myFolder;
            myCanvasParent = data.myCanvasParent;
            myScrollbar = data.myScrollbar;
            imagesInBag.sprite = data.myBaggieImage;
            //RemoveAllImagesAttachedToMe(data.hiddenSpot);
            //data.AttachImages(gameObject.transform);
        }
    }
    public void OpenFolder()
    {
        if (FolderToOpen != null)
        {
            myCanvasParent.Hide();
            myScrollbar.SetActive(false);
            StartCoroutine(FolderToOpen.ApplyMyImages((myReturnValue) =>
            {
                if (myReturnValue)
                {
                    FolderToOpen.PopulateMyDocViewer();
                    FolderToOpen.Show();
                    FolderToOpen.iAmOpen = true;
                    myJokeNotesComponent.folderIsOpen = true;
                    myJokeNotesComponent.instructionalText.text = "SWIPE AND TAP TO EXPLORE";
                }
            }));
        }
    }

    private void RemoveAllImagesAttachedToMe(Transform moveTo)
    {
        while(transform.childCount > 1)
        {
            gameObject.transform.GetChild(0).SetParent(moveTo);
        }
    } 
}
