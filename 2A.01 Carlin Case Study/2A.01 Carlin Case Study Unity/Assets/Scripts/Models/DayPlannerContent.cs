﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using TouchScript.Gestures;

public class DayPlannerContent : BaseComponent {

    [HideInInspector]
    public DocumentContainer myContainer;
    private string videoTitleOptional;
    //[HideInInspector]
    //public Image mediaImage;
    public RawImage mediaImage;
    public Material ddsMaterial;
    private Material originalMaterial;
    public string details;
    private Transform myParent;
    [HideInInspector]
    public bool hasVideo = false;
    private string movieFilePath;
    private string srtFilePath;
    [HideInInspector]
    public ContentPositionComponent myContentPos;
    [HideInInspector]
    public string myContentDate;
    [HideInInspector]
    public string myContentSubText;
    private DayPlannerPositionContentDataModel myData;
    public bool firstShow = true;
    //public VideoClip myVideoClip;

    public void Start()
    {
        //originalMaterial = GetComponent<Image>().material;
    }

    public void SetUpMyData(DayPlannerPositionContentDataModel data, ContentPositionComponent parent)
    {
        myData = data;
        myContentPos = parent;
        myParent = parent.gameObject.transform;
        //mediaImage = GetComponent<Image>();
        mediaImage = GetComponent<RawImage>();
        myContentDate = data.date;
        myContentSubText = data.subText;
        videoTitleOptional = data.videoTitle;
        string imageFilePath = null;
        originalMaterial = GetComponent<RawImage>().material;
        if ((data.media != null) && (HasAVideoExtension(data.media)))
        {
            //this is a video with a thumbnail image
            imageFilePath = CMSDataProxy.LocalMediaFilePath + data.media+".jpg";
            movieFilePath = CMSDataProxy.LocalMediaFilePath + data.media;
            srtFilePath = CMSDataProxy.LocalMediaFilePath + data.captions;
            //movieFilePath = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
            hasVideo = true;
        }
        else if ((data.media != "") && (data.media!=null))
        {
            imageFilePath = CMSDataProxy.LocalMediaFilePath + data.media;
        }
        if (imageFilePath != null && imageFilePath != "")
        {
            //mediaImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT5);
            //if(!hasVideo)
            //{
            //    GetComponent<RawImage>().material = ddsMaterial;
            //}
        }
        else
        {
            mediaImage.enabled = false;
        }
        //SetAspect();
        details = data.descriptiveText;
    }

    public bool HasAVideoExtension(string dataString)
    {
        if((dataString.Contains(".mp4")) || (dataString.Contains(".mov")))
        {
            return true;
        }
        return false;
    }

    public void ShowMyContentDateAndSubText()
    {
        myContentPos.myContentDate.text = myContentDate;
        myContentPos.myContentSubText.text = myContentSubText;
    }

    public override void Show(float time = 0.25F)
    {
        SetUpImagePos();
        base.Show(time);
    }

    public void SetUpImagePos()
    {
        string imageFilePath = null;
        if ((myData.media != null) && (HasAVideoExtension(myData.media)))
        {
            //this is a video with a thumbnail image
            imageFilePath = CMSDataProxy.LocalMediaFilePath + myData.media + ".jpg";
            mediaImage.texture = SpriteLoader.LoadTextureDXT(imageFilePath, TextureFormat.DXT1);
            GetComponent<RawImage>().material = originalMaterial;
        }
        else
        {
            mediaImage.texture = SpriteLoader.LoadTextureDXT(CMSDataProxy.LocalMediaFilePath + myData.media, TextureFormat.DXT5);
            GetComponent<RawImage>().material = ddsMaterial;
        }
        SetAspect();
    }

    private void SetAspect()
    {
        RawImage docImg = GetComponent<RawImage>();
        AspectRatioFitter myAspect = GetComponent<AspectRatioFitter>();
        float ratio = (float)docImg.texture.width / (float)docImg.texture.height;
        //float ratio = docImg.rectTransform.rect.width / docImg.rectTransform.rect.height;
        myAspect.aspectRatio = ratio;
        if (ratio > 1)
        {
            float width = 250;
            float height = width / ratio;
            docImg.rectTransform.sizeDelta = new Vector2(width, height);
            myAspect.aspectMode = AspectRatioFitter.AspectMode.WidthControlsHeight;
        }
        else
        {
            float height = 250;
            float width = ratio*height;
            docImg.rectTransform.sizeDelta = new Vector2(width, height);
            myAspect.aspectMode = AspectRatioFitter.AspectMode.HeightControlsWidth;
        }
    }

    public void OnTapped(TapGesture tapped)
    {
        if (tapped.State == Gesture.GestureState.Recognized)
        {
            DocumentViewer docView = myContainer.myViewer;
            if (hasVideo)
            {
                //docView.myVideoPlayerComponent.myVideoPlayer.url = movieFilePath;
                //docView.myVideoPlayerComponent.myVideoPlayer.clip = myVideoClip;
                //docView.myVideoPlayerComponent.myVideoPlayer.targetTexture.Release();
                docView.SetMyVideo(movieFilePath);
                docView.myVideoPlayerComponent.SetUpMySubText(details, srtFilePath);
                docView.myVideoPlayerComponent.SetUpMyTitle(myContentSubText);
                docView.ShowMyVideoPlayer();         
            }
            else
            {
                //docView.SetMyImage(mediaImage.sprite, false);
                docView.SetMyImage((Texture2D)mediaImage.texture, false);
                docView.ResetDocumentScaleAndPosition();
                docView.ShowMyDocumentImage();
            }
            docView.mainMenuButton.gameObject.SetActive(false);
            //docView.transform.SetAsLastSibling();
            docView.Show();
        }
    }
}
