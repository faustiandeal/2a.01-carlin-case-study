﻿using UnityEngine;
using System.Collections.Generic;
using cortina.architecture;
using System.Linq;
using VisitorTracking.Model;
using ComedyVisitorProfileDataModels;
using System;

public class CarlinCaseGameProxy : Proxy
{
    public const string Name = "CarlinCaseGameProxy";

    public PlayerStatusModel PlayerOneModel;

    public const float STANDARD_DELAY = 3;
#if UNITY_EDITOR
    public const float STANDARD_TIMER = 3;
#else
    public const float STANDARD_TIMER = 10;
#endif

    private const int POINT_VALUE = 1;
#if UNITY_EDITOR
    private const int ROUNDS = 4; // 6 questions per player, total 12 rounds
#else
    private const int ROUNDS = 12; // 6 questions per player, total 12 rounds
#endif

    public int Round;
    public int DisplayRound
    {
        get
        {
            return (int)Mathf.Ceil(Round / 2) + 1;
        }
    }

    public override void Initialize()
    {
        ResetVisitorData();
    }

    public void SetPlayerState(Player player, CarlinCaseFacade.States state)
    {
        if (player == Player.One)
        {
            PlayerOneModel.CurrentState = state;
        }
    }

    public void ResetVisitorData()
    {
        PlayerOneModel = new PlayerStatusModel(Player.One);
    }

    public void SendUpdatedState(Player player)
    {
        SendNotification(CarlinCaseNotifications.StateChanged, new NotificationPayloadObject
        {
            Player = player,
            Payload = (PlayerOneModel.CurrentState)
        });
    }
}

public class PlayerStatusModel : NCCVisitorDataModel
{
    public Player Player;
    public CarlinCaseFacade.States CurrentState = CarlinCaseFacade.States.Attract;
    //public JokeDataModel currentJoke;

    public PlayerStatusModel(Player playerNumber) { }
}