﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using cortina.language;
using System;
using System.Reflection;
using cortina.architecture;
using LitJson;

public class CMSDataProxy : Proxy
{
    public const string Name = "CMSDataProxy";
    public const string LocalMediaFilePath = "CMS_data/cache/media/";
    public const string JSONFilePath = "CMS_Data/cache/api_158.json";
    public const string JSONFilePath2 = "CMS_Data/cache/api_168.json";

    public CarlinCaseFacade.States CurrentState;

    public CarlinCaseDataModel CMSData { get; private set; }
    public CarlinCaseTextDataModel CMSData2 { get; private set; }

    public CMSDataProxy()
    {
        try
        {
            string configText = System.IO.File.ReadAllText(JSONFilePath);
            string configText2 = System.IO.File.ReadAllText(JSONFilePath2);
            Debug.Log(configText);
            Debug.Log(configText2);
            CarlinCaseDataModel[] cmsDataArray = JsonMapper.ToObject<CarlinCaseDataModel[]>(configText);
            CarlinCaseTextDataModel[] cmsDataArray2 = JsonMapper.ToObject<CarlinCaseTextDataModel[]>(configText2);
            CMSData = cmsDataArray[0];
            CMSData2 = cmsDataArray2[0];
        }
        catch (Exception e)
        {
            Debug.LogError("Unable to parse JSON!");
            Debug.Log(e);
        }
    }

    public override void Initialize()
    {
    }
}

#region Data Models
public class CarlinCaseTextDataModel
{
    public int Key;
    public string mainIntroText;
    public string mainIntroTextPt2;
    public string mainMenuText;
    public string mainMenuInstructionalText;
    public string careerHistoryTapText;
    public string careerHistorySwipeText;
    public string jokeNotesInstructionalText;
    public string dayPlannerInstructionalText;
}

public class CarlinCaseDataModel
{
    public int Key;
    public CareerHistoryDataModel careerHistory;
    public DayPlannersDataModel dayPlanners;
    public JokeNotesDataModel jokeNotes;
    public List<BestOfDataModel> bestOf;
}

public class CareerHistoryDataModel
{
    public int Key;
    public string description;
    public List<CareerHistoryCategoryDataModel> category;
}

public class DayPlannersDataModel
{
    public int Key;
    public string description;
    public List<DayPlannerArtifactsModel> artifacts;
}

public class JokeNotesDataModel
{
    public int Key;
    public string descriptionImagePopUp;
    public List<JokeNotesCategoryDataModel> category;
    public string description;
}

public class BestOfDataModel
{
    public int Key;
    public string image;
    public string text;
}

public class JokeNotesCategoryDataModel
{
    public int Key;
    public string title;
    public string ziplocBagImage;
    public List<JokeNotesImagesDataModel> jokeNotesImages;
}

public class JokeNotesImagesDataModel
{
    public int Key;
    public string image;
    public string transcribedText;
}

public class CareerHistoryCategoryDataModel
{
    public int Key;
    public string title;
    public List<CareerHistoryArtifactsDataModel> artifacts;
}

public class CareerHistoryArtifactsDataModel
{
    public int Key;
    public string image;
    public string transcribedText;
}

public class DayPlannerArtifactsModel
{
    public int Key;
    public string associatedImage;
    public List<RelatedContentDataModel> relatedContent;
    public DayPlannerPositionContentDataModel topLeft;
    public DayPlannerPositionContentDataModel bottomLeft;
    public DayPlannerPositionContentDataModel topRight;
    public DayPlannerPositionContentDataModel bottomRight;
    public string calendarDate;
}

public class DayPlannerPositionContentDataModel
{
    public int Key;
    public string media;
    public string date;
    public string subText;
    public string descriptiveText;
    public string videoTitle;
    public string captions;
}

public class RelatedContentDataModel
{
    //public int Key;
    //public string subText;
    //public string videoTitleOptional;
    //public string media;
    //public string details;
    //public string date;
    //public string videoThumbnail;
}

public class ConfigurableTextDataModel
{
    public string AttractTitle;
    public string AttractSubtitle;
    public string IntroBody;
    public string IntroCallToAction;
    public string AwaitingChallengerTapin;
    public string AwaitingChallengerSetup;
    public string SmileCalibrationTitle;
    public string SmileCalibrationBody;
    public string SmileCalibrationCallToAction;
    public string LaughPoints;
    public string ScoreboardWaitingForJokeSelection;
    public List<GenericStringDataModel> ScoreboardAttackingOptions;
    public List<GenericStringDataModel> ScoreboardDefendingOptions;
    public string ScoreboardJokeSuccess;
    public string ScoreboardJokeFail;
    public string ScoreboardEndGameVictorySuffix;
    public string SelectYourJokeLine1;
    public string SelectYourJokeLine2;
    public string AttackingPrepTitle;
    public string AttackingPrepBody;
    public string AttackingCallToAction;
    public string AttackingJokeSuccess;
    public string DefendingTitle;
    public string DefendingBody;
    public string DefendingJokeSuccess;
    public string Conclusion;
    public string TakeawayVictory;
    public string TakeawayDefeat;
    public string TakeawayTie;
    public List<GenericStringDataModel> TakeawayHashtags;
    public string GamefaceCallToAction;
    public string EndGameVictory;
    public string EndGameDefeat;
    public string EndGameTie;
}

public class GenericStringDataModel
{
    public string Text;
}

#endregion