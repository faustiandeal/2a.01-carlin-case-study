﻿using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using EnhancedUI;
using System;
using System.Collections.Generic;

/// <summary>
/// This is the view of our cell which handles how the cell looks.
/// It stores references to sub cells
/// </summary>
public class BaggieCellView : EnhancedScrollerCellView
    {
        public BaggieRowCellView[] baggieRowCellViews;

        /// <summary>
        /// This function just takes the Demo data and displays it
        /// </summary>
        /// <param name="data"></param>
        public void SetData(ref List<BaggieRowCellData> data, int startingIndex)
        {
            // loop through the sub cells to display their data (or disable them if they are outside the bounds of the data)
            for (var i = 0; i < baggieRowCellViews.Length; i++)
            {
                // if the sub cell is outside the bounds of the data, we pass null to the sub cell
                baggieRowCellViews[i].SetData(startingIndex + i < data.Count ? data[startingIndex + i] : null);
            }
        }
    }
