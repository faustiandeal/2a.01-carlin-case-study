﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaggieRowCellData {

    public string folderName;
    //public List<Sprite> myContainedImages = new List<Sprite>();
    public BaggieFolderComponent myFolder;
    public JokeNotesComponent myJokeNotesComponent;
    public BaseComponent myCanvasParent;
    public GameObject myScrollbar;
    public Sprite myBaggieImage;
    //public List<Image> myImages = new List<Image>();
    //public Transform baggieObjectTransform;
    [HideInInspector]
    public Transform hiddenSpot;

    public void SetUpMyImages()
    {
        //for (int i = 0; i < myContainedImages.Count; i++)
        //{
        //    GameObject containedImage = new GameObject(myContainedImages[i].name, typeof(Image));
        //    RectTransform myRect = containedImage.GetComponent<RectTransform>();
        //    Image myImage = containedImage.GetComponent<Image>();
        //    myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 300.0f);
        //    myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 300.0f);
        //    myRect.Rotate(Vector3.forward, Random.Range(-90.0f, 90.0f));
        //    myImage.sprite = myContainedImages[i];
        //    myImage.preserveAspect = true;
        //    myImages.Add(myImage);
        //}
        //myContainedImages.Clear();
    }

    //public void AttachImages(Transform trans)
    //{
    //    for (int i = 0; i < myImages.Count; i++)
    //    {
    //        myImages[i].transform.SetParent(trans);
    //        myImages[i].transform.SetAsFirstSibling();
    //        myImages[i].transform.localPosition = new Vector3(0, 0, 0);
    //    }
    //}
}
