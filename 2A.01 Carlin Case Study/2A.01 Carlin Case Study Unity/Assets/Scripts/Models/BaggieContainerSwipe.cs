﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaggieContainerSwipe : MonoBehaviour {

    public BaggieFolderComponent myFolder;

    public void Swipe()
    {
        if (myFolder.contentViewedPoint.childCount > 0)
        {
            myFolder.contentViewedPoint.GetChild(myFolder.contentViewedPoint.childCount - 1).GetComponent<SwipedDocument>().SwipedLeft();
        }
    }
}
