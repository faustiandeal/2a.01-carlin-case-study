﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using TMPro;

public class BaggieFolderCellView : EnhancedScrollerCellView 
{
    public TextMeshProUGUI FolderNameText;
    private BaggieFolderComponent FolderToOpen;

    public void SetData(BaggieScrollerData data)
    {
        FolderNameText.text = data.folderName;
        FolderToOpen = data.myFolder;
    }

    public void OpenFolder()
    {
        if (FolderToOpen != null)
        {
            FolderToOpen.Show();
            FolderToOpen.iAmOpen = true;
        }
    }
}
