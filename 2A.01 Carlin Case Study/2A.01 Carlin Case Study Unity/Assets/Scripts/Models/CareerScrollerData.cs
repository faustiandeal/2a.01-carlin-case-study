﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CareerScrollerData
{
    public string folderName;
    //public List<CareerHistoryArtifactsDataModel> Artifacts;
    public CareerFolderComponent myFolder;
    public Sprite folderImage;
    public int folderTextPlacementIndex;
    public float myCanter;
}
