﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using TMPro;

public class CareerFolderCellView : EnhancedScrollerCellView
{
    public TextMeshProUGUI FolderNameText;
    public Image folderImage;
    public Transform folderCurrentTextPlacement;
    public List<Transform> folderTextPlacementList;
    public CareerScrollerController myController;
    private CareerFolderComponent FolderToOpen;

    public void SetData(CareerScrollerData data)
    {
        FolderNameText.text = data.folderName;
        FolderToOpen = data.myFolder;
        folderImage.sprite = data.folderImage;
        folderCurrentTextPlacement.localPosition = myController.FolderTextPlacementList[data.folderTextPlacementIndex].localPosition;//folderTextPlacementList[data.folderTextPlacementIndex].localPosition;
        folderImage.gameObject.transform.localEulerAngles = new Vector3(0, 0, data.myCanter);
    }

    public void OpenFolder()
    {
        if (FolderToOpen != null)
        {
            //FolderToOpen.LoadTextures();
            StartCoroutine(FolderListFade((returnValue) =>
            {
                if (returnValue)
                {
                    StartCoroutine(FolderToOpen.ApplyMyImages((myReturnValue) =>
                    {
                        if (myReturnValue)
                        {
                            //FolderToOpen.LoadTextureFromBytes();
                            FolderToOpen.PopulateMyDocuments();
                            FolderToOpen.Show(0.0f);
                            FolderToOpen.iAmOpen = true;
                            StartCoroutine(FolderListFade((val) =>
                            {
                            }));
                        }
                    }));
                }
            }));
        }
    }

    private IEnumerator FolderListFade(System.Action<bool> callback)
    {
        if (myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha == 0.0f)
        {
            while (/*(!FolderToOpen.bytesLoaded) ||*/ 
                (myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha<1.0f))
            {
                if (myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha < 1.0f)
                {
                    myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha += Time.deltaTime;
                }
                yield return null;
            }
            callback(true);
        }
        else
        {
            while (myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha > 0.0f)
            {
                myController.PlayerComponent.CareerHistoryMenu.menuFadeImageCanvas.alpha -= Time.deltaTime;
                yield return null;
            }
            callback(true);
        }
    }
}
