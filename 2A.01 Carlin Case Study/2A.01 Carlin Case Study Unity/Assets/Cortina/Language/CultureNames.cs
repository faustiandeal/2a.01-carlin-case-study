﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cortina.language
{
    public static class CultureNames
    {
        public static string EnglishUS = "en-US";
        public static string FrenchFrance = "fr-FR";
        public static string SpanishInternational = "es-ES";
        public static string GermanGermany = "de-DE";
        public static string ChineseSimplified = "zh-CN";
    }
}
