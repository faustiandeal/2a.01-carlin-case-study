﻿using UnityEngine;
using System.Collections;
using cortina.architecture;
using System;
using System.Globalization;
using System.Collections.Generic;
namespace cortina.language
{
    public abstract class AbstractLanguageProxy<T> : Proxy, ILanguageProxy where T : class
    {
        private Dictionary<string, T> dict = new Dictionary<string, T>();

        protected void AddToDictionary(string language, T data)
        {
            dict.Add(language, data);
        }

        protected T data
        {
            get
            {
                if (!dict.ContainsKey(activeLanguage))
                {
                    throw new Exception("Error, the language " + activeLanguage + " does not have any data associated with it");
                }
                return dict[activeLanguage];
            }
        }

        private string activeLanguage = null;

        public string ActiveLanguage
        {
            get
            {
                return activeLanguage;
            }

            set
            {
                if (activeLanguage != value)
                {
                    activeLanguage = value;
                    OnLanguageChange(activeLanguage);
                }
            }
        }

        protected abstract void OnLanguageChange(string culture);

        public abstract override void Initialize();

    }
}