﻿using UnityEngine;
using System.Collections;
using System.Globalization;
namespace cortina.language
{
    public interface ILanguageProxy
    {
        string ActiveLanguage { get; set; }
    }
}
