using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Xml;
//using System.Diagnostics;
using System.IO;
using UnityEngine;

namespace Common.XML
{
    public class XmlMapper<T> : AbstractXML where T : new()
    {
        T rvalue;

        protected override void ConfigLoaded(XmlDocument xmldoc)
        {
            XmlNode oNode = xmldoc.DocumentElement;
            rvalue = (T)ParseClass(oNode, typeof(T));
        }

        public T GetData()
        {
            return rvalue;
        }

        object ParseClass(XmlNode node, Type propertyType)
        {
			if(node == null) return null;
			
           // Debug.Log("Mapping type " + propertyType);
            object newinstance = Activator.CreateInstance(propertyType);
            PropertyInfo[] pis = propertyType.GetProperties();
           // Debug.Log("The mumber of public properties is {0}." + pis.Length);
            foreach (PropertyInfo p in pis)
            {
                string translatedName = p.Name.ToLower();
                Type t = p.PropertyType;

              //   Debug.Log("Comparing type " + propertyType +  "->" + translatedName );

                if (t.IsArray)
                {
                    Type elementType = t.GetElementType();
				
					
	                    XmlNodeList list = node.SelectNodes(translatedName);
						if(list != null){
		                    ArrayList alist = new ArrayList();
		                    foreach (XmlNode n in list)
		                    {
		                        object o = ParseClass(n, elementType);
		                        alist.Add(o);
		                    }
		                    p.SetValue(newinstance, alist.ToArray(elementType), null);
						}
					
					

                   // Debug.Log("Type is array with " + alist.Count + " elements");
                }
                else if (t == typeof(int))
                {
                    int intval = 0;
					XmlNode test = node.Attributes[translatedName];
					if(test != null){
                   		 Int32.TryParse(test.Value, out intval);
                   		 p.SetValue(newinstance, intval, null);
					}

                  //  Debug.Log("Type is int = " + intval);
                }
				 else if (t == typeof(bool))
                {
                    bool intval = false;
					if(node.Attributes[translatedName] == null){
						intval = false;	
					} else {
                    	bool.TryParse(node.Attributes[translatedName].Value, out intval);
					}
						p.SetValue(newinstance, intval, null);

                   // Debug.Log("Type is bool = " + intval);
                }
                else if (t == typeof(string))
                {
					if(node.Attributes[translatedName] != null){
                    	p.SetValue(newinstance, node.Attributes[translatedName].Value, null);
						 //Debug.Log("Type is string = " + node.Attributes[translatedName].Value);
					} 
					else if(node.SelectNodes(translatedName).Count > 0){
						p.SetValue(newinstance, node.SelectNodes(translatedName)[0].InnerText.ToString(), null);
					}
					else {
						p.SetValue(newinstance, "", null);
					}
                   
                }
				 else if (t == typeof(float))
                {
                    float doubleval = 0;
					if(node.Attributes[translatedName] != null){
                    	float.TryParse(node.Attributes[translatedName].Value, out doubleval);
					}
                    p.SetValue(newinstance, doubleval, null);

                   // Debug.Log("Type is float = " + doubleval);
                }
                else if (t == typeof(double))
                {
                    double doubleval = 0;
                    double.TryParse(node.Attributes[translatedName].Value, out doubleval);
                    p.SetValue(newinstance, doubleval, null);

                   // Debug.Log("Type is double = " + doubleval);
                }
				else if (t == typeof(Vector3))
				{
					// parse vector3
				}
                else
                {
                    object o = ParseClass(node.SelectNodes(translatedName)[0], t);
                    p.SetValue(newinstance, o, null);
                }
            }
            return newinstance;
        }
    }

    public abstract class AbstractXML
    {

        public void Load(string path)
        {
			
			Debug.Log("!!!Loading XML = " + path);
			
            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                XmlDocument xmlDoc = new XmlDocument();				
                xmlDoc.XmlResolver = null;
                xmlDoc.Load(fs);
                ConfigLoaded(xmlDoc);
                fs.Close();
            }
            else
            {
                Debug.Log(path + " does not exist!");
            }
        }

        protected abstract void ConfigLoaded(XmlDocument xmldoc);

    }
}
