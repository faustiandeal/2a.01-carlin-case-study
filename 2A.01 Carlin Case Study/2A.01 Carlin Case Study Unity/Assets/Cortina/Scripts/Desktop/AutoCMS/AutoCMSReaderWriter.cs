﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using UnityEngine;
/*



*/
namespace autocms
{
    public class AutoCMSReaderWriter
    {
        private string serverurl;

        public AutoCMSReaderWriter(string cmsBaseUrl)
        {
            this.serverurl = cmsBaseUrl;
        }


        #region reading

        /// <summary>
        /// use this if you are reading a file on the hard drive
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T[] GetRecords<T>(string file)
        {

            UnityEngine.Debug.Log("getting cms file " + file);

            List<Record> mainrecords;

            CMSDataService service = new CMSDataService();
            mainrecords = service.PopulateData(file);

            return CallByReflection("GetDataList", typeof(T), mainrecords) as T[];
        }

        /// <summary>
        /// use this if you are making a request directly on the server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T[] GetRecords<T>()
        {
            List<Record> mainrecords;

            // extra section key from class
            int sectionKey = 0;
            Type ot = typeof(T);
            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(ot);
            foreach (System.Attribute attr in attrs)
            {
                if (attr is AutoCMSRecordAttribute)
                {
                    AutoCMSRecordAttribute a = (AutoCMSRecordAttribute)attr;
                    sectionKey = a.SecionKeyInt;
                    break;
                }
            }

            CMSDataService service = new CMSDataService();
            mainrecords = service.PopulateData(serverurl + "API/GetRecords/?section=" + sectionKey.ToString());

            return CallByReflection("GetDataList", typeof(T), mainrecords) as T[];
            //return GetDataList<T>(mainrecords) as T[];
        }

        Dictionary<int, string> GetKeyTypeDict(Type ot)
        {
            // build dict of key to field name
            Dictionary<int, string> dict = new Dictionary<int, string>();
            FieldInfo[] pis = ot.GetFields();
            foreach (FieldInfo p in pis)
            {
                string translatedName = p.Name;
                foreach (object attr in p.GetCustomAttributes(true))
                {
                    if (attr is AutoCMSFieldAttribute)
                    {
                        AutoCMSFieldAttribute a = attr as AutoCMSFieldAttribute;
                        dict.Add(a.FieldKeyInt, translatedName);
                    }
                }
            }
            return dict;
        }

        
        public T2 GetDataSingle<T2>(Record record) where T2 : new()
        {
            Type ot = typeof(T2);
            Dictionary<int, string> dict = GetKeyTypeDict(ot);

            object newinstance = Activator.CreateInstance(ot);

            // SPECIAL BUILT IN VARIABLES
            FieldInfo Tp = ot.GetField("AutoCMS_Flag");
            if (Tp != null)
            {
                Tp.SetValue(newinstance, record.Flag);
            }
            // row value
            Tp = ot.GetField("RowKey");
            if (Tp != null)
            {
                Tp.SetValue(newinstance, record.Key);
            }
            Tp = ot.GetField("MDate");
            if (Tp != null)
            {
                Tp.SetValue(newinstance, record.MDate);
            }


            foreach (NameValue nv in record.Fields)
            {
                if (dict.ContainsKey(nv.FKey))
                {
                    string propertyName = dict[nv.FKey];
                    FieldInfo p = ot.GetField(propertyName);
                    ProcessFields(p, nv, newinstance);
                }
            }

            return (T2)newinstance;
        }
        
        public T2[] GetDataList<T2>(List<Record> records) where T2 : new()
        {
            Type ot = typeof(T2);
            Dictionary<int, string> dict = GetKeyTypeDict(ot);

            List<T2> l = new List<T2>();

            if (records == null) return l.ToArray();

            foreach (Record r in records)
            {

                object newinstance = Activator.CreateInstance(ot);

                // SPECIAL BUILT IN VARIABLES
                FieldInfo Tp = ot.GetField("AutoCMS_Flag");
                if (Tp != null)
                {
                    Tp.SetValue(newinstance, r.Flag);
                }
                // row value
                Tp = ot.GetField("RowKey");
                if (Tp != null)
                {
                    Tp.SetValue(newinstance, r.Key);
                }
                Tp = ot.GetField("MDate");
                if (Tp != null)
                {
                    Tp.SetValue(newinstance, r.MDate);
                }

                foreach (NameValue nv in r.Fields)
                {
                    //UnityEngine.Debug.Log(nv.FKey);
                    if (dict.ContainsKey(nv.FKey))
                    {
                        string propertyName = dict[nv.FKey];
                        FieldInfo p = ot.GetField(propertyName);
                        ProcessFields(p, nv, newinstance);
                    }

                }
                l.Add((T2)newinstance);
            }
            return l.ToArray();
        }

        object ProcessLanguageField(Type ot, string json)
        {
            object newinstance = Activator.CreateInstance(ot);

            FieldInfo[] fields = ot.GetFields();

            string[] values = JsonMapper.ToObject<string[]>(json);
            int index = 0;

            foreach (FieldInfo field in fields)
            {
                if (field.DeclaringType == ot)
                {
                    NameValue nv = new NameValue();
                    nv.Value = values[index];
                    ProcessFields(field, nv, newinstance);
                    index++;
                }
            }

            return newinstance;
        }

        void ProcessFields(FieldInfo p, NameValue nv, object newinstance)
        {
            Type t = p.FieldType;

            if (t.IsArray)
            {
                Type elementType = t.GetElementType();
                p.SetValue(newinstance, CallByReflection("GetDataList", elementType, nv.Values));
            }
            else if (t.IsSubclassOf(typeof(AbstractLanguageModel)))
            {
                p.SetValue(newinstance, ProcessLanguageField(t, nv.Value));
            }
            else if (t == typeof(string))
            {
                p.SetValue(newinstance, nv.Value);
            }
            else if (t == typeof(bool))
            {
                bool intval;
                bool.TryParse(nv.Value, out intval);
                p.SetValue(newinstance, intval);
            }
            else if (t == typeof(float))
            {
                float testval;
                float.TryParse(nv.Value, out testval);
                p.SetValue(newinstance, testval);
            }
            else if (t == typeof(int))
            {
                int testval;
                int.TryParse(nv.Value, out testval);
                p.SetValue(newinstance, testval);
            }
            else if (t == typeof(double))
            {
                double testval;
                double.TryParse(nv.Value, out testval);
                p.SetValue(newinstance, testval);
            }
            else if (t == typeof(TimeSpan)) // for use with Duration Selector in CMS
            {
                TimeSpan testval = new TimeSpan();
                bool success = TimeSpan.TryParse(nv.Value, out testval);
                p.SetValue(newinstance, testval);
            }
            else if (nv.RecordValue != null)
            {
                p.SetValue(newinstance, CallByReflection("GetDataSingle", t, nv.RecordValue));
            }
        }


        float ParseFloat(string val)
        {
            float f = 0;
            float.TryParse(val, out f);
            return f;
        }

        object CallByReflection(string name, Type typeArg, object value)
        {
            MethodInfo method = typeof(AutoCMSReaderWriter).GetMethod(name);
            MethodInfo generic = method.MakeGenericMethod(typeArg);
            return generic.Invoke(this, new object[] { value });
        }


        #endregion



        #region writing

        public FileResponse AddFile(string filename, byte[] byteArray)
        {
            return UploadFilesToRemoteUrl(this.serverurl + @"Video/UploadMedia?qqfile=" + filename, new string[] { filename }, new byte[][] { byteArray });
        }



        public static FileResponse UploadFilesToRemoteUrl(string url, string[] files, byte[][] fileBytes, NameValueCollection nvc = null)
        {

           // long length = 0;
            string boundary = "----------------------------" +
            DateTime.Now.Ticks.ToString("x");


            HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest2.ContentType = "multipart/form-data; boundary=" +
            boundary;
            httpWebRequest2.Method = "POST";
            httpWebRequest2.KeepAlive = true;
            httpWebRequest2.Credentials =
            System.Net.CredentialCache.DefaultCredentials;



            Stream memStream = new System.IO.MemoryStream();

            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");


            // form keys
            string formdataTemplate = "\r\n--" + boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";
            if (nvc != null)
            {
                foreach (string key in nvc.Keys)
                {
                    string formitem = string.Format(formdataTemplate, key, nvc[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }
            }
           // memStream.Write(boundarybytes, 0, boundarybytes.Length);


            // media upload part
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n Content-Type: application/octet-stream\r\n\r\n";

            for (int i = 0; i < files.Length; i++)
            {

                //string header = string.Format(headerTemplate, "file" + i, files[i]);
                string header = string.Format(headerTemplate, "qqfile", files[i]);

                byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);

                //memStream.Write(headerbytes, 0, headerbytes.Length);


                if (fileBytes == null)
                {
                    FileStream fileStream = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;

                    while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }

                  //  memStream.Write(boundarybytes, 0, boundarybytes.Length);

                    fileStream.Close();
                }
                else
                {
                    memStream.Write(fileBytes[i], 0, fileBytes[i].Length);
                   // memStream.Write(boundarybytes, 0, boundarybytes.Length);
                }



            }

            httpWebRequest2.ContentLength = memStream.Length;

            Stream requestStream = httpWebRequest2.GetRequestStream();

            memStream.Position = 0;
            byte[] tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            requestStream.Close();


            WebResponse webResponse2 = httpWebRequest2.GetResponse();

            Stream stream2 = webResponse2.GetResponseStream();
            StreamReader reader2 = new StreamReader(stream2);

            string s = Encoding.UTF8.GetString(ReadToEnd(stream2));


            FileResponse filerp = LitJson.JsonMapper.ToObject<FileResponse>(s);



            webResponse2.Close();
            httpWebRequest2 = null;
            webResponse2 = null;

            return filerp;
        }



        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }


        public int AddRecord(IAutoCMSUploadable def)
        {
            // convert to json
            int sectionkey;
            int parentkey;
            List<int> fields = new List<int>();
            List<string> values = new List<string>();


            Type ot = def.GetType();

            AutoCMSRecordAttribute at = (ot.GetCustomAttributes(typeof(AutoCMSRecordAttribute), false) as AutoCMSRecordAttribute[])[0];
            sectionkey = at.SecionKeyInt;
            parentkey = at.ParentKeyInt;

            FieldInfo[] pis = ot.GetFields();
            foreach (FieldInfo p in pis)
            {
                foreach (object attr in p.GetCustomAttributes(true))
                {
                    if (attr is AutoCMSFieldAttribute)
                    {
                        AutoCMSFieldAttribute a = attr as AutoCMSFieldAttribute;
                        fields.Add(a.FieldKeyInt);
                        values.Add(p.GetValue(def).ToString());
                    }
                }
            }


            StringBuilder sb = new StringBuilder();

            sb.Append("{\"sectionKey\":" + sectionkey + ",\"parentid\":" + parentkey + ",\"fieldKeys\": [");

            int count = 0;
            foreach (int i in fields)
            {
                if (count > 0)
                {
                    sb.Append(",");
                }
                sb.Append(i.ToString());
                count++;
            }

            sb.Append(" ],");
            sb.Append("\"fieldValues\": [");

            count = 0;
            foreach (string s in values)
            {
                if (count > 0)
                {
                    sb.Append(",");
                }
                sb.Append("\"" + s + "\"");
                count++;
            }

            sb.Append(" ]");
            sb.Append("}");


           string respon =  Http.Post(serverurl + @"CMS\AddRecordExternal", sb.ToString());

            int rtnkey = 0;
            int.TryParse(respon, out rtnkey);
            return rtnkey;
        }


        #endregion
    }


    public class FileResponse
    {
        public bool success;
        public string filename;
        public string thumb;
        public string message;
    }



    public static class Http
    {
        public static byte[] Post(string uri, NameValueCollection pairs)
        {
            byte[] response = null;
            using (WebClient client = new WebClient())
            {
                response = client.UploadValues(uri, pairs);
            }
            return response;
        }

        public static string Post(string uri, string json)
        {
            string response = null;
            using (WebClient client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                response = client.UploadString(uri, json);
            }
            return response;
        }
    }

    public interface IAutoCMSUploadable
    {

    }

    [AttributeUsage(AttributeTargets.Field)]
    public class AutoCMSLanguageAttribute : Attribute
    {
        public readonly string Language;
        public readonly bool IsDefault;

        public AutoCMSLanguageAttribute(string val)
        {
            Language = val;
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class AutoCMSFieldAttribute : Attribute
    {
        public readonly int FieldKeyInt;

        public AutoCMSFieldAttribute(int val)
        {
            FieldKeyInt = val;
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AutoCMSRecordAttribute : Attribute
    {
        public readonly int SecionKeyInt;
        public readonly int ParentKeyInt;

        public AutoCMSRecordAttribute(int section, int parent)
        {
            SecionKeyInt = section;
            ParentKeyInt = parent;
        }
    }


    public class CMSDataService
    {

        public List<Record> PopulateData(string url)
        {


            string text = "";
            Console.WriteLine(url);

            if (url.IndexOf("http") != -1)
            {

                var request = WebRequest.Create(url);
                request.Timeout = (int)TimeSpan.FromSeconds(60).TotalMilliseconds;
                request.ContentType = "application/json; charset=utf-8";


                try
                {
                    var response = (HttpWebResponse)request.GetResponse();
                    using (var sr = new StreamReader(response.GetResponseStream()))
                    {
                        text = sr.ReadToEnd();
                    }
                }
                catch (Exception err)
                {
                    // retry in 10 seconds
                    // Debug.LogError(" error getting data... " + err);

                    // PopulateData(sec, surl);
                    return null;
                }
            }
            else
            {
                try
                {
                    text = System.IO.File.ReadAllText(url);
                }
                catch (Exception err)
                {
                    // Debug.LogError(" error getting data... " + err);
                    return null;
                }
            }

            //    Console.WriteLine(text);
            //	Debug.Log (text);

            JsonData data = JsonMapper.ToObject(text);

            List<Record> l = ParseRecordArray(data);


            /*foreach(Record r in l){
                Debug.Log("parsing " + r.Key);
                foreach(NameValue nv in r.Fields){
                    Debug.Log("->"+nv.Type);
                    Debug.Log(nv.Value);
                    Debug.Log(nv.Values);
                }
            }*/

            return l;
        }

        List<Record> ParseRecordArray(JsonData data)
        {
            List<Record> l = new List<Record>();

            int total = data.Count;
            //Debug.Log("record array total="+total);

            for (int i = 0; i < total; ++i)
            {
                Record r = new Record();
                r.Key = (int)(data[i]["Key"]);
                //Debug.Log("key="+r.Key);
                r.Fields = ParseFieldsArray(data[i]["Fields"]);
                l.Add(r);
            }

            return l;
        }

        Record ParseRecord(JsonData data)
        {
            Record r = new Record();
            r.Key = (int)(data["Key"]);
            //Debug.Log("key="+r.Key);
            r.Fields = ParseFieldsArray(data["Fields"]);
            return r;
        }

        List<NameValue> ParseFieldsArray(JsonData data)
        {
            List<NameValue> l = new List<NameValue>();

            int total = data.Count;

            for (int i = 0; i < total; ++i)
            {
                NameValue r = new NameValue();
                r.FKey = (int)data[i]["FKey"];
                r.Type = (int)data[i]["Type"];
                try
                {
                    if (r.Type == 1)
                    {
                        // ******************************************************EXperiment
                        // string record
                        var v = data[i]["Value"];
                        if (v != null && v.GetJsonType() == JsonType.String)
                        {
                            r.Value = (string)data[i]["Value"]; // old line
                        }
                        else if (v != null)
                        {
                            r.RecordValue = ParseRecord(data[i]["Value"]);
                        }
                        else
                        {
                            r.Value = "null no value";
                        }
                        //*************************************************************
                    }
                    else
                    {
                        // list record
                        r.Values = ParseRecordArray(data[i]["Value"]);
                    }
                }
                catch
                {
                    // single record option
                    try
                    {
                        r.RecordValue = ParseRecord(data[i]["Value"]);
                    }
                    catch
                    {
                        // bad!
                        r.Value = "null no value";
                    }
                }
                l.Add(r);
            }

            return l;
        }



    }



    #region web service types

    //[Serializable]
    public class Record
    {
        public int Key { get; set; }
        public int Flag { get; set; }
        public List<NameValue> Fields { get; set; }
        public DateTime MDate { get; set; }
    }

    //[Serializable]
    public class NameValue
    {
        public int FKey { get; set; }
        public string Name { get; set; }
        public List<Record> Values { set; get; }
        public Record RecordValue { set; get; }
        public string Value { set; get; }
        public int Type { get; set; }
    }

    //[Serializable]
    public class NameKey
    {
        public string Name { get; set; }
        public int Key { set; get; }
    }

    #endregion



    public class BaseAutoCMSModel
    {
        public int AutoCMS_Flag;
        public int RowKey;
        public DateTime MDate;
    }

    public class AbstractLanguageModel
    {
        public string this[int index]
        {
            get
            {
                string fieldName = (this.GetType().GetFields().GetValue(index) as FieldInfo).Name;
                return (string)this.GetType().GetField(fieldName).GetValue(this);
            }
        }
    }
}
