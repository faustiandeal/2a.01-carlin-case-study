﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Cortina/Stencil Mask/Stencil Mask Cover"
 {
     Properties
     {
          _MainTex ("Sprite Texture", 2D) = "white" {}     
     }
 
     SubShader
     {
         Tags
         {
             "Queue"="Transparent+1"
             "IgnoreProjector"="True"
             "RenderType"="Transparent"
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         //Ztest [_ZTestMode]
         Fog { Mode Off }
         Blend SrcAlpha OneMinusSrcAlpha
         
          GrabPass { "_MyGrabTexture" }
 
         Pass
         {
             
       
         CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
             #include "UnityCG.cginc"
       
             struct appdata_t
             {
                 float4 vertex   : POSITION;
                 float4 color    : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f
             {
                 float4 vertex   : SV_POSITION;
                 fixed4 color    : COLOR;
                 half2 texcoord  : TEXCOORD1;
                 float4 grabUV : TEXCOORD0;
             };
       
             fixed4 _Color;
 
             v2f vert(appdata_t IN)
             {
                 v2f OUT;
                 OUT.vertex = UnityObjectToClipPos(IN.vertex);
                 OUT.texcoord = IN.texcoord;
                 OUT.color = IN.color * _Color;
            	 OUT.grabUV = ComputeGrabScreenPos(OUT.vertex);
                 return OUT;
             }
 
             sampler2D _MainTex;
             sampler2D _AlphaMask;
             sampler2D _MyGrabTexture;
 
             fixed4 frag(v2f IN) : SV_Target
             {
	             //
	             fixed4 c = tex2D(_MyGrabTexture, IN.grabUV);
	             fixed4 al = tex2D(_MainTex, IN.texcoord);
	            // c.rgb =0;// 1-al.a;
	             c.a = 1-al.a;
	             //c.a = al.a;// - al.a;
	               //  fixed4 c = tex2Dproj( _MyGrabTexture, UNITY_PROJ_COORD(IN.grabUV));// 
	                // if (c.a<0.1) discard; 
	             return c;
             }
         ENDCG
         }
     }
     
     
     
     
     
 }
 