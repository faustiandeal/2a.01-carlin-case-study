﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Cortina/SmoothAlpha Additive"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff ("Alpha cutoff", Range(-1,1)) = 0.5
	}
	SubShader 
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200
		ZWrite Off
		Fog { Mode Off }
		Blend OneMinusDstColor One
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			sampler2D _MainTex;
			fixed4 _Color;
			float _Cutoff;
			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}
			half4 frag(v2f i) : COLOR
			{
				fixed4 c = _Color * tex2D(_MainTex, i.uv);
				if(c.a  < _Cutoff) discard;// * _Color.a;
				//fixed ca = tex2D(_CutTex, i.uv).a;
				//c.a = (ca + _Cutoff) * c.a;
				//c.a *= ca > _Cutoff ? 1f : 0f;
				return c;
			}
			ENDCG
		}
	}
	Fallback "Transparent/VertexLit"
}