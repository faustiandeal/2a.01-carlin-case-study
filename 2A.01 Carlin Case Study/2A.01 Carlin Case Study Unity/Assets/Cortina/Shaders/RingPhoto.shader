﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Cortina/RingPhoto"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Photo", 2D) = "white" {}
		_RingTex ("Ring Mask", 2D) = "white" {}
	}
	SubShader 
	{
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		LOD 200
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#define TRANSFORM_TEX(tex,name) (tex.xy * name##_ST.xy + name##_ST.zw)
			sampler2D _MainTex;
			sampler2D _RingTex;
			fixed4 _Color;
			
			float4 _MainTex_ST;
			
			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				//o.uv = v.texcoord.xy;
				return o;
			}
			half4 frag(v2f i) : COLOR
			{
				fixed4 c = tex2D(_MainTex, i.uv);
				fixed4 r = tex2D(_RingTex, i.uv);
				c.a = r.r * _Color.a;
				
				return c;
			}
			ENDCG
		}
		
		
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			sampler2D _RingTex;
			fixed4 _Color;
			
			
			struct appdata
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}
			half4 frag(v2f i) : COLOR
			{
				fixed4 r = tex2D(_RingTex, i.uv);
				
				r.a = r.g;
				r.rgb = _Color.rgb;
				r.a *= _Color.a;
				return r;
			}
			ENDCG
		}
		
		
	}
	Fallback "Transparent/VertexLit"
}