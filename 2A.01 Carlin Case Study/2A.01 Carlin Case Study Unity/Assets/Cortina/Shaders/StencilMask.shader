﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Cortina/Stencil Mask/Stencil Mask"
 {
     Properties
     {
         // _MainTex ("Sprite Texture", 2D) = "white" {}     
     }
 
     SubShader
     {
         Tags
         {
             "Queue"="Transparent"
             "IgnoreProjector"="True"
             "RenderType"="Transparent"
             "PreviewType"="Plane"
             "CanUseSpriteAtlas"="True"
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         Fog { Mode Off }
         Blend SrcAlpha OneMinusSrcAlpha
         
         GrabPass { "_MyGrabTexture" }
 
         Pass
         {
             Stencil
             {
                 Ref 1
                 Comp always
                 Pass replace
             }
       
         CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #pragma multi_compile DUMMY PIXELSNAP_ON
             #include "UnityCG.cginc"
       
             struct appdata_t
             {
                 float4 vertex   : POSITION;
                 float4 color    : COLOR;
                 float2 texcoord : TEXCOORD0;
             };
 
             struct v2f
             {
                 float4 vertex   : SV_POSITION;
                 fixed4 color    : COLOR;
                 half2 texcoord  : TEXCOORD0;
               //  float4 grabUV : TEXCOORD1;
             };
       
             fixed4 _Color;
 
             v2f vert(appdata_t IN)
             {
                 v2f OUT;
                 OUT.vertex = UnityObjectToClipPos(IN.vertex);
                 OUT.texcoord = IN.texcoord;
                 OUT.color = IN.color * _Color;
                // OUT.grabUV = ComputeGrabScreenPos(OUT.vertex);
                 return OUT;
             }
 
             sampler2D _MainTex;
             sampler2D _AlphaMask;
             sampler2D _MyGrabTexture;
 
             fixed4 frag(v2f IN) : SV_Target
             {
                // fixed4 c = tex2D(_MainTex, IN.texcoord);
                // if (c.a==0) discard; 
                 
                return half4(0,0,0,0);
                 
               // fixed4 g = tex2D(_MyGrabTexture, IN.grabUV);
               // g.rgb = 0;
                //return g;
             }
         ENDCG
         }
     }
     
     
     
     
     
 }
 