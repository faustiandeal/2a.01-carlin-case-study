﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Panel : MonoBehaviour {

    public Image ImageComponent;

	public Color Color
    {
        get
        {
            return ImageComponent.color;
        }
        set
        {
            ImageComponent.color = value;
        }
    }

    public float Alpha
    {
        set
        {
            Color c = ImageComponent.color;
            c.a = value;
            ImageComponent.color = c;
        }
        get
        {
            return ImageComponent.color.a;
        }
    }
}
