﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ViewError : MonoBehaviour
{
    public Text FeedbackText;

    private void Awake()
    {
        transform.localPosition = Vector3.zero;
    }

    public void DisplayError(string err)
    {
        FeedbackText.text = err;
    }
}
