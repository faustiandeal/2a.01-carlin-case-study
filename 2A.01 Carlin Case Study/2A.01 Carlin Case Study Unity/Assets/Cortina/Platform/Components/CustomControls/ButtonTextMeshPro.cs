﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.EventSystems;
//using UnityEngine.UI;
//using TMPro;
//using System;

//[RequireComponent(typeof(Button))]
//public class ButtonTextMeshPro : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
//{
//    [HideInInspector]
//    public Button Button;
//    public TextMeshProUGUI Label;
//    public Image DropShadowImage;

//    protected bool pointerIsDown = false;
//    protected float timeColorTransitionFinished;

//    public bool Interactable
//    {
//        get
//        {
//            return Button.IsInteractable();
//        }
//        set
//        {
//            SetInteractable(value);
//        }
//    }

//    public string Text
//    {
//        get
//        {
//            return Label.GetParsedText();
//        }
//        set
//        {
//            Label.SetText(value);
//        }
//    }

//    private void Awake()
//    {
//        Button = GetComponent<Button>();
//        if (Button == null)
//        {
//            Debug.LogError("Can't Find Button!");
//        }
//        else
//        {
//            Button.onClick.AddListener(() =>
//            {
//                Label.color = Button.colors.pressedColor;
//            });
//        }
//        SetInteractable(Button.interactable);//Button.IsInteractable());
//    }
//    protected void Update()
//    {
//        if (timeColorTransitionFinished > Time.time)
//        {
//            if (pointerIsDown)
//            {
//                Label.color = Color.Lerp(Button.colors.normalColor, Button.colors.pressedColor, (timeColorTransitionFinished - Time.time) / timeColorTransitionFinished);
//            }
//            else
//            {
//                Label.color = Color.Lerp(Button.colors.pressedColor, Button.colors.normalColor, (timeColorTransitionFinished - Time.time) / timeColorTransitionFinished);
//            }
//        }
//    }

//    public void SetInteractable(bool isInteractable)
//    {
//        Button.interactable = isInteractable;
//        Label.color = new Color(Label.color.r, Label.color.g, Label.color.b, (isInteractable ? Button.colors.normalColor.a : Button.colors.disabledColor.a));
//    }

//    public void OnPointerDown(PointerEventData eventData)
//    {
//        if (Interactable)
//        {
//            pointerIsDown = true;
//            timeColorTransitionFinished = Time.time + Button.colors.fadeDuration;
//        }
//    }

//    public void OnPointerExit(PointerEventData eventData)
//    {
//        if (Interactable)
//        {
//            pointerIsDown = false;
//            timeColorTransitionFinished = Time.time + Button.colors.fadeDuration;
//        }
//    }

//    public void OnPointerUp(PointerEventData eventData)
//    {
//        if (Interactable)
//        {
//            pointerIsDown = false;
//            timeColorTransitionFinished = Time.time + Button.colors.fadeDuration;
//        }
//    }

//    private void OnDisable()
//    {
//        pointerIsDown = false;
//        Label.color = Button.colors.pressedColor;
//    }
//}