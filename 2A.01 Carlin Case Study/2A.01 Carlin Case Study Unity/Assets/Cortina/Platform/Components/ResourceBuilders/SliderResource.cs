﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class SliderResource : RectTransformResource<Slider>
    {
        public override string ResourceID
        {
            get
            {
                return "Slider";
            }
        }
    }
}
