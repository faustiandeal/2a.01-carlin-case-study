﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class VertLayoutResource : LayoutResource<VerticalLayoutGroup>
    {
        public override string ResourceID
        {
            get
            {
                return "VerticalLayout";
            }
        }

       


    }
}
