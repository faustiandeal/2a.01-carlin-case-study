﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
namespace cortina.platform
{
    [XmlResource]
    public class TextMeshResource : RectTransformResource<TMPro.TextMeshProUGUI>
    {
        public override string ResourceID
        {
            get
            {
                return "TextMeshPro";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, TMPro.TextMeshProUGUI unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "FontSize":
                    unityObject.fontSize = XmlResourceBuilder.ConvertToType<int>(attributeValue);
                    break;
                case "Color":
                    unityObject.color = XmlResourceBuilder.ConvertToType<Color>(attributeValue);
                    break;
                case "Font":
                    unityObject.font = Resources.Load<TMPro.TMP_FontAsset>(attributeValue);
                    break;
                case "LineSpacing":
                    unityObject.lineSpacing = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    break;
                default:
                     base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }
        }

        public override string ConvertPropertyName(string input)
        {
            switch (input)
            {
                case "Text":
                    return "text";
            }
            return input;
        }

    }

}