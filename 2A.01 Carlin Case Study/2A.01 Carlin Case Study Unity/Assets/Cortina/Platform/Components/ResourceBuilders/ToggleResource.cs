﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class ToggleResource : RectTransformResource<Toggle>
    {
        public override string ResourceID
        {
            get
            {
                return "Toggle";
            }
        }
    }
}
