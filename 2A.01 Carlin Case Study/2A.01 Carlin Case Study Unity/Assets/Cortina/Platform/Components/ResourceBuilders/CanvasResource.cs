﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class CanvasResource : RectTransformResource<Canvas>
    {
        public override string ResourceID
        {
            get
            {
                return "Canvas";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, Canvas unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "Interactable":
                    unityObject.GetComponent<GraphicRaycaster>().enabled = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }


        }


    }
}
