﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
namespace cortina.platform
{
    [XmlResource]
    public class RawImageResource : RectTransformResource<UnityEngine.UI.RawImage>
    {
        public override string ResourceID
        {
            get
            {
                return "RawImage";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, RawImage unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "Source":
                    Sprite spr = Resources.Load<Sprite>(attributeValue);
                    if (spr != null)
                    {
                        unityObject.texture = spr.texture;
                    }
                    break;
                case "Interactable":
                    unityObject.raycastTarget = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }
        }
        
    } // end class
} // end namespace