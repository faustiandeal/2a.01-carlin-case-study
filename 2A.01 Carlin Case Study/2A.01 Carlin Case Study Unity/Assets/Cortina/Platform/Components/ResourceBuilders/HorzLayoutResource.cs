﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class HorzLayoutResource : LayoutResource<HorizontalLayoutGroup>
    {
        public override string ResourceID
        {
            get
            {
                return "HorizontalLayout";
            }
        }
        

    }

   
}
