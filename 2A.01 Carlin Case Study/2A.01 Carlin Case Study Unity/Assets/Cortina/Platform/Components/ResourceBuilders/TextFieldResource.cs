﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
namespace cortina.platform
{
    [XmlResource]
    public class TextFieldResource : RectTransformResource<UnityEngine.UI.Text>
    {
        public override string ResourceID
        {
            get
            {
                return "TextField";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, Text unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "FontSize":
                    unityObject.fontSize = XmlResourceBuilder.ConvertToType<int>(attributeValue);
                    break;
                case "Color":
                    unityObject.color = XmlResourceBuilder.ConvertToType<Color>(attributeValue);
                    break;
                case "Font":
                    //unityObject.font = 
                    break;
                case "LineSpacing":
                    unityObject.lineSpacing = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    break;
                default:
                     base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }

           
        }

        public override string ConvertPropertyName(string input)
        {
            switch (input)
            {
                case "Text":
                    return "text";
            }
            return input;
        }

    }

}