﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Reflection;

namespace cortina.platform
{
    [XmlResource]
    public class ButtonResource : RectTransformResource<UnityEngine.UI.Button>
    {
        public override string ResourceID
        {
            get
            {
                return "Button";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, Button unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "OnClick":
                    MethodInfo methodInfo = facade.GetType().GetMethod(attributeValue);
                    unityObject.onClick.AddListener(()=>
                    {
                        methodInfo.Invoke(facade, new object[] { unityObject });
                    });
                    break;
                case "Alpha":
                    Image img = unityObject.GetComponent<Image>();
                    Color c = img.color;
                    c.a = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.GetComponent<Image>().color = c;
                    break;
                case "Text":
                    unityObject.transform.Find("Text").GetComponent<Text>().text = ((attributeValue == null) ? "": attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }

           
        }
    }
}