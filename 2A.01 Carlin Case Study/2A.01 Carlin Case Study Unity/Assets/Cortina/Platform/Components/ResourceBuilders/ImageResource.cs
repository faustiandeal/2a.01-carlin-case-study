﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class ImageResource : RectTransformResource<UnityEngine.UI.Image>
    {
        public override string ResourceID
        {
            get
            {
                return "Image";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, Image unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "Source":
                    string extension = System.IO.Path.GetExtension(attributeValue);
                    if (extension.Length == 0)
                    {
                        unityObject.sprite = Resources.Load<Sprite>(attributeValue);
                    }
                    else
                    {   
                        string fullpath = System.IO.Path.Combine(Application.streamingAssetsPath, "images/" + attributeValue);
                        if (System.IO.File.Exists(fullpath))
                        {
                            Texture2D tex = new Texture2D(1, 1);
                            tex.LoadImage(System.IO.File.ReadAllBytes(fullpath));
                            tex.Apply();
                            unityObject.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
                            unityObject.name = fullpath;
                        }
                        else
                        {
                            Debug.LogError("Image not found: " + fullpath);
                        }
                    }
                    break;
                case "Interactable":
                    unityObject.raycastTarget = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }
        }
        
    }
}