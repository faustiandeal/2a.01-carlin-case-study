﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
namespace cortina.platform
{
    [XmlResource]
    public class GroupResource : RectTransformResource<CanvasGroup>
    {
        public override string ResourceID
        {
            get
            {
                return "Group";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, CanvasGroup unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "Alpha":
                    unityObject.alpha = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    break;
                case "Interactable":
                    unityObject.interactable = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }


        }
    }
}
