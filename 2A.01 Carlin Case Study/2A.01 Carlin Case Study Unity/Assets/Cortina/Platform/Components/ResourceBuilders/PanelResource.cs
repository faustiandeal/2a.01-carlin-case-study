﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class PanelResource : RectTransformResource<Panel>
    {
        public override string ResourceID
        {
            get
            {
                return "Panel";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, Panel unityObject, XmlViewComponent facade)
        {

            switch (attributeName)
            {
                case "Image":
                    unityObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(attributeName);
                    break;
            }

            base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
        }
    }
}
