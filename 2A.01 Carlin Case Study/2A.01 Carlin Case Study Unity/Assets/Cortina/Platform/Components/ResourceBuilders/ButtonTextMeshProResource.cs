﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Reflection;

namespace cortina.platform
{
    [XmlResource]
    public class ButtonTextMeshProResource : RectTransformResource<ButtonTextMeshPro>
    {
        public override string ResourceID
        {
            get
            {
                return "ButtonTextMeshPro";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, ButtonTextMeshPro unityObject, XmlViewComponent xmlViewComponent)
        {
            switch (attributeName.ToLower())
            {
                case "onclick":
                    MethodInfo methodInfo = xmlViewComponent.GetType().GetMethod(attributeValue);
                    unityObject.Button.onClick.AddListener(()=>
                    {
                        methodInfo.Invoke(xmlViewComponent, new object[] { unityObject });
                    });
                    break;
                case "alpha":
                    Image img = unityObject.GetComponent<Image>();
                    Color c = img.color;
                    c.a = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.Button.image.color = c;
                    break;
                case "text":
                    unityObject.Label.SetText((attributeValue == null) ? "": attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, xmlViewComponent);
                    break;
            }

           
        }
    }
}