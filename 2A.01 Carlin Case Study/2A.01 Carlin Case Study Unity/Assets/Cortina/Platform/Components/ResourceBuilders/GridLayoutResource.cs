﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace cortina.platform
{
    [XmlResource]
    public class GridLayoutResource : LayoutResource<GridLayoutGroup>
    {
        public override string ResourceID
        {
            get
            {
                return "GridLayout";
            }
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, GridLayoutGroup unityObject, XmlViewComponent facade)
        {
            float[] values = new float[0];
            switch (attributeName.ToLower())
            {
                case "gridpadding":
                case "padding":
                    values = GetCommaSeparatedValues(attributeValue);
                    if (values.Length == 1)
                    {
                        unityObject.padding = new RectOffset((int)values[0], (int)values[0], (int)values[0], (int)values[0]);
                    }
                    else if (values.Length == 2 || values.Length == 3)
                    {
                        unityObject.padding = new RectOffset((int)values[0], (int)values[1], (int)values[0], (int)values[1]);
                    }
                    else if (values.Length == 4)
                    {
                        unityObject.padding = new RectOffset((int)values[0], (int)values[1], (int)values[2], (int)values[3]);
                    }
                    break;
                case "cellsize":
                    values = GetCommaSeparatedValues(attributeValue);
                    if (values.Length == 1)
                    {
                        unityObject.cellSize = new Vector2(values[0], values[0]);
                    }
                    else if (values.Length == 2)
                    {
                        unityObject.cellSize = new Vector2(values[0], values[1]);
                    }
                    break;
                case "cellspacing":
                case "spacing":
                    values = GetCommaSeparatedValues(attributeValue);
                    if (values.Length == 1)
                    {
                        unityObject.spacing = new Vector2(values[0], values[0]);
                    }
                    else if (values.Length == 2)
                    {
                        unityObject.spacing = new Vector2(values[0], values[1]);
                    }
                    break;
                case "columns":
                case "columncount":
                    unityObject.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
                    unityObject.constraintCount = int.Parse(attributeValue);
                    break;
                case "rows":
                case "rowcount":
                    unityObject.constraint = GridLayoutGroup.Constraint.FixedRowCount;
                    unityObject.constraintCount = int.Parse(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }
        }

        float[] GetCommaSeparatedValues(string attributeValue)
        {
            char[] commaSplitter = new char[1] { ',' };
            string[] stringValues = attributeValue.Split(commaSplitter);
            List<float> listValues = new List<float>();
            try
            {
                for (int i = 0; i < stringValues.Length; i++)
                {
                    listValues.Add(float.Parse(stringValues[i]));
                }
                return listValues.ToArray();
            }
            catch(Exception e)
            {
                Debug.LogError("Could not parse attribute value '" + attributeValue + "' into values");
                return new float[0];
            }
        }
    }
}