﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System;

namespace cortina.platform
{
    public interface IXmlResource
    {
        string ResourceID { get; }
        Component Instantiate(ViewDescriptor descp);
        Type GetResourceType();
        string ConvertPropertyName(string input);
    }
}
