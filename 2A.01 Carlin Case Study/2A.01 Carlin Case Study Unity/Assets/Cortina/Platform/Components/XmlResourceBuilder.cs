﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System;
using System.Reflection;
using System.Linq;
using System.IO;
using UnityEngine.UI;

namespace cortina.platform
{
    public static class XmlResourceBuilder
    {
        private static Dictionary<Component, ViewDescriptor> viewDescriptorDict = new Dictionary<Component, ViewDescriptor>();
        private static Dictionary<Type, IXmlResource> typeDict = new Dictionary<Type, IXmlResource>();
        private static Dictionary<string, IXmlResource> resourceDict = new Dictionary<string, IXmlResource>();

        public static void InitializeEngine()
        {
            resourceDict.Clear();
            typeDict.Clear();
            viewDescriptorDict.Clear();

            LoadAllResourceBuilders(Assembly.GetExecutingAssembly());
        }

        static void LoadAllResourceBuilders(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetCustomAttributes(typeof(XmlResourceAttribute), true).Length > 0)
                {
                    IXmlResource instance = Activator.CreateInstance(type) as IXmlResource;
                    AddToDict(instance);
                }
            }
        }

        public static ViewDescriptor GetViewDescriptor(Component c)
        {
            return viewDescriptorDict[c];
        }
        public static void RemoveDesciptor(Component c)
        {
            viewDescriptorDict.Remove(c);
        }

        public static Transform GenerateElement(ViewDescriptor descrip)
        {
            // resource dictionary only contains elements which have [Resource] Attribute or have been created before
            // if it is not in our resource dictionary, it is probably an xml view
            if (!resourceDict.ContainsKey(descrip.ElementName))
            {
                Transform hostTransform = descrip.ParentNode;

                string eleName = descrip.ElementName;
                string pathprefix = descrip.ElementPath;

                string xmlPath = ((pathprefix.Length > 0) ? pathprefix + "/" : "") + eleName + ".xml";
                string combined = System.IO.Path.Combine(Application.streamingAssetsPath, xmlPath);

                Debug.Log("Generating " + combined);

                if (File.Exists(combined))
                {
                    // check to see if this is xml view
                    XmlViewComponent comp = new XmlView().ConstructView(xmlPath, hostTransform, true, descrip);
                    if (comp == null) throw new Exception("XmlViewComponent \"" + eleName + "\" cannot be constructed");
                    viewDescriptorDict.Add(comp, descrip);
                    return comp.transform;
                }
                else
                {
                    // try to dynamically create our object (Highly experimental)
                    string classname = eleName;
                    Type t = FindTypeInKnownAssemblies(classname);//, Assembly.GetExecutingAssembly());
                    if (t == null)
                    {
                        throw new Exception("Unknown element type " + classname);
                    }
                    var d1 = typeof(GenericResource<>);
                    Type[] typeArgs = { t };
                    var makeme = d1.MakeGenericType(typeArgs);
                    IXmlResource o = Activator.CreateInstance(makeme) as IXmlResource;
                    resourceDict.Add(descrip.ElementName, o);

                    Component c = o.Instantiate(descrip);
                    descrip.Component = c;
                    return c.transform;
                }


            }
            else
            {
                // load resource component
                Component c = resourceDict[descrip.ElementName].Instantiate(descrip);
                descrip.Component = c;
                return c.transform;
            }
        }

        static Type FindTypeInKnownAssemblies(string type)
        {
            System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var A in assemblies)
            {
                foreach (Type t in A.GetTypes())
                {
                    if (t.ToString().IndexOf(type) != -1)
                    {
                        return t;
                    }
                }
            }

            return null;
        }

        internal static string GetConvertedName(Type ctype, string name)
        {
            return typeDict[ctype].ConvertPropertyName(name);
        }

        static void AddToDict(IXmlResource r)
        {
            Type type = r.GetResourceType();
            if (typeDict.ContainsKey(type))
            {
                throw new Exception("ResourceBuilder: type already part of dictionary = " + type.ToString() + ", " + r.ResourceID);
            }
            typeDict.Add(type, r);
            resourceDict.Add(r.ResourceID, r);
        }

        // possible to make a cache for faster subsequent looksups, TODO
        public static bool ConvertAttributeToProperty(string attributeName, string attributeValue, Component unityObject)
        {
            // don't do ones with data binding since it can't be converted here
            if (attributeValue.IndexOf('{') != -1) return false;

            // default to unity variables
            Type t = unityObject.GetType();

            // check properties
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo g in props)
            {
                if (g.Name == attributeName)
                {
                    object converted = XmlResourceBuilder.ConvertToType(attributeValue, g.PropertyType);
                    try
                    {
                        g.SetValue(unityObject, converted, null);
                        return true;
                    }
                    catch (Exception err)
                    {
                        Debug.LogError("Cannot convert " + t + "." + g.Name + " to " + converted.GetType() + ". Must be instead " + g.GetType());
                    }

                }
            }

            // check fields
            foreach (FieldInfo g in t.GetFields())
            {
                if (g.Name == attributeName)
                {
                    object converted = XmlResourceBuilder.ConvertToType(attributeValue, g.FieldType);
                    try
                    {
                        g.SetValue(unityObject, converted);
                        return true;
                    }
                    catch (Exception err)
                    {
                        Debug.LogError("Cannot convert " + t + "." + g.Name + " to " + converted.GetType() + ". Must be instead " + g.GetType());
                    }

                }
            }

            //Debug.LogWarning("Unconvertable attribute " + attributeName + " on " + unityObject.name + "(" + unityObject.GetType() + ")");
            return false;
        }



        public static T ConvertToType<T>(string attributeValue)
        {
            Type type = typeof(T);
            return (T)XmlResourceBuilder.ConvertToType(attributeValue, type);
        }

        public struct MinMax
        {
            public float Min { get; set; }
            public float Max { get; set; }

            public MinMax(float min, float max)
            {
                Min = min;
                Max = max;
            }
        }

        public static MinMax ConvetAlignToVector(string input)
        {
            if (input == "Right")
            {
                return new MinMax(1, 1);
            }
            else if (input == "Left")
            {
                return new MinMax(0, 0);
            }
            else if (input == "Center")
            {
                return new MinMax(0.5f, 0.5f);
            }
            else if (input == "Top")
            {
                return new MinMax(1, 1);
            }
            else if (input == "Bottom")
            {
                return new MinMax(0, 0);
            }
            else if (input == "Stretch")
            {
                return new MinMax(0, 1);
            }
            else
            {
                return new MinMax(0.5f, 0.5f);
            }
        }

        public static object ConvertToType(string attributeValue, Type type)
        {
            if (type == typeof(string))
            {
                return attributeValue;
            }
            else if (type == typeof(float))
            {
                return float.Parse(attributeValue);
            }
            else if (type == typeof(int))
            {
                return int.Parse(attributeValue);
            }
            else if (type == typeof(Color))
            {
                Color c;
                ColorUtility.TryParseHtmlString(attributeValue, out c);
                return c;
            }
            else if (type == typeof(bool))
            {
                bool b;
                bool.TryParse(attributeValue, out b);
                return b;
            }
            else if (type == typeof(Vector2))
            {
                string[] parts = attributeValue.Split(',');
                return new Vector2(
                    float.Parse(parts[0]),
                    float.Parse(parts[1])
                    );
            }
            else if (type == typeof(Vector3))
            {
                string[] parts = attributeValue.Split(',');
                return new Vector3(
                    float.Parse(parts[0]),
                    float.Parse(parts[1]),
                     float.Parse(parts[2])
                    );
            }
            else if (type == typeof(RectOffset))
            {
                string[] splits = attributeValue.Split(' ');
                return new RectOffset(
                        ConvertToType<int>(splits[0]),
                        ConvertToType<int>(splits[1]),
                        ConvertToType<int>(splits[2]),
                        ConvertToType<int>(splits[3])
                        );
            }
            else if (type == typeof(TextAnchor))
            {
                switch (attributeValue)
                {
                    case "UpperLeft":
                        return TextAnchor.UpperLeft;
                    case "LowerLeft":
                        return TextAnchor.LowerLeft;
                    case "UpperRight":
                        return TextAnchor.UpperRight;
                    case "LowerRight":
                        return TextAnchor.LowerRight;
                    case "UpperCenter":
                        return TextAnchor.UpperCenter;
                    case "LowerCenter":
                        return TextAnchor.LowerCenter;
                    default:
                        return TextAnchor.MiddleCenter;
                }
            }
            else
            {
                return attributeValue;
            }


        }
    }
}