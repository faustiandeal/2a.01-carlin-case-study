﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace cortina.platform
{
    public abstract class LayoutResource<T> : RectTransformResource<T> where T: Component
    {
        public override abstract string ResourceID
        {
            get;
        }

        public override void OnProcessAttribute(string attributeName, string attributeValue, T unityObject, XmlViewComponent facade)
        {
            HorizontalOrVerticalLayoutGroup g = unityObject.GetComponent<HorizontalOrVerticalLayoutGroup>();
            switch (attributeName)
            {
                case "ForceChildWidth":
                    g.childForceExpandWidth = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                case "ForceChildHeight":
                    g.childForceExpandHeight = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                case "ControlChildHeight":
                    g.childControlHeight = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                case "ControlChildWidth":
                    g.childControlWidth = XmlResourceBuilder.ConvertToType<bool>(attributeValue);
                    break;
                case "Spacing":
                    g.spacing = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    break;
                case "Padding":
                    if(attributeName.Contains(" "))
                    {
                        g.padding = XmlResourceBuilder.ConvertToType<RectOffset>(attributeValue);
                    }
                    else
                    {
                        int val = XmlResourceBuilder.ConvertToType<int>(attributeValue);
                        g.padding = new RectOffset(val, val, val, val);
                    }
                    break;
                case "ChildAlignment":
                    g.childAlignment = XmlResourceBuilder.ConvertToType<TextAnchor>(attributeValue);
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }
        }
    }
}
