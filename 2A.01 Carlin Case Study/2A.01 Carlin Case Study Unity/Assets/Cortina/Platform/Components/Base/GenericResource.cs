﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace cortina.platform
{
    public class GenericResource<T> : RectTransformResource<T> where T : Component
    {
        private string resourceId = "no resource";

        public override string ResourceID
        {
            get
            {
                return resourceId;
            }
        }

        protected override T CreateComponent(ViewDescriptor descp)
        {
            GameObject go = new GameObject();
            go.AddComponent<RectTransform>();
            return go.AddComponent<T>();
        }
    }
}
