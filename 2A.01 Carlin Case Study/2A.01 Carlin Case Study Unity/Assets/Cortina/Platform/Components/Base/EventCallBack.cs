﻿using cortina.platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Cortina.Platform.Components.Base
{
    public class EventCallBack
    {
        public XmlViewComponent Target;
        public string Method;
    }
}
