﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Linq;
using System.Reflection;


namespace cortina.platform
{
    public abstract class AbstractResource<T> : Node, IXmlResource where T : Component
    {
        public abstract string ResourceID
        {
            get;
        }

        public virtual string ConvertPropertyName(string input)
        {
            return input;
        }

        public Type GetResourceType()
        {
            return typeof(T);
        }

        protected virtual T CreateComponent(ViewDescriptor descp)
        {
            GameObject go = GameObject.Instantiate(Resources.Load<GameObject>(ResourceID));

            return go.GetComponent<T>();
        }

        public Component Instantiate(ViewDescriptor descp)
        {
            T comp = CreateComponent(descp);
            if (comp == null)
            {
                Debug.LogError("unable to create component "
                + descp.XmlElementSource.Name.LocalName
                + " -- " + descp.ElementPath);
            }
            comp.name = descp.XmlElementSource.Name.LocalName;

            comp.transform.SetParent(descp.ParentNode, false);

            // apply name of element as a style
            ApplyStyleSheet(descp.ElementName, comp, descp.StyleSheetRef, descp.XmlView);

            foreach (XAttribute attr in descp.XmlElementSource.Attributes())
            {
                // check for data binding
                if (attr.Value[0] == '{' && attr.Value[attr.Value.Length - 1] == '}')
                {
                    descp.BindingCxt.AddBinder(descp, attr.Name.ToString(), attr.Value.Substring(1, attr.Value.Length - 2));
                }
                else
                {
                    if (attr.Name == "Style")
                    {
                        ApplyStyleSheet(attr.Value, comp, descp.StyleSheetRef, descp.XmlView);
                    }
                    else
                    {
                        OnProcessAttribute(attr.Name.LocalName, attr.Value, comp, descp.XmlView);
                    }
                }
            }

            OnPostInstantiate(comp);

            return comp;
        }

        protected virtual void OnPostInstantiate(T component)
        {

        }

        // based off GameObject
        public virtual void OnProcessAttribute(string attributeName, string attributeValue, T unityObject, XmlViewComponent facade)
        {
            switch (attributeName)
            {
                case "X":
                    Vector3 vx = unityObject.transform.localPosition;
                    vx.x = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.transform.localPosition = vx;
                    return;
                case "Y":
                    Vector3 vy = unityObject.transform.localPosition;
                    vy.y = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.transform.localPosition = vy;
                    return;
                case "Z":
                    Vector3 vz = unityObject.transform.localPosition;
                    vz.z = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.transform.localPosition = vz;
                    return;
                case "Rotation2D":
                    Vector3 rot2s = new Vector3(
                        0, 0, XmlResourceBuilder.ConvertToType<float>(attributeValue)
                        );
                    unityObject.transform.localEulerAngles = rot2s;
                    return;
                case "Rotation":
                    string[] rotspits = attributeName.Split(',');
                    Vector3 rot = new Vector3(
                        XmlResourceBuilder.ConvertToType<float>(rotspits[0]),
                         XmlResourceBuilder.ConvertToType<float>(rotspits[1]),
                         XmlResourceBuilder.ConvertToType<float>(rotspits[2]));
                    unityObject.transform.localScale = rot;
                    return;
                case "Scale":
                    string[] scaleSplits = attributeName.Split(',');
                    Vector3 scale;
                    if (scaleSplits.Length == 1)
                    {
                        scale.x = scale.y = scale.z = XmlResourceBuilder.ConvertToType<float>(scaleSplits[0]);
                    }
                    else
                    {
                        scale = new Vector3(
                            XmlResourceBuilder.ConvertToType<float>(scaleSplits[0]),
                            XmlResourceBuilder.ConvertToType<float>(scaleSplits[1]),
                            XmlResourceBuilder.ConvertToType<float>(scaleSplits[2])
                            );
                    }
                    unityObject.transform.localScale = scale;
                    return;
                case "Scale2D":
                    Vector3 scale2d = unityObject.transform.localScale;
                    scale2d.x = scale2d.y = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    unityObject.transform.localScale = scale2d;
                    return;
                case "Active":
                    unityObject.gameObject.SetActive(XmlResourceBuilder.ConvertToType<bool>(attributeValue));
                    return;
                case "Name":
                    unityObject.gameObject.name = attributeValue;
                    return;
            }

            XmlResourceBuilder.ConvertAttributeToProperty(ConvertPropertyName(attributeName), attributeValue, unityObject);

            // can an abstractresource bind its event also?
            // if so, then code will go here
        }

        protected virtual void ApplyStyleSheet(string style, T unityObject, StyleSheet ss, XmlViewComponent facade)
        {
            // lookup style sheet
            if (ss == null) return;

            StyleSheetParameters[] styleSettings = ss.GetStyle(style);
            if (styleSettings != null)
            {
                foreach (StyleSheetParameters sp in styleSettings)
                {
                    OnProcessAttribute(sp.StyleName, sp.StyleValue, unityObject, facade);
                }
            }
        }


    }
}