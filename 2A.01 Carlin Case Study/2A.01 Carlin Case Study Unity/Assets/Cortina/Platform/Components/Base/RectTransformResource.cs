﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace cortina.platform
{
    public abstract class RectTransformResource<T> : AbstractResource<T> where T : Component
    {

        public override abstract string ResourceID
        {
            get;
        }

        // based off RectTransforms
        public override void OnProcessAttribute(string attributeName, string attributeValue, T unityObject, XmlViewComponent facade)
        {
            RectTransform rt = unityObject.GetComponent<RectTransform>();

            switch (attributeName)
            {   
                case "Width":
                    Vector2 size = rt.sizeDelta;
                    size.x = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    rt.sizeDelta = size;
                    break;
                case "Height":
                    Vector2 size2 = rt.sizeDelta;
                    size2.y = XmlResourceBuilder.ConvertToType<float>(attributeValue);
                    rt.sizeDelta = size2;
                    break;
                case "AlignHorz":
                    XmlResourceBuilder.MinMax stretchHorzVal = XmlResourceBuilder.ConvetAlignToVector(attributeValue);
                    rt.anchorMin = new Vector2(stretchHorzVal.Min, rt.anchorMin.y);
                    rt.anchorMax = new Vector2(stretchHorzVal.Max, rt.anchorMax.y);
                    break;
                case "AlignVert":
                    XmlResourceBuilder.MinMax stretchVertVal = XmlResourceBuilder.ConvetAlignToVector(attributeValue);
                    rt.anchorMin = new Vector2(rt.anchorMin.x, stretchVertVal.Min);
                    rt.anchorMax = new Vector2(rt.anchorMax.x, stretchVertVal.Max);
                    break;
                case "Margins":
                    string[] splits = attributeValue.Split(' ');
                    rt.offsetMax = new Vector2(
                        -XmlResourceBuilder.ConvertToType<float>(splits[0]),
                        -XmlResourceBuilder.ConvertToType<float>(splits[1]));
                    rt.offsetMin = new Vector2(
                        XmlResourceBuilder.ConvertToType<float>(splits[2]),
                        XmlResourceBuilder.ConvertToType<float>(splits[3]));
                    break;
                case "Pivot":
                    string[] commasplit = attributeValue.Split(',');
                    rt.pivot = new Vector2(
                        XmlResourceBuilder.ConvertToType<float>(commasplit[0]),
                        XmlResourceBuilder.ConvertToType<float>(commasplit[1]));
                    break;
                case "OverrideSize":
                    LayoutElement le = unityObject.GetComponent<LayoutElement>();
                    if (le == null) le = unityObject.gameObject.AddComponent<LayoutElement>();
                    le.preferredHeight = rt.sizeDelta.y;
                    le.preferredWidth = rt.sizeDelta.x;
                    ContentSizeFitter csf = unityObject.GetComponent<ContentSizeFitter>();
                    if (csf == null) csf = unityObject.gameObject.AddComponent<ContentSizeFitter>();
                    csf.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
                    csf.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
                    break;
                default:
                    base.OnProcessAttribute(attributeName, attributeValue, unityObject, facade);
                    break;
            }

        }

        protected override void OnPostInstantiate(T component)
        {
            base.OnPostInstantiate(component);

            //RectTransform rt = component.GetComponent<RectTransform>();

            //// if we are stretching width and height, let's remove margins so our rect transform fits to container
            //// that is if we have no width and height, otherwise let that width and height help define margins
            //if (rt.anchorMin == Vector2.zero && rt.anchorMax == Vector2.one)
            //{
            //    // if no width or height is set
            //    if (rt.sizeDelta == Vector2.zero)
            //    {
            //        rt.offsetMax = new Vector2(0, 0);
            //        rt.offsetMin = new Vector2(0, 0);
            //    }
            //}
        }
    }
}
