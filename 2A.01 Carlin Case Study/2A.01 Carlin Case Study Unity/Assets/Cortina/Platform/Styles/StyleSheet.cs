﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace cortina.platform
{
    public class StyleSheet
    {
        private static Dictionary<string, StyleSheetInfo> dictionary = new Dictionary<string, StyleSheetInfo>();
        private StyleSheetInfo targetSheet;

        public static void ClearCache()
        {
            dictionary.Clear();
        }

        public StyleSheetInfo GetActiveSheet()
        {
            return targetSheet;
        }

        public StyleSheet(string cssFile)
        {
            // input can be a file, or inline styles, we tell if there's a } char in it.
            if (cssFile.IndexOf('}') == -1)
            {

                if (!dictionary.ContainsKey(cssFile))
                {
                    string fpath = System.IO.Path.Combine(Application.streamingAssetsPath, "styles/" + cssFile);
                    string contents = System.IO.File.ReadAllText(fpath);
                    dictionary.Add(cssFile, ProcessStyleSheet(contents));
                }
                targetSheet = dictionary[cssFile];
            }
            else
            {
                targetSheet = ProcessStyleSheet(cssFile);
            }

           

        }

        // can use a better parser, this is quick and dirty
        StyleSheetInfo ProcessStyleSheet(string sheetContents)
        {
            StyleSheetInfo info = new StyleSheetInfo();

            // get parts
            string[] parts = sheetContents.Split('}');
            foreach (string p in parts)
            {
                string[] halves = p.Split('{');
                if (halves.Length == 2)
                {
                    string target = halves[0];
                    string values = halves[1];
                    // remove period in front of style (in case anybody does that)
                    //if(target[0] == '.')
                    //{
                    //    target = target.Substring(1, target.Length - 1);
                    //}
                    info.AddTarget(CleanString(target), CreateParameterList(halves[1]));
                }
            }


            return info;
        }

        StyleSheetParameters[] CreateParameterList(string rawContents)
        {
            List<StyleSheetParameters> list = new List<StyleSheetParameters>();
            string[] parameterRows = rawContents.Split(';');
            foreach (string r in parameterRows)
            {
                string[] keyvaluepair = r.Split(':');
                if (keyvaluepair.Length == 2)
                {
                    list.Add(new StyleSheetParameters(CleanString(keyvaluepair[0]), CleanString(keyvaluepair[1])));
                }
            }
            return list.ToArray();
        }

        string CleanString(string input)
        {
          return input.Trim().Trim().Replace("\r", string.Empty).Replace("\n", string.Empty);
        }

        // @TODO need to return actual data from the css file
        public StyleSheetParameters[] GetStyle(string name)
        {
            return targetSheet.GetTarget(name).Parameters;
        }

        // creates new dictionary from internal style sheet and imported one
        internal void CombineStyles(StyleSheet styleSheet)
        {
            StyleSheetInfo info = new StyleSheetInfo();

            IDictionary dict = targetSheet.GetDictionary();
            foreach(KeyValuePair<string, StyleSheetTarget> kvp in dict)
            {
                info.AddTarget(kvp.Key, kvp.Value.Parameters);
            }

            dict = styleSheet.GetActiveSheet().GetDictionary();
            foreach (KeyValuePair<string, StyleSheetTarget> kvp in dict)
            {
                info.AddTarget(kvp.Key, kvp.Value.Parameters);
            }

            targetSheet = info;
        }
    }

    public class StyleSheetInfo
    {
        private Dictionary<string, StyleSheetTarget> styeDict = new Dictionary<string, StyleSheetTarget>();

        public Dictionary<string, StyleSheetTarget> GetDictionary()
        {
            return styeDict;
        }

        public void AddTarget(string name, StyleSheetParameters[] parameters)
        {
            IDictionary dict = styeDict;
            dict.Add(name, new StyleSheetTarget()
            {
                Name = name,
                Parameters = parameters
            });
        }

        public StyleSheetTarget GetTarget(string name)
        {
            if (!styeDict.ContainsKey(name))
            {
                return StyleSheetTarget.NoStyles;
            }
            return styeDict[name];
        }
    }

    public class StyleSheetTarget
    {
        public static StyleSheetTarget NoStyles = new StyleSheetTarget()
        {
            Enabled = false
        };

        public bool Enabled = true;
        public string Name;
        public StyleSheetParameters[] Parameters;
    }

    public class StyleSheetParameters
    {
        public string StyleName;
        public string StyleValue;

        public StyleSheetParameters(string n, string v)
        {
            StyleName = n;
            StyleValue = v;
        }
    }

}