﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace cortina.platform
{
    public class Program : MonoBehaviour
    {
        public string Startup;
        public Transform TargetTransform;

        public GameObject BuildCallback;

#if UNITY_EDITOR
        public bool WatchFiles;
#endif

        // Use this for initialization
        void Awake()
        {
            XmlResourceBuilder.InitializeEngine();
            XmlViewComponent component = null;

            try
            {
                component = new XmlView().ConstructView(Startup, TargetTransform, true);
            }
            catch (Exception err)
            {
                ViewError go = Instantiate<ViewError>(Resources.Load<ViewError>("View Error"));
                go.transform.SetParent(TargetTransform, false);
                go.DisplayError(err.ToString());
                Debug.LogError(err);
            }

#if UNITY_EDITOR
            if (WatchFiles)
            {
                XmlWatcher watcher = gameObject.AddComponent<XmlWatcher>();
                watcher.ProgramReference = this;
            }
#endif

            if (BuildCallback != null)
                BuildCallback.SendMessage("Built", component);
        }

        public void Rebuild()
        {
            StyleSheet.ClearCache();
            XmlView.ClearCache();

            // wipe scene and memory
            foreach (Transform t in TargetTransform)
            {
                Destroy(t.gameObject);
            }
            Resources.UnloadUnusedAssets();

            Awake();
        }
    }
}

public class XmlViewException : Exception
{
    public XmlViewException(string message) : base(message) { }
}