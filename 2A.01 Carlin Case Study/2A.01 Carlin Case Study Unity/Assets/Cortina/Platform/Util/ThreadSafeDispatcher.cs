using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ThreadSafeDispatcher<T> : MonoBehaviour
{

	#region message threading sync
	
	// thread shared objects
	//private bool isMessageSync = false;
	private Queue<T> syncMessages = new Queue<T>();
	
	// is is called from a thread besides the main thread
	protected void SyncMessage(T val)
	{
		// prevent two threads from queuing at the same time
		lock(this){
			//isMessageSync = true;	
			syncMessages.Enqueue(val);
		}
	}
	
	// the main thread calls this to access the values put in
	// this may not need locking, since this is the only place dequeing and worse case is it thinks there is less in the queue then there really is
	private void CheckThreadSync()
	{	
		lock(this){
			// threading sync ability
			//if(isMessageSync){
				while(syncMessages.Count > 0){
					ReceiveThreadSafeMessage(syncMessages.Dequeue());	
				}
				//if(syncMessages.Count == 0) isMessageSync = false;
			//}	
		}	
	}
	
	// called at teh speed of the physics engine
	// read from thread shared
	protected virtual void FixedUpdate()
	{
		CheckThreadSync();
	}
		
	protected abstract void ReceiveThreadSafeMessage(T mes);
	
	#endregion
	             
}

