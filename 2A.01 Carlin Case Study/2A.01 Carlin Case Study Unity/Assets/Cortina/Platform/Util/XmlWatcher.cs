﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using cortina.platform;
using System.Text.RegularExpressions;

public class XmlWatcher : ThreadSafeDispatcher<FileSystemEventArgs>
{
    FileSystemWatcher xmlWatch;
    public Program ProgramReference;

    // Use this for initialization
    void Start()
    {
        // Create a new FileSystemWatcher and set its properties.
        xmlWatch = new FileSystemWatcher();
        xmlWatch.Path = Path.Combine(Application.streamingAssetsPath,"views");
        xmlWatch.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
        xmlWatch.Filter = "";
        xmlWatch.IncludeSubdirectories = true;
        xmlWatch.Changed += new FileSystemEventHandler(OnChanged);
        xmlWatch.EnableRaisingEvents = true;
    }

    private void OnChanged(object sender, FileSystemEventArgs e)
    {
        string strFileExt = Path.GetExtension(e.FullPath).ToLower();
        // filter file types 
        if (strFileExt == ".xml" || strFileExt == ".css")
        {
            SyncMessage(e);
        }
        
    }

    protected override void ReceiveThreadSafeMessage(FileSystemEventArgs mes)
    {
        ProgramReference.Rebuild();
    }

    private void OnApplicationQuit()
    {
        if(xmlWatch != null)
        {
            xmlWatch.Changed -= new FileSystemEventHandler(OnChanged);
            xmlWatch.EnableRaisingEvents = false;
            xmlWatch = null;
        }
    }
}
