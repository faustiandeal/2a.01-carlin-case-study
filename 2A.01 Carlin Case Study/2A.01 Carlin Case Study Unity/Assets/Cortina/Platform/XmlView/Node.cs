﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
/*
* Node is the base class for all Xml Entities that get rendered into Unity objects.
* This constructs the inner elements of an XmlView
*/
namespace cortina.platform
{
    public class Node
    {
        BindingContext bindings = new BindingContext();
        protected XmlViewComponent codebehind;

        public void ProcessNodeWithScript<T>(Transform parentNod) where T : XmlViewComponent
        {
            T script = parentNod.gameObject.AddComponent<T>();
            codebehind = script;
        }

        protected BindingContext GetBindingContext()
        {
            return bindings;
        }

        // context is a list of all the bindings in the view, these must be fed into the code behind
        protected void ExecuteBindings()
        {
            if (codebehind != null)
            {
                codebehind.ProcessBindings(bindings);
            }
        }

        protected Transform ConstructNode(ViewDescriptor descrip)
        {
         //   Debug.Log("process element " + descrip.ElementName);

            descrip.ElementTransform = XmlResourceBuilder.GenerateElement(descrip);

            // process any children of this element
            foreach (XElement el in descrip.XmlElementSource.Elements())
            {

                ConstructNode(new ViewDescriptor()
                {
                    ElementName = el.Name.LocalName,
                    ElementPath = el.Name.NamespaceName,
                    XmlElementSource = el,
                    ParentNode = descrip.ElementTransform,
                    StyleSheetRef = descrip.StyleSheetRef,
                    BindingCxt = descrip.BindingCxt,
                    XmlView = descrip.XmlView
                });
            }

            return descrip.ElementTransform;
        }

    }
}