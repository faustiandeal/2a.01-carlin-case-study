﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.IO;
using System.Linq;
using Assets.Cortina.Platform.Components.Base;
/**
* Constructs XmlViewComponent from the XML definition
*/
namespace cortina.platform
{
    public class XmlView : Node
    {
        private GroupResource heiarchyProxy;
        private static Dictionary<string, string> xmlCache = new Dictionary<string, string>();
        private IDictionary<string, string> namespaceDictionary;
        public static void ClearCache()
        {
            xmlCache.Clear();
        }

        // load xml for application startup.  
        public XmlViewComponent ConstructView(string xmlPath, Transform mainNode, bool buildNode = false, ViewDescriptor parentDescriptor = null)
        {
            try
            {
                System.Diagnostics.Stopwatch watch = System.Diagnostics.Stopwatch.StartNew();

                Transform hostTransform;
                if (buildNode)
                {
                    hostTransform = CreateRootNode(mainNode);
                }
                else
                {
                    hostTransform = mainNode;
                }

                // make all xml views into a canvas group. In the future we'll let xml views define their own
                // xml mapping object
                CanvasGroup grp = hostTransform.gameObject.AddComponent<CanvasGroup>();
                heiarchyProxy = new GroupResource();

                // read xml
                if (!xmlCache.ContainsKey(xmlPath))
                {
                    string combined = System.IO.Path.Combine(Application.streamingAssetsPath, xmlPath);
                    try
                    {
                        string fileContents = System.IO.File.ReadAllText(combined);
                        xmlCache.Add(xmlPath, fileContents);
                    }
                    catch
                    {
                        throw new Exception("Unable to locate xml " + combined);
                    }
                }
                XmlReader reader = XmlReader.Create(new StringReader(xmlCache[xmlPath]));
                XmlDocument doc = new XmlDocument();
                XmlNode node = doc.ReadNode(reader);


                bool scriptSet = false;
                StyleSheet ss = null;

                //get list of namespaces
                //XmlNamespaceManager nsMgr = new XmlNamespaceManager(doc.NameTable);
                //namespaceDictionary = nsMgr.GetNamespacesInScope(XmlNamespaceScope.All);

                // process internal attributes, but make sure to do script first
                XmlAttribute scriptattr = node.Attributes["Script"];
                if (scriptattr != null)
                {
                    // attach code behind
                    XmlAttribute at = scriptattr;

                    if (at != null)
                    {
                        string script = at.Value;
                        Type codeBehindScriptType = null;
                        codeBehindScriptType = Type.GetType(script);
                        PutScriptOnObject(hostTransform, codeBehindScriptType);
                        scriptSet = true;
                    }
                }
                foreach (XmlAttribute att in node.Attributes)
                {
                    if (att.Name == "Script")
                    {
                        // we already have processed this
                        continue;
                    }
                    else if (att.Name == "StyleSheet")
                    {
                        // load style sheet if exists  
                        XmlAttribute atss = att; //node.Attributes["StyleSheet"];

                        if (atss != null)
                        {
                            ss = new StyleSheet(atss.Value);
                        }
                    }
                    else if (att.Name == "InheritStylesheet")
                    {
                        // add or use parent styles if parent sheet exists
                        XmlAttribute inheritStyleAttr = att;// node.Attributes["InheritStylesheet"];
                        bool inheritStyle = true;
                        if (inheritStyleAttr != null)
                        {
                            inheritStyle = XmlResourceBuilder.ConvertToType<bool>(inheritStyleAttr.Value);
                        }
                        if (inheritStyle && parentDescriptor != null && parentDescriptor.StyleSheetRef != null)
                        {
                            if (ss == null)
                            {
                                ss = parentDescriptor.StyleSheetRef;
                            }
                            else
                            {
                                ss.CombineStyles(parentDescriptor.StyleSheetRef);
                            }
                        }
                    }
                    else if (att.Name == "Styles")
                    {
                        ss = new StyleSheet(att.Value);
                    }
                    else
                    {
                        // default canvas group attribute processing
                        heiarchyProxy.OnProcessAttribute(att.Name.ToString(), att.Value, grp, null);

                        // now do any xmlview properties
                        if (scriptSet)
                        {
                            XmlResourceBuilder.ConvertAttributeToProperty(att.Name.ToString(), att.Value, codebehind);
                        }
                    }
                }

                // process any external attributes set in the view loading our view
                if (parentDescriptor != null)
                {
                    foreach (XAttribute attr in parentDescriptor.XmlElementSource.Attributes())
                    {
                        if (attr.Name.ToString() == "DataContext")
                        {
                            // not sure how we do more then strings right now
                            codebehind.ProcessBindings(GetBindingContext());
                            codebehind.DataContext = attr.Value;
                        }
                        else if (attr.Name.ToString() == "OnEventTrigger")
                        {
                            codebehind.SetEventCallback(new EventCallBack()
                            {
                                Method = attr.Value,
                                Target = parentDescriptor.XmlView
                            });
                        }
                        else
                        {
                            // default canvas group attribute processing
                            heiarchyProxy.OnProcessAttribute(attr.Name.ToString(), attr.Value, grp, null);

                            // now do any xmlview properties
                            if (scriptSet)
                            {
                                string attributeName = attr.Name.ToString();
                                bool attributeAdded = XmlResourceBuilder.ConvertAttributeToProperty(attributeName, attr.Value, codebehind);
                                //if our attribute failed to add, could it be an event?
                                if (!attributeAdded)
                                {
                                    Type t = codebehind.GetType();
                                    foreach (EventInfo e in t.GetEvents())
                                    {
                                        if (e.Name == attributeName)
                                        {
                                            try
                                            {
                                                MethodInfo mi = parentDescriptor.XmlView.GetType().GetMethod(attr.Value);
                                                e.AddEventHandler(
                                                    codebehind,
                                                    Delegate.CreateDelegate(e.EventHandlerType, parentDescriptor.XmlView, mi)
                                                    );
                                            } catch(Exception err)
                                            {
                                                Debug.LogError("Can't bind event " + attributeName + " to " + attr.Value + " on parent view." + Environment.NewLine + err.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    hostTransform.name = xmlPath;
                }


                if (!scriptSet)
                {
                    Type codeBehindScriptType = typeof(XmlViewComponent);
                    PutScriptOnObject(hostTransform, codeBehindScriptType);
                }



                // process children
                foreach (XmlNode el in node.ChildNodes)
                {
                    if (el.NodeType == XmlNodeType.Element)
                    {
                        XElement elem = XElement.Parse(el.OuterXml);

                        ConstructNode(new ViewDescriptor()
                        {
                            XmlView = codebehind,
                            ElementName = elem.Name.LocalName,
                            ElementPath = elem.Name.NamespaceName,
                            XmlElementSource = elem,
                            ParentNode = hostTransform,
                            StyleSheetRef = ss,
                            BindingCxt = GetBindingContext()
                        });
                    }
                }

                ExecuteBindings();

                Debug.Log("<color=green>View " + xmlPath + " Constructed in " + watch.Elapsed.TotalSeconds + " seconds</color>");
            }
            catch (Exception error)
            {
                throw new XmlViewException(xmlPath + Environment.NewLine + Environment.NewLine + error.ToString());
            }


            codebehind.OnViewInitialized();
            return codebehind;

        }


        void PutScriptOnObject(Transform tr, Type t)
        {
            MethodInfo method = typeof(Node).GetMethod("ProcessNodeWithScript");
            MethodInfo generic = method.MakeGenericMethod(t);
            generic.Invoke(this, new object[] { tr });
        }

        Transform CreateRootNode(Transform parent)
        {
            GameObject go = new GameObject();
            RectTransform rt = go.AddComponent<RectTransform>();

            go.transform.SetParent(parent, false);
            go.transform.localPosition = Vector3.zero;

            rt.anchorMin = new Vector2(0, 0);
            rt.anchorMax = new Vector2(1, 1);

            rt.offsetMax = new Vector2(0, 0);
            rt.offsetMin = new Vector2(0, 0);


            return go.transform;
        }


    }

}
