﻿using UnityEngine;
using System.Collections;
using System.Xml.Linq;
/**
 * Defines each XmlViewComponent
 */ 
namespace cortina.platform
{
    public class ViewDescriptor
    {
        public XElement XmlElementSource; // xml source

        // unity components
        public Component Component;
        public Transform ElementTransform;
        public Transform ParentNode;
        public string ElementName;
        public string ElementPath = string.Empty;

        // shared references
        public XmlViewComponent XmlView; // delegates can be bound from view events
        public StyleSheet StyleSheetRef;
        public BindingContext BindingCxt;
    }
}

