﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEngine;
/**
 * Binding context for an XmlViewComponent
 */ 
namespace cortina.platform
{
    public class BindingContext
    {
        static uint instanceId;

        private Dictionary<string, Bindable> bindingDict = new Dictionary<string, Bindable>();

        public BindingContext()
        {
            instanceId++;
        }

        public void Clear()
        {
            bindingDict.Clear();
        }

        internal void AddBinder(ViewDescriptor dscrip, string attributeName, string bindingValue)
        {
            Bindable b = new Bindable()
            {
                BindingName = attributeName,
                BindingValue = bindingValue,
                ViewDescription = dscrip
            };

            bindingDict.Add(bindingValue, b);
        }

        internal void Bind(XmlViewComponent xmlViewComponent, object dataContext)
        {
            FieldInfo[] fields = dataContext.GetType().GetFields();
            foreach (FieldInfo dataContextField in fields)
            {   
                string fieldname = dataContextField.Name;
                if (!bindingDict.ContainsKey(fieldname))
                {
                    Debug.LogWarning("not able to bind " + fieldname);
                    continue;
                }

                Bindable b = bindingDict[fieldname];

                Component c = b.ViewDescription.Component;

                Type ctype = c.GetType();
                // change property
                object datavalue = dataContextField.GetValue(dataContext);
                bool bound = false;
                foreach (PropertyInfo componentField in ctype.GetProperties())
                {
                    string convertedName = XmlResourceBuilder.GetConvertedName(ctype, b.BindingName);
                    if(componentField.Name == convertedName)
                    {
                        Debug.Log("<color=yellow>Binding " + fieldname + " on " + c.name + " to \"" + datavalue+"\"</color>");
                        componentField.SetValue(c, datavalue, null);
                        bound = true;
                        break;
                    }
                }
                if (!bound)
                {
                    Debug.LogError("not able to bind " + datavalue + " to " + c.name);
                }
            }
        }

        internal Bindable[] GetBindables()
        {
            return bindingDict.Values.ToArray();
        }
    }

    public struct Bindable
    {
        public ViewDescriptor ViewDescription;
        public string BindingName;
        public string BindingValue;

    }
}
