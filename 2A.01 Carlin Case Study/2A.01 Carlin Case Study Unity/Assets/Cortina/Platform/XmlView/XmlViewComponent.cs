﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using cortina.architecture;
using Assets.Cortina.Platform.Components.Base;
/**
* An XmlViewComponent is the monobehavior representation of a the XML file view.
* This class acts as a facade class and fully supports the mediation pipeline.
*/
namespace cortina.platform
{
    public class XmlViewComponent : MonoBehaviour
    {
        EventCallBack callerBacker;

        BindingContext binder;
        private object dataContext;
        public object DataContext
        {
            get
            {
                return dataContext;
            }

            set
            {
                binder.Bind(this, value);

                dataContext = value;
            }
        }

        public virtual void OnViewInitialized()
        {
            // I suppose something important should go here
        }

        public void ExecuteCallback(object payload)
        {
            if (callerBacker != null)
                callerBacker.Target.BroadcastMessage(callerBacker.Method, payload);
        }

        public virtual void SetEventCallback(EventCallBack obj)
        {
            callerBacker = obj;
        }

        public void ProcessBindings(BindingContext bindCxt)
        {
            binder = bindCxt;
        }

        protected T GetElementByName<T>(string id)
        {
            Transform t = transform.FindDeepChild(id);
            if (t == null)
            {
                throw new System.Exception("Element with name " + id + " not found in " + this.name);
            }
            T c = t.GetComponent<T>();
            return c;
        }

        protected XmlViewComponent AttachXmlComponent(string xmlfile)
        {
            XmlView vm = new XmlView();
            ViewDescriptor descriptor = XmlResourceBuilder.GetViewDescriptor(this);
            XmlViewComponent comp = vm.ConstructView(xmlfile, this.transform, true, new ViewDescriptor()
            {
                StyleSheetRef = descriptor.StyleSheetRef
            });
            return comp;
        }

        protected T AttachXmlComponent<T>(string xmlfile) where T : XmlViewComponent
        {
            XmlViewComponent comp = AttachXmlComponent(xmlfile);
            return comp as T;
        }

        protected virtual void OnDestroy()
        {
            XmlResourceBuilder.RemoveDesciptor(this);
        }

    }
}