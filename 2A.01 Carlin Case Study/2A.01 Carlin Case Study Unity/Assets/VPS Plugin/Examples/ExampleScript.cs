﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using cortina.VisitorProfileSystem;
using VisitorTracking.Model;

public class ExampleScript : MonoBehaviour
{
    public VPSKioskClient kioskClient;
    public Text DebugText;
    public Button BeginAttractButton;
    public Button BeginMainButton;
    public Button ResetButton;
    public Button InitializeUIButton;
    public Button CreateProfileButton;
    public Button SetMetaDataButton;

    private void Awake()
    {
        BeginAttractButton.onClick.AddListener(() =>
        {
            kioskClient.BeginAttract();
        });

        BeginMainButton.onClick.AddListener(() =>
        {
            kioskClient.BeginMain();
        });

        ResetButton.onClick.AddListener(() =>
        {
            kioskClient.ResetRouter();
        });

        InitializeUIButton.onClick.AddListener(() =>
        {
            kioskClient.InitializeUIOverlay(1);
        });

        CreateProfileButton.onClick.AddListener(() =>
        {
            kioskClient.CreateVisitor<VPSVisitorDataModel>(new VPSVisitorDataModel
            {
                RecognitionDeviceId = "123456"
            });
        });

        SetMetaDataButton.onClick.AddListener(() =>
        {
            kioskClient.SetVisitorMetaData(12344, new Dictionary<string, string>()
            {
                {"FirstName","John" },
                {"LastName","Snow"},
                {"SomeOtherAttribute","SomeOtherValue"}
            });
        });

        VPSKioskClient.DebugLogEvent += OnKioskDebugMessage;
        DebugText.text = "";
    }

    void OnKioskDebugMessage(string message)
    {
        DebugText.text += message + "\n";
    }
}
