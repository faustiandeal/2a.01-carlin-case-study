﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using cortina;
using ApplicationManagerVPSPlugin;
using VisitorTracking.Model;

namespace cortina.VisitorProfileSystem
{
    public class VPSKioskClient : MonoBehaviour
    {
        public static event Action Reset;
        public static event Action Shutdown;
        public static event Action<VPSVisitorDataModel> VisitorProfileReceived;
        public static event Action<int,string> VisitorEntered;
        public static event Action<int,string> VisitorExited;
        public static event Action Connected;
        public static event Action Disconnected;
        public static event Action<string> ClientError;
        public static event Action<byte[]> FileDownloadReceived;
        public static event Action<string, byte[]> CustomEventReceived;
        public static event Action<string> DebugLogEvent;

        public string ApplicationManagerIp = "127.0.0.1";
        public int Port = 6602;
        public bool AutoGetProfile = true; // Automatically Get Profile Information When Recognized By Device, Otherwise handle this manually
        public bool DebugMode = true;

        protected ApplicationManagerClient.ApplicationManagerClient routerClient;

        protected virtual void Start()
        {
            ThreadDispatcher.Init();
            ConnectToVPS();
        }

        #region Overridable Interface

        protected virtual void HandleRouterClientMessageReceived(string command, byte[] payload)
        {
            ThreadDispatcher.Invoke(() =>
            {
                KioskDebugger("Message Received from command: " + command);
                string dataString = (payload == null ? " " : Encoding.UTF8.GetString(payload));
                KioskDebugger("Message payload: " + dataString);
                switch (command)
                {
                    case VPSCommands.Reset:
                        HandleReset();
                        break;
                    case VPSCommands.Shutdown:
                        HandleShutdown();
                        break;
                    case VPSCommands.VisitorEntered:
                        HandleVisitorEntered(dataString);
                        break;
                    case VPSCommands.GetVisitorProfile:
                        HandleGetVisitorProfile(dataString);
                        break;
                    case VPSCommands.VisitorExited:
                        HandleVisitorExited(dataString);
                        break;
                    case "version_succeed":
                    case VPSCommands.Connected:
                        HandleConnected();
                        break;
                    case VPSCommands.Disconnected:
                        HandleDisconnected(dataString);
                        break;
                    case VPSCommands.BeginState:
                        HandleDisconnected(dataString);
                        break;
                    default:
                        CustomMessageReceived(command, payload);
                        break;
                }
            });
        }

        protected virtual void HandleReset()
        {
            if (Reset != null)
            {
                Reset();
            }
        }

        protected virtual void HandleShutdown()
        {
            if (Shutdown != null)
            {
                Shutdown();
            }
        }

        protected virtual void HandleVisitorEntered(string data)
        {
            try
            {
                VPSPayloadObjectDTO payload = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(data);
                if (payload != null && payload.Data != null)
                {
                    if (AutoGetProfile)
                    {
                        GetVisitorProfile(payload);
                    }
                    if (VisitorEntered != null)
                    {
                        VisitorEntered(payload.DeviceId, (string)payload.Data);
                    }
                }
                else
                {
                    Debug.LogError("Visitor Entered Payload is null!");
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Error mapping visitor JSON: " + e);
                Debug.Log(data);
            }
        }

        protected virtual void HandleGetVisitorProfile(string data)
        {
            if (VisitorProfileReceived != null)
            {
                VPSVisitorDataModel visitor = null;
                try
                {
                    visitor = LitJson.JsonMapper.ToObject<VPSVisitorDataModel>(data);
                }
                catch (Exception e)
                {
                    Debug.LogError("Error mapping visitor JSON: " + e);
                    Debug.Log(data);
                }
                VisitorProfileReceived(visitor);
            }
        }

        protected virtual void HandleVisitorExited(string data)
        {
            try
            {
                VPSPayloadObjectDTO payload = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(data);
                if (payload != null && payload.Data != null)
                {
                    if (AutoGetProfile)
                    {
                        GetVisitorProfile(payload);
                    }
                    if (VisitorExited != null)
                    {
                        VisitorExited(payload.DeviceId, (string)payload.Data);
                    }
                }
                else
                {
                    Debug.LogError("Visitor Entered Payload is null!");
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Error mapping visitor JSON: " + e);
                Debug.Log(data);
            }
        }

        protected virtual void HandleConnected()
        {
            Debug.Log("Connected to VPS Backend");
            if (Connected != null)
            {
                Connected();
            }
        }

        protected virtual void HandleDisconnected(string data)
        {
            Debug.Log("Connection to VPS Backend Lost");
            if (Disconnected != null)
            {
                Disconnected();
            }
        }

        protected virtual void HandleRouterClientError(string e)
        {
            Debug.LogError(e);
            if (ClientError != null)
            {
                ClientError(e);
            }
        }

        protected virtual void HandleFileReceived(byte[] fileBytes)
        {
            if (FileDownloadReceived != null)
            {
                FileDownloadReceived(fileBytes);
            }
        }

        protected virtual void CustomMessageReceived(string command, byte[] payload)
        {
            // Override to handle project specific messages
            KioskDebugger("VPS custom message received: `" + command + "` with payload: " + Encoding.UTF8.GetString(payload));
            if (CustomEventReceived != null)
            {
                CustomEventReceived(command, payload);
            }
        }

        protected virtual void OnApplicationQuit()
        {
            routerClient.Shutdown();
        }

        protected virtual void KioskDebugger(object message, bool isError = false)
        {
            if (DebugMode)
            {
#if UNITY_EDITOR
                if (isError)
                {
                    Debug.LogError(message);
                }
                else
                {
                    Debug.Log(message);
                }
#endif
                if (DebugLogEvent != null)
                {
                    DebugLogEvent(message.ToString());
                }
            }
        }

        #endregion

        #region Public Interface

        public virtual void ConnectToVPS()
        {
            routerClient = new ApplicationManagerClient.ApplicationManagerClient(ApplicationManagerIp, Port);
            routerClient.Connected += HandleConnected;
            routerClient.MessageReceived += HandleRouterClientMessageReceived;
            routerClient.Error += HandleRouterClientError;
        }

        public virtual void BeginState(string state, int monitorId = 0)
        {
            SendMessageToApplicationManager(VPSCommands.BeginState, state, monitorId);
        }

        public virtual void BeginAttract(int monitorId = 0)
        {
            BeginState(UIOverlayStates.Attract);
        }
        public virtual void BeginMain(int monitorId = 0)
        {
            BeginState(UIOverlayStates.Main);
        }

        public virtual void CreateVisitor<T>(T visitor) where T : VPSVisitorDataModel
        {
            SendMessageToApplicationManager(VPSCommands.CreateVisitorProfile, visitor);
        }

        public virtual void ResetRouter()
        {
            SendMessageToApplicationManager(VPSCommands.Reset);
        }

        public virtual void InitializeUIOverlay(int monitorId)
        {
            SendMessageToApplicationManager(VPSCommands.InitializeUIOverlay, BitConverter.GetBytes(monitorId));
        }

        public virtual void SetVisitorMetaData(int visitorId, Dictionary<string, string> metaDataDictionary)
        {
            VisitorMetaPost visitorMetaPostData = new VisitorMetaPost
            {
                VisitorKey = visitorId
            };
            List<MetaObject> metaObjects = new List<MetaObject>();
            foreach(KeyValuePair<string,string> kvp in metaDataDictionary)
            {
                metaObjects.Add(new MetaObject
                {
                    Name = kvp.Key,
                    Value = kvp.Value
                });
            }
            visitorMetaPostData.MetaData = metaObjects.ToArray();
            SetVisitorMetaData(visitorMetaPostData);
        }

        public virtual void SetVisitorMetaData(VisitorMetaPost visitorMetaPostData)
        {
            SendMessageToApplicationManager(VPSCommands.SetVisitorMetaData, visitorMetaPostData);
        }

        public virtual void GetVisitorMetaData(int visitorId, string[] keys = null, string[] tags = null)
        {
            VistorMetaRequest visitorMetaRequestObject = new VistorMetaRequest
            {
                VisitorKey = visitorId,
                MetaNames = keys,
                Tags = tags
            };
            GetVisitorMetaData(visitorMetaRequestObject);
        }

        public virtual void GetVisitorMetaData(VistorMetaRequest visitorMetaRequestObject)
        {
            SendMessageToApplicationManager(VPSCommands.GetVisitorMetaData, visitorMetaRequestObject);
        }

        public virtual void GetVisitorProfile(string visitorRecognitionDeviceId, int deviceId = 0)
        {
            SendMessageToApplicationManager(VPSCommands.GetVisitorProfile, visitorRecognitionDeviceId, deviceId);
        }

        public virtual void GetVisitorProfile(VPSPayloadObjectDTO requestPayload)
        {
            SendMessageToApplicationManager(VPSCommands.GetVisitorProfile, requestPayload);
        }

        public virtual void DownloadFile(string filePath)
        {
            SendMessageToApplicationManager(VPSCommands.DownloadFile, filePath);
        }

        public void SendMessageToApplicationManager(string command)
        {
            SendMessageToApplicationManager(command, new byte[0]);
        }

        public virtual void SendMessageToApplicationManager(string command, object payload, int deviceId = 0)
        {
            SendMessageToApplicationManager(
                command,
                Encoding.ASCII.GetBytes(LitJson.JsonMapper.ToJson(new VPSPayloadObjectDTO
                {
                    DeviceId = deviceId,
                    Data = payload
                })));
        }

        public virtual void SendMessageToApplicationManager(string command, VPSPayloadObjectDTO payloadDTO)
        {
            SendMessageToApplicationManager(
                command,
                Encoding.ASCII.GetBytes(LitJson.JsonMapper.ToJson(payloadDTO)));
        }

        public virtual void SendMessageToApplicationManager(string command, byte[] payload)
        {
            KioskDebugger("VPS attempting to send command: `" + command + "` with payload: " + Encoding.UTF8.GetString(payload));
            try
            {
                routerClient.SendMessage(command, payload);
                KioskDebugger("VPS sent command: `" + command + "` with payload: " + Encoding.UTF8.GetString(payload));
            }
            catch (Exception e)
            {
                KioskDebugger("VPS failed to send command: `" + command + "` with exception " + e.Message, true);
            }
        }

        #endregion

        public static Sprite GetSpriteFromByteArray(byte[] imageData)
        {
            Texture2D texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            texture.LoadImage(imageData);
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f));
        }
    }
}