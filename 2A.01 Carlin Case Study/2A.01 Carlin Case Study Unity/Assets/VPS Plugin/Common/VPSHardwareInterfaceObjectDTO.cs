namespace cortina.VisitorProfileSystem
{
    public class VPSHardwareInterfaceObjectDTO
    {
        public int DeviceId; // Visitor Recognition Device Id, to be used throughout the VPS
    }
}