using LitJson;
using PluginCommon;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace cortina.VisitorProfileSystem
{
    public class VPSUtils
    {
        public static byte[] ObjectToByteArray(object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        public static VPSPayloadObjectDTO GetVPSPayloadFromObject<T>(AppCommunicationObject obj)
        {
            byte[] payload = obj.Payload as byte[];
            string jsonString = Encoding.ASCII.GetString(payload);
            return GetVPSPayloadFromObject<T>(jsonString);
        }

        public static VPSPayloadObjectDTO GetVPSPayloadFromObject<T>(string jsonString)
        {
            JsonData jsonData = JsonMapper.ToObject<JsonData>(jsonString);
            string dataObjectJson = jsonData["Data"].ToJson();
            VPSPayloadObjectDTO vpsPayloadObj = JsonMapper.ToObject<VPSPayloadObjectDTO>(jsonString);
            if (typeof(T) == typeof(string))
            {
                vpsPayloadObj.Data = jsonData["Data"].ToString();
            }
            else if (typeof(T).IsPrimitive || typeof(T).Namespace.StartsWith("System"))
            {
                vpsPayloadObj.Data = (T)Convert.ChangeType(dataObjectJson, typeof(T));
            }
            else
            {
                vpsPayloadObj.Data = JsonMapper.ToObject<T>(dataObjectJson);
            }
            return vpsPayloadObj;
        }
    }
}