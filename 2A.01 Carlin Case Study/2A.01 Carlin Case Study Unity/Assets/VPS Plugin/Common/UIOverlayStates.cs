﻿namespace ApplicationManagerVPSPlugin
{
    public class UIOverlayStates
    {
        public const string Attract = "Attract";
        public const string LoadingProfile = "LoadingProfile";
        public const string ViewingProfile = "ViewingProfile";
        public const string Main = "Main";
    }
}
