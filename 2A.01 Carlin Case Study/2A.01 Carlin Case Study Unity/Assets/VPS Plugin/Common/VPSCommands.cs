// VRDP = Visitor Recognition Device Plugin
// VPS = Visitor Profile System
// Kiosk = Individual Unity or HTML application
// UI Overlay = WPF UI Overlay

// IMPORTANT! If you make changes to this file, you must also make changes in `VPSCommands.js`

namespace cortina.VisitorProfileSystem
{
    public class VPSCommands
    {
        public const string Reset = "vps_Reset"; // Signals that the VPS as a whole needs to reset
        public const string Shutdown = "vps_Shutdown"; // Signals that the VPS as a whole should shutdown
        public const string GetVisitorProfile = "vps_GetVisitorProfile"; // Signals to VPS Router that it needs to get profile information 
        public const string CreateVisitorProfile = "vps_CreateVisitorProfile"; // Signals to VPS Router that it needs to create a new profile for a visitor
        public const string GetVisitorMetaData = "vps_GetVisitorMetaData";
        public const string SetVisitorMetaData = "vps_SetVisitorMetaData";
        public const string VisitorEntered = "vps_VisitorEntered"; // Signals to Kiosk that a new visitor with a profile has entered
        public const string VisitorExited = "vps_VisitorExited"; // Signals to Kiosk or VPS Router that a visitor has exited the kiosk
        public const string UploadFile = "vps_UploadFile";
        public const string DownloadFile = "vps_DownloadFile";
        public const string Connected = "vps_Connected";
        public const string Disconnected = "vps_Disconnected";
        public const string BeginState = "vps_BeginState"; // Signals to Kiosk or UI Overlay
        public const string WebSocket = "vps_WebSocket";
        public const string InitializeUIOverlay = "vps_InitializeUIOverlay"; // Signals to the VPS Router that a new overlay needs to be created
    }
}