﻿namespace cortina.VisitorProfileSystem
{
    [System.Serializable]
    public class VPSVisitorDataModel
    {
        public int VisitorId = -1; // id in server database
        public string RecognitionDeviceId;
    }
}