﻿using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;
using cortina;
using cortina.VisitorProfileSystem;
using ComedyVisitorProfileDataModels;
using VisitorTracking.Model;
using LitJson;

public class NCCKioskClient : VPSKioskClient
{
    public static new event Action<NCCVisitorDataModel> VisitorProfileReceived;
    public static event Action<VisitorHumorResponse> VisitorSenseOfHumorReceived;
    public static event Action<MediaResponse> RelatedMediaContentReceived;
    public static event Action<VisitorAttributeGroupingResponse> VisitorAttributeGroupsReceived;
    public static event Action<VisitorAttributeVideoClipResponse> VisitorAttributeClipsReceived;

    #region Overrides

    protected override void CustomMessageReceived(string command, byte[] data)
    {
        base.CustomMessageReceived(command, data);
        ThreadDispatcher.Invoke((InvokeHandler)(() =>
        {
            string dataString = (data == null ? " " : Encoding.UTF8.GetString(data));
            VPSPayloadObjectDTO payloadObjectDto = new VPSPayloadObjectDTO();
            switch (command)
            {
                case NCCCommands.GetSenseOfHumor:
                    payloadObjectDto = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    VisitorHumorResponse response = LitJson.JsonMapper.ToObject<VisitorHumorResponse>((string)payloadObjectDto.Data);
                    if (VisitorSenseOfHumorReceived != null)
                    {
                        VisitorSenseOfHumorReceived(response);
                    }
                    break;
                case NCCCommands.GetRelatedMedia:
                    payloadObjectDto = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    MediaResponse mediaResponse = LitJson.JsonMapper.ToObject<MediaResponse>((string)payloadObjectDto.Data);
                    if (RelatedMediaContentReceived != null)
                    {
                        RelatedMediaContentReceived(mediaResponse);
                    }
                    break;
                case NCCCommands.GetVisitorAttributeGroups:
                    payloadObjectDto = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    VisitorAttributeGroupingResponse visitorAttributeGroupsResponse = LitJson.JsonMapper.ToObject<VisitorAttributeGroupingResponse>((string)payloadObjectDto.Data);
                    if (VisitorAttributeGroupsReceived != null)
                    {
                        VisitorAttributeGroupsReceived(visitorAttributeGroupsResponse);
                    }
                    break;
                case NCCCommands.GetVisitorAttributeClips:
                    payloadObjectDto = LitJson.JsonMapper.ToObject<VPSPayloadObjectDTO>(dataString);
                    VisitorAttributeVideoClipResponse videoClipsResponse = LitJson.JsonMapper.ToObject<VisitorAttributeVideoClipResponse>((string)payloadObjectDto.Data);
                    if (VisitorAttributeClipsReceived != null)
                    {
                        VisitorAttributeClipsReceived(videoClipsResponse);
                    }
                    break;
                default:
                    Debug.LogWarning("Received unrecognized command: `" + command +"`");
                    //Debug.Log(data);
                    break;
            }
        }));
    }
    protected override void HandleGetVisitorProfile(string data)
    {
        if (VisitorProfileReceived != null)
        {
            NCCVisitorDataModel visitor = new NCCVisitorDataModel();
            try
            {
                JsonData jsonData = JsonMapper.ToObject<JsonData>(data);
                string payloadString = jsonData["Data"].ToJson();
                visitor = JsonMapper.ToObject<NCCVisitorDataModel>(payloadString);
            }
            catch (Exception e)
            {
                Debug.LogError("Error mapping visitor JSON: " + e);
                Debug.Log(data);
            }
            VisitorProfileReceived(visitor);
        }
    }

    #endregion

    #region Public Interface
    public void SetTakeaway(NCCVisitorDataModel profile, NCCTakeawayDataModel takeaway)
    {
        SendMessageToApplicationManager(NCCCommands.SetTakeaway, takeaway);
    }

    public void GetSenseOfHumorProfile(int visitorId)
    {
        SendMessageToApplicationManager(NCCCommands.GetSenseOfHumor, visitorId);
    }

    public void GetRelatedMediaContent(MediaRequest mediaRequest)
    {
        SendMessageToApplicationManager(NCCCommands.GetRelatedMedia, mediaRequest);
    }

    public void GetVisitorAttributeGroups(MultiVisitorMediaRequest request)
    {
        SendMessageToApplicationManager(NCCCommands.GetVisitorAttributeGroups, request);
    }

    public void GetVisitorAttributeClips(MultiVisitorMediaRequest request)
    {
        SendMessageToApplicationManager(NCCCommands.GetVisitorAttributeClips, request);
    }
    #endregion
}
