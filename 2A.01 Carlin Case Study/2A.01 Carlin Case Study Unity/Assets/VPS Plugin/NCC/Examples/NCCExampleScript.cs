﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using cortina.VisitorProfileSystem;
using VisitorTracking.Model;

public class NCCExampleScript : MonoBehaviour
{
    public NCCKioskClient kioskClient;
    public Text DebugText;
    public Image DebugImage;
    public Button BeginAttractButton;
    public Button ClipsButton;
    public Button ResetButton;
    public Button InitializeUIButton;
    public Button CreateProfileButton;
    public Button SetMetaDataButton;
    public Button GetProfileButton;
    public Button DownloadMediaButton;

    private void Awake()
    {
        BeginAttractButton.onClick.AddListener(() =>
        {
            kioskClient.BeginAttract();
        });

        ClipsButton.onClick.AddListener(() =>
        {
            kioskClient.GetVisitorAttributeClips(new ComedyVisitorProfileDataModels.MultiVisitorMediaRequest
            {
                VisitorKeys = new List<int> { 37, 71, 74, 78, 79, 80, 51, 52, 53, 54, 56, 57 },
                AvIdentifier = "2B01"
            });
        });

        ResetButton.onClick.AddListener(() =>
        {
            kioskClient.ResetRouter();
        });

        InitializeUIButton.onClick.AddListener(() =>
        {
            kioskClient.InitializeUIOverlay(1);
        });

        CreateProfileButton.onClick.AddListener(() =>
        {
            kioskClient.CreateVisitor<VPSVisitorDataModel>(new VPSVisitorDataModel
            {
                RecognitionDeviceId = "123456"
            });
        });

        SetMetaDataButton.onClick.AddListener(() =>
        {
            kioskClient.SetVisitorMetaData(12344, new Dictionary<string, string>()
            {
                {"FirstName","John" },
                {"LastName","Snow"},
                {"SomeOtherAttribute","SomeOtherValue"}
            });
        });

        GetProfileButton.onClick.AddListener(() =>
        {
            kioskClient.GetVisitorProfile("3M3DDML441W8KP9V");
        });

        DownloadMediaButton.onClick.AddListener(() =>
        {
            kioskClient.DownloadFile("37_bio_info_Picture.jpg");   
        });

        NCCKioskClient.VisitorProfileReceived += (NCCVisitorDataModel newVisitor) =>
        {
            DebugImage.sprite = NCCKioskClient.GetSpriteFromByteArray(newVisitor.PictureBytes);
        };

        NCCKioskClient.DebugLogEvent += OnKioskDebugMessage;
        DebugText.text = "";
    }

    void OnKioskDebugMessage(string message)
    {
        DebugText.text += message + "\n";
    }
}
