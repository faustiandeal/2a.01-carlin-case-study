﻿using cortina.VisitorProfileSystem;

[System.Serializable]
public class NCCVisitorDataModel : VPSVisitorDataModel
{
    public string RFID
    {
        get
        {
            return RecognitionDeviceId;
        }
        set
        {
            RecognitionDeviceId = value;
        }
    }

    public string Name;
    public string Email;
    public string Age;
    public string Zip;
    public string PictureFilePath;
    public byte[] PictureBytes;
}