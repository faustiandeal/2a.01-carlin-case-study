﻿public class NCCCommands
{
    public const string SetTakeaway = "ncc_SetTakeaway";
    public const string GetTakeaway = "ncc_GetTakeaway";
    public const string SetProfileAttribute = "ncc_SetProfileAttribute";
    public const string GetSenseOfHumor = "ncc_GetSenseOfHumor";
    public const string GetRelatedMedia = "ncc_GetRelatedMedia";
    public const string GetVisitorAttributeGroups = "ncc_GetVisitorAttributeGroups";
    public const string GetVisitorAttributeClips = "ncc_GetVisitorAttributeClips";
    // If something else is required here please let Rich know
}
