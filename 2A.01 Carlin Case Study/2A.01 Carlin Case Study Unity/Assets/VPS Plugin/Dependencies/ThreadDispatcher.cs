﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace cortina {
    public static  class ThreadDispatcher {

        static ThreadDispatherMono dispatcher;

        /// <summary>
        /// Must be called on Unity's thread
        /// </summary>
        public static void Init()
        {
            if (dispatcher == null)
            {
                GameObject go = new GameObject();
                go.hideFlags = HideFlags.HideInHierarchy;
                dispatcher = go.AddComponent<ThreadDispatherMono>();
            }
        }

        public static void Invoke(InvokeHandler invokee)
        {
            ThreadDispatherMono.SyncMessage(invokee);
        }

    }
}

public delegate void InvokeHandler();

public class ThreadDispatherMono : MonoBehaviour
{
    public int QueueCount = 0;

    static ThreadDispatherMono Instance;

    private static object locker = new object();
    private static Queue<InvokeHandler> syncMessages = new Queue<InvokeHandler>();


    void Awake()
    {
       
        this.name = "ThreadDispatherMono";
        if (Instance == null) Instance = this;
    }

    // is is called from a thread besides the main thread
    public static void SyncMessage(InvokeHandler val)
    {
        // prevent two threads from queuing at the same time
        lock (locker)
        {
            syncMessages.Enqueue(val);
            Instance.QueueCount = syncMessages.Count;
        }
    }

    // the main thread calls this to access the values put in
    // this may not need locking, since this is the only place dequeing and worse case is it thinks there is less in the queue then there really is
    private void CheckThreadSync()
    {
        lock (locker)
        {
            while (syncMessages.Count > 0)
            {
                InvokeHandler invke = syncMessages.Dequeue();
                invke.Invoke();
            }
           
        }
    }

    // called at teh speed of the physics engine
    // read from thread shared
    protected virtual void FixedUpdate()
    {
        CheckThreadSync();
    }
    
    
}